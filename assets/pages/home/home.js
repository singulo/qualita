$('.owl-destaques').owlCarousel({
    autoplay: true,
    items: 1,
    //loop: true,
    margin: 0,
    //pagination: true
});

$('.owl-destaques-mobile').owlCarousel({
    autoplay: false,
    items: 1,
    loop: true,
    margin: 0,
});


if(new Date('2017-09-20') >= new Date())
{
    $.fancybox({
        'href': $('#base_url').val() + 'assets/images/semana_farroupilha_2017.jpg',
        'closeClick': false
    });

}