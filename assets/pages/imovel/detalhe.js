$(window).load(function()
{
    $("#section-fotos a").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	1600,
        'speedOut'		:	1200,
        'overlayShow'	:	false,
        'loop'	        :	false
    });
});