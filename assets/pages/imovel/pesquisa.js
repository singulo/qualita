var maxResultado = 10;

console.log(filtro);
$('#form-pesquisar').dejsonify(filtro);

//$("select.selectpicker[name='cidade']").selectpicker('val', filtro.cidade);
//$("select.selectpicker[name='id_tipo']").selectpicker('val', filtro.id_tipo);
////$("select.selectpicker[name='condominio']").selectpicker('val', filtro.condominios);
//$("#form-pesquisar input[name='id']").val(filtro.id);
//
//$('#form-pesquisar input[name="preco_min"]').val(filtro.preco_min);
//$('#form-pesquisar input[name="preco_max"]').val(filtro.preco_max);

var imovelVisualizacao = $('#imovel-visualizacao').clone();

$(document).ready(function(){

    console.log('awheaiweaw');
    if(window.location.pathname == '/imovel/pesquisar' || window.location.pathname == '/novo/imovel/pesquisar')
    {
        var pag = getParameterByName('pagina', location.href);

        pesquisar(pag, pag != null);
    }

    $('#ordenar_por').on('change', function(){
        pesquisar(0);
    });
});


function pesquisar(pagina, forcar_total)
{
    if(window.location.pathname != '/imovel/pesquisar' && window.location.pathname != '/novo/imovel/pesquisar')
    {
        $('#form-pesquisar').submit();
    }
    else
    {
        var msg_encontrados = '';
        if(pagina > 0)
            msg_encontrados = $('#msg-resultados-encontrados').html();

        $('#msg-resultados-encontrados').html('Buscando <strong>imóveis</strong>, aguarde ...');

        $('#imoveis-encontrados').children().fadeOut(800, function(){
            $(this).remove();
        });

        filtro = {};

        filtro.pagina = pagina;

        if(forcar_total)
            filtro.forcar_total = 1;

        filtro.limite = maxResultado;
        filtro.ordenacao = $('#ordenar_por').val();

        //ATUALIZA URL SEM RECARREGAR A PÁGINA

        window.history.pushState('', document.title, $('#base_url').val() + 'imovel/pesquisar?' + $.param($('#form-pesquisar ').jsonify()) + '&' + $.param(filtro));
        filtro.filtro = $('#form-pesquisar').jsonify();


        ajaxPost(
            filtro,
            base_url_filial('imovel/buscar', true),
            {
                successCallback: function (data) {
                    $.each(data.imoveis, function (key, imovel) {
                        console.log(imovel);
                        $('#imoveis-encontrados').append(montar_imovel(imovel));
                    });

                    if (data.total != undefined && data.total > 0)
                    {
                        $('#msg-resultados-encontrados').html('<strong>Encontramos ' + data.total + '</strong> <br> <small>ofertas em nosso site.</small>');
                        atualizar_paginacao(parseInt(filtro.pagina) + 1, data.total);
                    }
                    else if(data.total != undefined)
                    {
                        $('#msg-resultados-encontrados').html('Nada foi encontrado.');
                        atualizar_paginacao(parseInt(filtro.pagina) + 1, 0);
                    }
                    else
                        $('#msg-resultados-encontrados').html(msg_encontrados);
                },
                failureCallback: function (data) {
                    alertify.error('Ocorreu um erro ao obter os imóveis.')
                },
                errorCallback: function (jqXhr, textStatus, errorThrown) {
                    alertify.error('Ocorreu um erro ao obter os imóveis. Tente novamente mais tarde.')
                },
                completeCallback: function () {
                }
            }
        );
    }
}

function montar_imovel(imovel)
{
    novaDiv = imovelVisualizacao.clone();

    novaDiv.find('img.imovel-img').attr('src', $('#filial_fotos_imoveis').val() + imovel.foto);

    novaDiv.find('.imovel-url').attr('href', $('#base_url').val() + 'imovel?id=' + imovel.id);

    novaDiv.find('.imovel-tipo').text(imoveis_tipos[imovel.id_tipo].tipo);

    novaDiv.find('.imovel-cidade').html(imovel.cidade);
    novaDiv.find('.imovel-descricao').text(imovel.descricao);
    if(imovel.suites > 0)
        novaDiv.find('.imovel-suites').text(imovel.suites + ' suítes');
    else
        novaDiv.find('.imovel-suites-bloco').remove();

    if(imovel.area_total > 0)
        novaDiv.find('.imovel-area').text(imovel.area_total + 'm²');
    else
        novaDiv.find('.imovel-area-bloco').remove();

    if(imovel.garagem > 0)
        novaDiv.find('.imovel-garagem').text(imovel.garagem);
    else
        novaDiv.find('.imovel-garagem-bloco').remove();

    novaDiv.find('.imovel-valor').html(exibir_valor_imovel(imovel, 'R$'));

    novaDiv.show();

    return novaDiv;
}

function atualizar_paginacao(paginaAtual, items)
{
    $('#pagination').pagination({
        items: items,
        itemsOnPage: maxResultado,
        cssStyle: 'light-theme',
        nextText: 'Próximo <i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i> Anterior',
        currentPage: paginaAtual,
        onPageClick : function(pageNumber){
            pesquisar((pageNumber-1), maxResultado);
        }
    });
}