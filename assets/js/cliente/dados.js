$(document).ready(function() {

    $("#form-dados").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            nome: {
                required: true,
                minlength: 5
            }
        }
    });

    //USUARIO LOGADO
    if(JSON.parse($('#usuario').val()) != null)
    {
        var usuario = JSON.parse($('#usuario').val());

        //$('#form-dados').dejsonify(usuario);

        var creci = '';

        if(usuario.corretor.nivel != 4)
            creci = 'creci: ' + usuario.corretor.creci;

        $('#modal-dados img.corretor-foto').attr('src', $('#filial_fotos_corretores').val() + usuario.corretor.id_corretor + '.jpg');
        $('#modal-dados .corretor-nome').text(usuario.corretor.nome);
        $('#modal-dados .corretor-creci').text(creci);
        $('#modal-dados .corretor-email').html('email: <a href="mailto:' + usuario.corretor.email + '" target="_top">' + usuario.corretor.email + '</a>');
    }
});

function cliente_salvar_dados()
{
    if(!$("#form-dados").valid()) return;

    var $btn = $('.btn-salvar').button('loading');

    var dados = $("#form-dados").jsonify();

    cliente_atualizar_dados(
        dados,
        {
            successCallback: function(data){
                alertify.success('Seus dados foram atualizados!');
            },
            failureCallback: function(data){
                alertify.error('Por favor certifique-se de ter editado pelo menos um dos campo.');
            },
            errorCallback: function(){
                alertify.error('Ocorreu um erro ao atualizar seus dados.');
            },
            completeCallback: function(){
                $btn.button('reset');
            }
        });
}