$('.selectpicker').selectpicker();

$('.telefone').mask('(99)9999-9999?9');

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function obter_cidades_pela_uf(uf, simpleRequest)
{
    ajaxPost(
        uf,
        $('#base_url').val() + 'cidade/obter_cidade_pela_uf',
        simpleRequest
    );
}

$("#modal-dados").on('show.bs.modal', function () {
    if($('#form-dados select[name=uf]').val() != undefined && $('#form-dados select[name=uf]').val() != '')
        obter_cidades($('#form-dados'), $('#form-dados select[name=cidade]').data('cidade-default'));
});


function base_url_filial(acrescentar, filial_na_sessao)
{
    if(filial_na_sessao == undefined || filial_na_sessao === false)
        return  $('#base_url').val() + acrescentar;
    else
    {
        var filial_acrescentar = 'filial=' + $('#filial_sessao').val();

        if (acrescentar.indexOf("?") !== -1)
            filial_acrescentar = acrescentar + '&' + filial_acrescentar;
        else
            filial_acrescentar = acrescentar + '?' + filial_acrescentar;

        return $('#base_url').val() + filial_acrescentar;
    }
}