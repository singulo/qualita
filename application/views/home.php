<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-principal'); ?>
<? $this->load->view('templates/filtro-rapido'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/home/home.css'); ?>">

<!-- DESTAQUES -->
<div class="imoveis-destaques">
    <img src="<?= base_url('assets/images/arrow-up.png'); ?>" class="img-responsive center-block">

    <h3 class="text-center"><em>CELEBRE AS FÉRIAS COM A FAMÍLIA <br><strong>IMÓVEIS DECORADOS, PRONTOS PARA MORAR</strong></em></h3>
    <hr>

    <div class="container">
        <? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
        <div class="hidden-xs hidden-sm owl-destaques">
            <? foreach(array_chunk($destaques, 3) as $imoveis) : ?>
                <div class="item">
                    <? if(isset($imoveis[0])) : ?>
                        <div class="col-md-6 imovel-unico ">
                            <a href="<?= base_url('imovel?id=' . $imoveis[0]->id); ?>">
                                <div class="col-md-12 img-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imoveis[0]->foto ; ?>)">
                                    <div class="row legenda-imovel">
                                        <div class="col-md-6">
                                            <h4><?= $_SESSION['filial']['tipos_imoveis'][$imoveis[0]->id_tipo]->tipo; ?></h4>
                                            <small><?= format_valor($imoveis[0]->valor, 'R$'); ?></small>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span class="glyphicon glyphicon-map-marker"></span> <?= $imoveis[0]->cidade; ?></p>
                                            <p><span class="glyphicon glyphicon-bed"></span> <?= $imoveis[0]->suites; ?> suites</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <? endif; ?>
                    <? if(isset($imoveis[1])) : ?>
                        <div class="col-md-6 imovel-dividido pull-right">
                            <a href="<?= base_url('imovel?id=' . $imoveis[1]->id); ?>">
                                <div class="col-md-12 img-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imoveis[1]->foto ; ?>)">
                                    <div class="row legenda-imovel">
                                        <div class="col-md-6">
                                            <h4><?= $_SESSION['filial']['tipos_imoveis'][$imoveis[1]->id_tipo]->tipo; ?></h4>
                                            <small><?= format_valor($imoveis[1]->valor, 'R$'); ?></small>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span class="glyphicon glyphicon-map-marker"></span> <?= $imoveis[1]->cidade; ?></p>
                                            <p><span class="glyphicon glyphicon-bed"></span> <?= $imoveis[1]->suites; ?> suites</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <? endif; ?>
                    <? if(isset($imoveis[2])) : ?>
                        <div class="col-md-6 imovel-dividido pull-right">
                            <a href="<?= base_url('imovel?id=' . $imoveis[2]->id); ?>">
                                <div class="col-md-12 img-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imoveis[2]->foto ; ?>)">
                                    <div class="row legenda-imovel">
                                        <div class="col-md-6">
                                            <h4><?= $_SESSION['filial']['tipos_imoveis'][$imoveis[2]->id_tipo]->tipo; ?></h4>
                                            <small><?= format_valor($imoveis[2]->valor, 'R$'); ?></small>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span class="glyphicon glyphicon-map-marker"></span> <?= $imoveis[2]->cidade; ?></p>
                                            <p><span class="glyphicon glyphicon-bed"></span> <?= $imoveis[2]->suites; ?> suites</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
        <div class="hidden-md hidden-lg owl-destaques-mobile">
            <? foreach($destaques as $imovel) : ?>
                <div class="item">
                    <div class="imovel-unico ">
                        <div class="img-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imoveis[0]->foto ; ?>)">
                            <a href="<?= base_url('imovel?id=' . $imoveis[0]->id); ?>">
                                <div class="row legenda-imovel">
                                    <div class="col-xs-6">
                                        <h4><?= $_SESSION['filial']['tipos_imoveis'][$imoveis[0]->id_tipo]->tipo; ?></h4>
                                        <small><?= format_valor($imoveis[0]->valor, 'R$'); ?></small>
                                    </div>
                                    <div class="col-xs-6">
                                        <p><span class="glyphicon glyphicon-map-marker"></span> <?= $imoveis[0]->cidade; ?></p>
                                        <p><span class="glyphicon glyphicon-bed"></span> <?= $imoveis[0]->suites; ?> suites</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>


<?
$data['imoveis_linha_titulo'] = '<h3><em>CONDOMÍNIOS <br><strong>FECHADOS.</strong></em></h3>';
$data['imoveis_linha'] = $_SESSION['filial']['condominios']; ?>

<? $this->load->view('templates/imovel/linha', $data); ?>
<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<script type="text/jscript" src="<?= base_url('assets/pages/home/home.js'); ?>"></script>

