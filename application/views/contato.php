<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/contato/contato.css'); ?>">

<div class="mapa-contato col-xs-12">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div id="map-canvas" data-marker-none="true" data-lat-lng="<?= $_SESSION['filial']['latitude_longitude']; ?>" style="width: 100%; height: 500px;"></div>
            </div>
        </div>
        <div class="col-md-6">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <em>
                        <h1>Fale com a gente</h1>
                        <p>Se tiver dúvidas, críticas sugestões ou quer vender seu imóvel, basta preencher os campos abaixo com os seus dados e a sua mensagem, e clicar em "Enviar". Depois é só ficar atento, que em breve lhe daremos um retorno.</p>
                    </em>
                </div>
            </div>
        </div>

        <form id="form-contato" onsubmit="return false;">
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <input type="text" name="nome" class="form-control" placeholder="Qual é o seu nome?">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Informe seu email">
                </div>
                <div class="form-group">
                    <input type="text" name="telefone" class="form-control telefone" placeholder="Número para contato?">
                </div>
                <div class="form-group">
                    <input type="text" name="assunto" class="form-control" placeholder="Qual é o assunto?">
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="form-group">
                    <textarea name="mensagem" class="form-control" rows="8" placeholder="Digite aqui o motivo do seu contato..."></textarea>
                </div>

                <button type="button" class="btn btn-info pull-right btn-enviar" data-loading-text="Aguarde..." onclick="cliente_enviar_novo_contato();">ENVIAR</button>
            </div>
        </form>
    </div>
    </div>
</div>
<? $data['busca_rapida'] = false; ?>
<? $this->load->view('templates/filtro-rapido', $data); ?>


<?
$data['imoveis_linha_titulo'] = '<h3><em>CONDOMÍNIOS <br><strong>FECHADOS.</strong></em></h3>';
$data['imoveis_linha'] = $_SESSION['filial']['condominios_horizontais']; ?>

<? $this->load->view('templates/imovel/linha', $data); ?>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<!--Google Analytics-->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-88034517-1', 'auto');
    ga('send', 'pageview');

</script>

<!--MAPA-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIN1JY_p-QMDCkL4Dm9Qo2HqByRzeDYYg"></script>
<script src="<?= base_url('assets/js/gmap.js'); ?>"></script>