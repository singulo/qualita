<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $data['busca_rapida'] = false; ?>
<? $this->load->view('templates/filtro-rapido', $data); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/condominios-lancamentos/lista.css'); ?>">

<div class="container condominios">
    <? foreach($condominios as $condominio) : ?>
        <div class="col-md-4 condominio">
            <a href="<?= base_url('condominio?id=' . $condominio->id); ?>">
                <figure>
                    <img src="<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>">
                    <figcaption>
                        <div class="col-xs-12">
                            <h4 class="text-center text-uppercase"><?= $condominio->nome; ?></h4>
                        </div>
                    </figcaption>
                </figure>
            </a>
        </div>
    <? endforeach; ?>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<style>
    .condominios
    {
        margin-top: 30px;
    }

    .condominio img
    {
        width: 100%;
        height: 300px;
    }

    .condominio figcaption
    {
        background-color: rgba(27, 55, 77, 0.82);
        margin-top: -40px;
        height: 40px;
        position: relative;
        margin-bottom: 25px;
        color: #fff;
    }
</style>

