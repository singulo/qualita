<? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/CondominioTipos.php'; ?>
<? require_once MODULESPATH . 'simples/helpers/condominio_detalhe_helper.php'; ?>

<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/tabs/css/tabs.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/tabs/css/tabstyles.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/empreendimento/empreendimento.css'); ?>" />

<div id="imagens-principais-carousel">
    <? foreach($condominio->fotos_principais as $foto) : ?>
        <div class="item">
            <img src="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo ?>" >
        </div>
    <? endforeach; ?>
</div>
<div class="hidden-xs hidden-sm owl-carousel-navigation">
    <a class="prev"><img src="<?= base_url('assets/images/arrow-prev.png'); ?>"></a>
    <a class="next"><img src="<?= base_url('assets/images/arrow-next.png'); ?>"></a>
</div>

<div class="lancamento-cabecalho">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <span><?= CondominioTipos::toString($condominio->condominio_tipo); ?></span>
                <h3><?= $condominio->nome; ?> - <?= $condominio->cidade; ?></h3>
            </div>
            <div class="hidden-xs hidden-sm">
                <? if(count($condominio->unidades) > 1) : ?>
                    <div class="col-md-3 text-right">
                        <span>Área</span>
                        <h3><? exibir_valor_min_max($condominio->unidades, 'area_util', 'de %sm² a %sm²'); ?></h3>
                    </div>
                    <div class="col-md-1 text-right">
                        <span>Domitórios</span>
                        <h3><? exibir_valor_min_max($condominio->unidades, 'dormitorios'); ?></h3>
                    </div>
                <? elseif(count($condominio->unidades) == 1) : ?>
                    <div class="col-md-3 text-right">
                        <span>Área</span>
                        <h3><?= obter_valor_min($condominio->unidades, 'area_util') . 'm²'; ?></h3>
                    </div>
                    <div class="col-md-1 text-right">
                        <span>Domitórios</span>
                        <h3><?= obter_valor_min($condominio->unidades, 'dormitorios'); ?></h3>
                    </div>
                <? else : ?>
                    <div class="col-md-3 text-right">
                        <span>Área</span>
                        <h3>Nenhum</h3>
                    </div>
                    <div class="col-md-1 text-right">
                        <span>Domitórios</span>
                        <h3>Nenhum</h3>
                    </div>
                <? endif; ?>
                <? if(count($condominio->unidades) > 1) : ?>
                    <div class="col-md-2 text-right">
                        <span>A partir de</span>
                        <h3><?= format_valor(obter_valor_min($condominio->unidades, 'valor'), '<small>R$</small>'); ?></h3>
                    </div>
                <? else : ?>
                    <div class="col-md-2 text-right">
                        <span>A partir de</span>
                        <h3>Não informado</h3>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-5 imovel-descricao">
        <h4 class="pull-left">DESCRIÇÃO DO CONDOMÍNIO</h4> <small class="pull-right">CÓDIGO: <?= $condominio->id; ?></small>
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-pills text-center">
                    <li>
                        <span><?= $condominio->area_total; ?>m²</span><br><small> ÁREA TOTAL</small>
                    </li>
                    <? if(strlen($condominio->previsao_entrega) > 0) : ?>
                        <li>
                            <span><?= $condominio->previsao_entrega; ?></span><br><small> Previsão de entrega</small>
                        </li>
                    <? endif; ?>
                </ul>

                <p><?= $condominio->descricao; ?></p>

                <div class="imovel-complementos">
                    <? if($condominio->complementos) : ?>
                        <h4>AQUI VOCÊ VAI ENCONTRAR</h4>
                        <? foreach($condominio->complementos as $complemento) : ?>
                            <span><?= $complemento->complemento; ?></span>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 imovel-midias">
        <section>
            <div class="tabs tabs-style-linetriangle">
                <nav>
                    <ul>
                        <li>
                            <a href="#section-fotos">
                                <span>
                                    <img src="<?= base_url('assets/images/icon-photo.png'); ?>"><br> <small>Fotos  (<?= count($condominio->fotos['normais']);?>)</small>
                                </span>
                            </a>
                        </li>
                        <? if(count($condominio->fotos['plantas']) > 0) : ?>
                            <li>
                                <a href="#section-plantas">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-metragem.png'); ?>"><br> <small>Plantas (<?= count($condominio->fotos['plantas']);?>)</small>
                                    </span>
                                </a>
                            </li>
                        <? endif; ?>
                        <? if(count($condominio->videos) > 0) : ?>
                            <li>
                                <a href="#section-videos">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-youtube.png'); ?>"><br> <small>Vídeos (<?= count($condominio->videos);?>)</small>
                                    </span>
                                </a>
                            </li>
                        <? endif; ?>
                        <? if(count($condominio->unidades) > 0) : ?>
                            <li>
                                <a href="#section-unidades">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-metragem.png'); ?>"><br> <small>Unidades (<?= count($condominio->unidades);?>)</small>
                                    </span>
                                </a>
                            </li>
                        <? endif; ?>
                    </ul>
                </nav>
                <div class="content-wrap">
                    <section id="section-fotos">
                        <? foreach($condominio->fotos['normais'] as $foto) : ?>
                            <div class="col-md-3">
                                <a rel="galleria-fotos" href="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>" title="<?= $foto->legenda; ?>">
                                    <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>" >
                                </a>
                            </div>
                        <? endforeach; ?>
                    </section>
                    <? if(count($condominio->fotos['plantas']) > 0) : ?>
                        <section id="section-plantas">
                            <? foreach($condominio->fotos['plantas'] as $foto) : ?>
                                <div class="col-md-3">
                                    <a rel="galleria-plantas" href="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>" title="<?= $foto->legenda; ?>">
                                        <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>" >
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </section>
                    <? endif; ?>
                    <? if(count($condominio->videos) > 0) : ?>
                        <section id="section-videos">
                            <? foreach($condominio->videos as $video) : ?>
                                <div class="col-md-4">
                                    <iframe width="100%" height="150" src="https://www.youtube.com/embed/<?= $video->id_youtube; ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <? endforeach; ?>
                        </section>
                    <? endif; ?>
                    <? if(count($condominio->unidades) > 0) : ?>
                        <section id="section-unidades">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Área útil</th>
                                    <th>Dormitórios</th>
                                    <th>Suítes</th>
                                    <th>Vagas</th>
                                    <th>Valor</th>
                                </tr>
                                <tbody>
                                <? foreach($condominio->unidades as $unidade) : ?>
                                    <tr>
                                        <td><?= $unidade->area_util; ?>m²</td>
                                        <td><?= $unidade->dormitorios; ?></td>
                                        <td><?= $unidade->suites; ?></td>
                                        <td><?= $unidade->vagas; ?></td>
                                        <td><?= format_valor($unidade->valor, '<small>R$</small>'); ?></td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                                </thead>
                            </table>
                        </section>
                    <? endif; ?>
                </div><!-- /content -->
            </div><!-- /tabs -->
        </section>
    </div>
</div>

<? if(count($condominio->imoveis) > 0) : ?>

<?
$data['imoveis_linha_titulo'] = '<h3><em>IMÓVEIS DO CONDOMÍNIO <br><strong>' . $condominio->nome . '</strong></em></h3>';
$data['imoveis_linha'] = $condominio->imoveis;
$data['eh_imovel'] = TRUE; ?>

<? $this->load->view('templates/imovel/linha', $data); ?>

<? endif; ?>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<script src="<?= base_url('assets/plugins/tabs/js/cbpFWTabs.js'); ?>"></script>
<script>
    (function() {

        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });

    })();

    $(document).ready(function(){
        $('#imagens-principais-carousel').owlCarousel({
            center: true,
            autoplay: true,
            autoWidth: true,
            items: 1,
            loop: true,
            margin: 0,
            responsive:{
                1200:{
                    items: 1.47
                }
            }
        });

        $(".owl-carousel-navigation .next").click(function(){
            $('#imagens-principais-carousel').trigger('next.owl.carousel');
        });
        $(".owl-carousel-navigation .prev").click(function(){
            $('#imagens-principais-carousel').trigger('prev.owl.carousel');
        });

        $('#section-fotos a').fancybox({
            openEffect	: 'elastic',
            closeEffect	: 'elastic',
        });

        $('#section-plantas a').fancybox({
            openEffect	: 'elastic',
            closeEffect	: 'elastic',
        });
    });
</script>