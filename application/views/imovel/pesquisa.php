<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $data['busca_rapida'] = false; ?>

<? $this->load->view('templates/filtro-rapido', $data); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/imovel/pesquisa.css'); ?>" />

<div class="container">
    <h3 class="text-center"><em id="msg-resultados-encontrados">ENCONTRAMOS 20 OFERTAS DE IMÓVEIS <br><strong>CONFIRA A SEGUIR</strong></em></h3>
    <div class="imoveis col-xs-12">
        <img src="<?= base_url('assets/images/arrow-up.png'); ?>" class="img-responsive center-block">
        <hr>
        <div class="row" id="imoveis-encontrados">
            <div class="col-md-6 imovel" id="imovel-visualizacao" style="display: none;">
                <a href="#" class="imovel-url">
                    <figure>
                        <img src="http://placehold.it/350x250" class="imovel-img">
                        <figcaption>
                            <div class="col-sm-6 col-md-8">
                                <h4 class="imovel-tipo text-uppercase">APARTAMENTO 3 DORM.</h4><small class="imovel-valor">R$ 190.000,00</small>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <p><span class="glyphicon glyphicon-map-marker"></span> <i class="imovel-cidade">Porto Alegre</i></p>
                                <p class="imovel-suites-bloco"><span class="glyphicon glyphicon-bed"></span> <i class="imovel-suites">2 suites</i></p>
                            </div>
                        </figcaption>
                    </figure>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-12">
        <div class="center-block" style="display: table;">
            <div id="pagination" class="pagination-holder clearfix">
                <div id="light-pagination" class="pagination light-theme simple-pagination">
                </div>
            </div>
        </div>
    </div>
</div>

<?
$data['imoveis_linha_titulo'] = '<h3><em>CONDOMÍNIOS <br><strong>FECHADOS.</strong></em></h3>';
$data['imoveis_linha'] = $_SESSION['filial']['condominios']; ?>

<? $this->load->view('templates/imovel/linha', $data); ?>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>



