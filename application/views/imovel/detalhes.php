<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? require_once(APPPATH . '../modules/admin/helpers/valor_imovel_formater_helper.php'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/tabs/css/tabs.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/tabs/css/tabstyles.css'); ?>" />

<? $this->load->view('imovel/contato-interesse'); ?>

<div class="hidden-xs hidden-sm">
    <div id="imagens-principais-carousel">
        <? foreach($imovel->fotos as $foto) : ?>
            <div class="item">
                <img src="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->f_id_foto . '.jpg'; ?>"  onerror="this.src='<?= base_url('assets/images/imovel-sem-foto.jpg');?>'" height="580px">
            </div>
        <? endforeach; ?>
    </div>
</div>
<div class="visible-xs visible-sm">
    <div class="col-xs-12">
        <div class="row">
            <img style="padding-bottom: 15px;" class="img-responsive" src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos[0]->f_id_foto . '.jpg'; ?>"  onerror="this.src='<?= base_url('assets/images/imovel-sem-foto.jpg');?>'" height="580px">
        </div>
    </div>
</div>
<div class="hidden-xs hidden-sm owl-carousel-navigation">
    <a class="prev"><img src="<?= base_url('assets/images/arrow-prev.png'); ?>"></a>
    <a class="next"><img src="<?= base_url('assets/images/arrow-next.png'); ?>"></a>
</div>

<input id="cod_imovel" type="hidden" value="<?= $imovel->f_codigo; ?>">

<div class="imovel-cabecalho">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <? if(strpos(strtolower($imovel->f_tipo), 'apartamento') === false) : ?>
                    <span><?= $imovel->f_condominio; ?></span>
                <? endif; ?>
                <h1><?= $imovel->f_tipo; ?> - <small><?= $imovel->f_cidade; ?></small></h1>
            </div>
            <div class="hidden-xs hidden-sm">
                <div class="col-md-3 text-right">
                    <span>Área</span>
                    <h1><?= $imovel->f_area_total; ?>m²</h1>
                </div>
                <div class="col-md-1 text-right">
                    <span>Suítes</span>
                    <h1><?= $imovel->f_numero_suites;?></h1>
                </div>
                <div class="col-md-1 text-right">
                    <span>Vagas</span>
                    <h1><?= $imovel->f_numero_garagens;?></h1>
                </div>
                <div class="col-md-2 text-right">
                    <span>A partir de</span>
                    <h1><?= format_valor($imovel->f_valor, 'R$'); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container" style="margin-bottom: 50px;">
    <div class="col-md-7 imovel-midias">
        <section>

            <h4>MÍDIAS DO IMÓVEL</h4>

            <div class="tabs tabs-style-linetriangle">
                <nav>
                    <ul>
                        <li>
                            <a href="#section-fotos">
                                <span>
                                    <img src="<?= base_url('assets/images/icon-photo.png'); ?>"><br> <small>Fotos</small>
                                </span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="content-wrap">
                    <section id="section-fotos">
                        <div class="aviso-para-ver-fotos text-left">
                            <h4>Este imóvel contem <strong><?= $total_fotos; ?> fotos</strong></h4>
                            <h5>Tenha acesso as fotos de <strong>todos imóveis</strong>, apenas faça seu cadastro.</h5>
                            <button class="btn btn-info" data-toggle="modal" data-target="#modal-login">Entre/Cadastre-se</button>
                        </div>
                    </section>
                </div><!-- /content -->
            </div><!-- /tabs -->
        </section>
    </div>
    <div class="col-md-5 imovel-descricao">
        <h4 class="pull-left">DESCRIÇÃO IMÓVEL</h4> <small class="pull-right">CÓDIGO: <?= $imovel->f_codigo; ?></small>
        <div class="col-xs-12">
            <div class="row">
                <ul class="nav nav-pills text-center">
                    <li>
                        <span><?= $imovel->f_area_total; ?>m²</span><br><small> ÁREA TOTAL</small>
                    </li>
                    <li>
                        <span><?= $imovel->f_numero_suites;?></span><br><small> SUÍTES</small>
                    </li>
                    <li>
                        <span><?= $imovel->f_numero_garagens;?></span><br><small> VAGAS</small>
                    </li>
                    <li>
                        <button class="btn-compartilhar">
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.imobiliariaqualita.com.br%2Fimovel%3Fcod_imovel%3D<?=$imovel->f_codigo; ?>&amp;src=sdkpreparse"><i class="fa fa-facebook" aria-hidden="true"></i> Compartilhar</a>
                        </button>
                    </li>
                </ul>

                <p><?= $imovel->f_descricao; ?></p>

                <div class="col-xs-12 imovel-valor">
                    <div class="row">
                        <div class="col-md-7">
                            <h1><?= format_valor($imovel->f_valor, '<small>R$</small>'); ?></h1>
                        </div>
                        <div class="col-md-5">
                            <button type="button" class="btn btn-info btn-interesse">FAZER PROPOSTA</button>
                        </div>
                    </div>
                </div>

                <? if(strlen($imovel->f_complementos_condominio) > 0) : ?>
                    <div class="imovel-complementos">
                        <h4>AQUI VOCÊ VAI ENCONTRAR</h4>
                        <? foreach(explode(',', $imovel->f_complementos_unidade) as $complemento) : ?>
                            <span><?= trim($complemento); ?></span>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>

<?
$data['imoveis_linha_titulo'] = '<h3><em>LANÇAMENTOS <br><strong>IMÓVEIS NA PLANTA.</strong></em></h3>';
$data['imoveis_linha'] = $_SESSION['filial']['lancamentos']; ?>

<? $this->load->view('templates/imovel/linha', $data); ?>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<script src="<?= base_url('assets/plugins/tabs/js/cbpFWTabs.js'); ?>"></script>
<script src="<?= base_url('assets/pages/imovel/detalhe.js?v=1.2'); ?>"></script>

<style>
    .btn-compartilhar a,
    .btn-compartilhar a:hover
    {
        color: white;
        text-decoration: none;
    }

    .btn-compartilhar .fa
    {
        margin-right: 6px;
    }

    .btn-compartilhar
    {
        background-color: #4267b2;
        width: 140px;
        padding: 10px;
        border: none;
    }

    .tabs-style-linetriangle nav li.tab-current a:after
    {
        border-top-color: #fff;
    }

    .tabs-style-linetriangle nav a span
    {
        padding-bottom: 10px;
    }

    section > div.tabs.tabs-style-linetriangle > nav img
    {
        height: 55px;
    }

    .tabs nav a
    {
        line-height: 1.5;
    }

    .tabs nav a small
    {
        font-size: 13px;
        font-style: italic;
    }

    .content-wrap #section-fotos img
    {
        margin-bottom: 25px;
    }

    .imovel-midias h4,
    .imovel-descricao h4,
    .imovel-descricao small,
    .imovel-descricao ul li span
    {
        color: #636262;
        font-style: italic;
        font-weight: 600;
    }

    .imovel-descricao small
    {
        margin-top: 13px;
        font-size: 13px;
    }

    .imovel-descricao ul li:not(:last-child)
    {
        border-right: 1px solid #e0e0e0;
    }

    .imovel-descricao ul
    {
        margin-top: 20px;
        margin-bottom: 25px;
    }

    .imovel-descricao ul li
    {
        margin-right: 8px;
        padding-right: 8px;
        width: 17%;
    }

    .imovel-descricao ul li span
    {
        font-size: 18px;
    }

    .imovel-descricao ul li small
    {
        font-size: 10px;
    }

    .imovel-descricao .imovel-valor
    {
        padding-left: 0;
        padding-right: 0;
    }

    .imovel-descricao .imovel-valor h1
    {
        font-weight: 700;
        font-style: italic;
    }

    .imovel-descricao .imovel-valor small
    {
        float: left;
        font-size: 16px;
        padding-right: 5px;
    }

    .imovel-descricao .imovel-valor button.btn
    {
        margin-top: 15px;
        border-radius: 0px;
        background-color: #093c5f;
        border: none;
        font-style: italic;
        height: 45px;
        width: 100%;
        margin-bottom: 15px;
    }

    .imovel-descricao .imovel-complementos h4
    {
        margin-top: 100px;
    }

    .imovel-descricao .imovel-complementos span
    {
        margin-right: 5px;
        border: solid 1px #ccc;
        padding: 0px 5px;
        margin-bottom: 5px;
        display: inline-block;
        font-style: italic;
        font-size: 13px;
    }

    #imagens-principais-carousel
    {
        background-color: #174f7b;
    }

    @media screen and (max-width: 768px)
    {
        #imagens-principais-carousel
        {
            height: 580px;
        }

        #imagens-principais-carousel .owl-item
        {
            background-color: transparent;
            opacity: 1 !important;
        }
    }

    #imagens-principais-carousel.owl-carousel .owl-item.active.center
    {
        opacity: 1;
    }

    #imagens-principais-carousel.owl-carousel .owl-item
    {
        opacity: 0.3;
        -webkit-transition: all 300ms ease-in;
        -moz-transition: all 300ms ease-in;
        -ms-transition: all 300ms ease-in;
        -o-transition: all 300ms ease-in;
        transition: all 300ms ease-in;
    }

    .owl-carousel-navigation a:hover
    {
        cursor: pointer;
    }

    .owl-carousel-navigation .prev,
    .owl-carousel-navigation .next
    {
        margin-top: -365px;
        z-index: 2;
        position: absolute;
    }

    .owl-carousel-navigation a.prev
    {
        left: 0;
        margin-left: 210px;
    }

    .owl-carousel-navigation a.next
    {
        right: 0;
        margin-right: 210px;
    }

    .owl-carousel-navigation a.prev img,
    .owl-carousel-navigation a.next img
    {
        height: 75px;
    }

    .owl-theme .owl-controls
    {
        display: none;
    }

    .imovel-cabecalho
    {
        font-style: italic;
        background: rgba(226,226,226,1);
        background: -moz-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(255,255,255,1) 0%, rgba(228, 228, 228,1) 50%, rgba(216, 216, 216,1) 51%, rgba(254,254,254,1) 100%);
        background: -webkit-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(255,255,255,1) 0%, rgba(228, 228, 228,1) 50%, rgba(216, 216, 216,1) 51%, rgba(254,254,254,1) 100%);
        background: -o-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(255,255,255,1) 0%, rgba(228, 228, 228,1) 50%, rgba(216, 216, 216,1) 51%, rgba(254,254,254,1) 100%);
        background: -ms-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(255,255,255,1) 0%, rgba(228, 228, 228,1) 50%, rgba(216, 216, 216,1) 51%, rgba(254,254,254,1) 100%);
        background: linear-gradient(to bottom, rgba(226,226,226,1) 0%, rgba(255,255,255,1) 0%, rgb(228, 228, 228) 50%, rgb(216, 216, 216) 51%, rgba(254,254,254,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0 );
        border-radius: 2px;
        padding-top: 10px;
        padding-bottom: 10px;
        -webkit-box-shadow: -1px 7px 23px 0px rgba(0,0,0,0.52);
        -moz-box-shadow: -1px 7px 23px 0px rgba(0,0,0,0.52);
        box-shadow: -1px 7px 23px 0px rgba(0, 0, 0, 0.52);
        margin-bottom: 25px;
    }

    .imovel-cabecalho h1
    {
        font-weight: 700;
        margin-top: 0px;
        font-size: 27px;
    }

    #section-fotos a img
    {
        width: 100%;
        height: 86px;
    }

</style>
<script>
    (function() {

        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });

    })();

    $(document).ready(function(){
        $('#imagens-principais-carousel').owlCarousel({
            center: true,
            autoplay: true,
            autoWidth: true,
            items: 1,
            loop: true,
            margin: 0,
            responsive:{
                0 : {
                    items: 1,
                    center: false
                },
                1200:{
                    items: 1.47
                }
            }
        });

        /*$('#imagens-imovel-carousel').owlCarousel({
            items: 4,
        });*/

        $(".owl-carousel-navigation .next").click(function(){
            $('#imagens-principais-carousel').trigger('next.owl.carousel');
        });
        $(".owl-carousel-navigation .prev").click(function(){
            $('#imagens-principais-carousel').trigger('prev.owl.carousel');
        });
    });
</script>