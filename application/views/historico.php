<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $data['busca_rapida'] = false; ?>
<? $this->load->view('templates/filtro-rapido', $data); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/condominios-lancamentos/lista.css'); ?>">

<div class="container">
    <h1>HISTÓRICO</h1>
    <p>Aqui você pode ver seu histórico de imóveis visualizados, separados por dia.</p>

    <? require_once(MODULESPATH . '/simples/helpers/valor_imovel_formater_helper.php'); ?>

    <? $dia_visualizacao = date("Y-m-d"); $hoje_add = false; ?>

    <? if(count($dias) > 0) : ?>

        <? foreach($dias as $dia => $imoveis) : ?>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#<?= 'dia-'. $dia; ?>"><?= ((new DateTime($dia))->format('Y-m-d') ==  date("Y-m-d")) ? 'Hoje' : (new DateTime($dia))->format('d/m/Y') ; ?></a>
                        </h4>
                    </div>
                    <div id="<?= 'dia-'. $dia; ?>" class="panel-collapse collapse">
                        <div class="panel-body">

                            <? foreach ($imoveis as $imovel) : ?>
                                <a href="<?= base_url('imovel?id=' . $imovel->id); ?>">
                                    <div class="imovel col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>">
                                            </div>
                                            <div class="col-md-8">
                                                <h3 class="no-margin-top"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></h3>
                                                <p><?= $imovel->cidade; ?></p>
                                                <b><small>Valor do imóvel</small></b>
                                                <h3 class="no-margin-top"><small><b>R$</b></small> <?= format_valor_miniatura($imovel); ?></h3>
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            <? endforeach; ?>
                        </div>
                        <div class="panel-footer"><?= count($imoveis). ' imóveis visualizados';?></div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    <? else : ?>

        <div class="alert alert-info fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Infomação!</strong> Nenhum imóvel visualizado ainda.
        </div>

    <? endif; ?>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>
