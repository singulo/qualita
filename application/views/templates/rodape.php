<div class="rodape col-xs-12" style="background-image: url('<?= base_url('assets/images/bg-rodape.png'); ?>'); background-size: cover!important;">
    <div class="container">
        <div class="info-imobiliaria">
            <div class="hidden-xs col-md-6 pull-left">
                <img src="<?= base_url('assets/images/logo-qualita-imoveis.png'); ?>">
            </div>
            <div class="col-md-6 text-right">
                <em>
                    <h3>Venha nos visitar</h3>
                    <p><?= $_SESSION['filial']['endereco']; ?> <br> <?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?> <br> <br> <?= $_SESSION['filial']['telefone_1']; ?> <br> <?= $_SESSION['filial']['email_padrao']; ?></p>
                </em>
            </div>
        </div>
    </div>
</div>
<div class="direitos-simples col-xs-12">
    <div class="container">
        <div class="text-right">
            <a href="http://www.simplesimob.com.br/">
                <img src="<?= base_url('assets/images/simplesimob-logo.png'); ?>" class="img-responsive pull-right" alt="Simples Imob" style="padding-left: 10px; margin-top: -4px;">
            </a>
            <p>Copyright <?= date('Y'); ?> | Todos os direitos reservados, desenvolvido por Singulo Comunicação | Site integrado ao sistema Simples Imob </p>
        </div>
    </div>
</div>

<style>
    .rodape {
        background-repeat: repeat-x;
        padding-bottom: 40px;
        padding-top: 65px;
    }

    .rodape .info-imobiliaria
    {
        color: #fff;
    }

    .rodape img
    {
        height: 150px;
    }

    .direitos-simples
    {
        padding-top: 14px;
        padding-bottom: 5px;
        font-size: 10px;
    }
</style>