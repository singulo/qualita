<? if( ! isset($filtro))
{
    $filtro = new stdClass();
    $filtro->preco_min = 0;
    $filtro->id = "";
    $filtro->finalidade = array();
    $filtro->preco_max = 3000000;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->dormitorios = NULL;
    $filtro->garagem = NULL;
    $filtro->banheiros = NULL;
    $filtro->suites = NULL;
}
?>
<? ob_start();
require_once MODULESPATH . 'simples/helpers/form_values_helper.php';
ob_end_clean();
?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/filtro-rapido/filtro-rapido.css'); ?>">

<div class="filtro-imovel">
    <div class="container">
        <div class="hidden-xs col-md-3">
            <h2>
                REALIZE <br> <strong>O SEU </strong> SONHO <br> COM A <strong>QUALITÀ</strong>
            </h2>
        </div>
        <form id="form-pesquisar" class="form-filtro col-md-9" action="<?= base_url_filial('imovel/pesquisar')?>">
            <input type="hidden" name="id">
            <div class="col-md-4">
                <select name="cidade" class="selectpicker" data-live-search="true" title="CIDADE" multiple>
                    <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                        <? if(strlen($cidade->cidade) > 0 ) :?>
                            <option value="<?= $cidade->cidade; ?>"  <? if(select_value($cidade, $filtro->cidade)) echo 'selected'; ?>><?= $cidade->cidade; ?></option>
                        <? endif; ?>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="col-md-4">
                <select name="id_tipo" class="selectpicker" data-live-search="true" title="DORMITÓRIO/TIPO" multiple>
                    <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                        <option value="<?= $tipo->id; ?>"  <? if(select_value($tipo, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="col-md-4">
                <input type="hidden" name="preco_min">
                <input type="hidden" name="preco_max">
                <!--                <input type="text" class="slider-filtro span2" value="" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[--><?//= isset($filtro->preco_min) ? $filtro->preco_min : 0;?><!--,--><?//= isset($filtro->preco_min) ? $filtro->preco_max : 3000000;?><!--]" data-slider-ticks-labels='["R$0", "R$3.000.000"]'/>-->
                <label class="filtro-valores"><small>R$</small> <?= isset($filtro) ?  number_format($filtro->preco_min, 2, ',', '.') : '0,00'; ?> até <small>R$</small><?= isset($filtro) ?  number_format($filtro->preco_max, 2, ',', '.') : '3.000.000,00+'; ?></label>
                <input type="text" class="slider-filtro span2" value="" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[<?= isset($filtro) ? $filtro->preco_min : '0'; ?>, <?= isset($filtro) ? $filtro->preco_max : '3000000'; ?>]"/>
            </div>
            <div class="col-md-12">
                <button type="button" onclick="pesquisar(0);" class="btn btn-info btn-lg">PROCURAR IMÓVEL</button>
            </div>
        </form>
    </div>
    <? if(!isset($busca_rapida) || $busca_rapida !== FALSE) : ?>
        <nav class="navbar navbar-default filtro-rapido">
            <div class="container text-center">
                <ul>
                    <li><button type="button" onclick="window.location.href = '<?= base_url('imovel/pesquisar?id=&id_tipo%5B%5D=2'); ?>'">APARTAMENTOS</button></li>
                    <li><button type="button" onclick="window.location.href = '<?= base_url('imovel/pesquisar?id=&id_tipo%5B%5D=1'); ?>'">CASAS</button></li>
                    <li><button type="button" onclick="window.location.href = '<?= base_url('imovel/pesquisar?id=&id_tipo%5B%5D=5'); ?>'">DUPLEX</button></li>
                    <li><button type="button" onclick="window.location.href = '<?= base_url('imovel/pesquisar?id=&id_tipo%5B%5D=6'); ?>'">SOBRADOS</button></li>
                    <li><button type="button" onclick="window.location.href = '<?= base_url('imovel/pesquisar?id=&id_tipo%5B%5D=4'); ?>'">TERRENOS</button></li>
                </ul>
            </div>
        </nav>
    <? endif; ?>
</div>
<script>
    var filtro = <?= json_encode($filtro); ?>;
</script>