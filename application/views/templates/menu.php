<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/menu/menu.css'); ?>">

<? if(!$this->session->has_userdata('usuario')) : ?>
    <? $this->load->view('cliente/login'); ?>
    <? $this->load->view('cliente/cadastrar'); ?>
<? else : ?>
<? $this->load->view('cliente/dados'); ?>
<? endif; ?>

<div class="menu-superior">
    <div class="container">
        <div class="hidden-xs hidden-sm col-md-3 pull-left"><em><incc data-text="INCC-M / {data}: {valor}">INCC-M / ...</incc></em></div>
        <div class="col-xs-12 col-md-5 pull-right text-right">
<!--            <a href="--><?//= base_url('quem-somos'); ?><!-- "><img src="--><?//= base_url('assets/images/icon-star.png'); ?><!--"> Conheça a Qualità</a>-->
            <a href="<?= base_url('contato'); ?>"><img src="<?= base_url('assets/images/icon-email.png'); ?>"> Contato</a>
            <? if($this->session->has_userdata('usuario')) : ?>
                <a href="#modal-dados" data-toggle="modal"><img src="<?= base_url('assets/images/icon-login.png'); ?>"> <?= $this->session->userdata('usuario')->nome; ?></a>
            <? else : ?>
                <a href="#modal-login" data-toggle="modal"><img src="<?= base_url('assets/images/icon-login.png'); ?>"> Entrar/Cadastrar</a>
            <? endif; ?>
        </div>
    </div>
</div>
<div class="menu-imovel">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="<?= base_url(); ?>"><img src="<?= base_url('assets/images/logo-qualita-imoveis-horizontal.png'); ?>"></a>
            </div>
            <div class="col-md-5 text-right">
                <ul>
                    <li><a href="<?= base_url('imovel/pesquisar'); ?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lancamentos'); ?>">LANÇAMENTOS</a></li>
                    <li><a href="<?= base_url('condominio/lista'); ?>">CONDOMÍNIOS</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <form action="<?= base_url('imovel/pesquisar'); ?>" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="código do imóvel" name="id">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="hidden-xs col-md-2">
                <? if($this->session->has_userdata('usuario')) : ?>
                <a href="<?= base_url('historico'); ?>">
                    <? else : ?>
                    <a href="#modal-login" data-toggle="modal">
                        <? endif; ?>
                        <div class="col-xs-6 col-md-4">
                            <h1><? if(isset($_SESSION['quantidade_imoveis_visualizados'])) echo $_SESSION['quantidade_imoveis_visualizados']; else echo 0; ?></h1>
                        </div>
                        <div class="col-xs-6" style="margin-top: 10px;">
                            <span style="line-height: 5px;">imóveis <br> <small>visualizados</small></span>
                        </div>
                    </a>
            </div>
        </div>
    </div>
</div>

