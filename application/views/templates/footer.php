    </body>

    <!--  MODERNIZR  -->
    <script type="text/jscript" src="<?= base_url('assets/js/modernizr.js'); ?>" ></script>

    <script type="text/jscript" src="<?= base_url('assets/plugins/bootstrap-3.3.6/js/bootstrap.min.js'); ?>" ></script>

    <!--   MULTISELECT  -->
    <script type="text/jscript" src="<?= base_url('assets/plugins/bootstrap-select/bootstrap-select.min.js'); ?>" ></script>

    <!--   VALUE SLIDER  -->
    <script type="text/jscript" src="<?= base_url('assets/plugins/bootstrap-slider/bootstrap-slider.js'); ?>" ></script>

    <!--  JSONIFY  -->
    <script type="text/jscript" src="<?= base_url('assets/simples/js/jsonify.js'); ?>"></script>

    <!--FANCYBOX-->
    <script src="<?= base_url('assets/plugins/fancybox/jquery.fancybox.pack.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/fancybox/jquery.mousewheel-3.0.6.pack.js'); ?>"></script>

    <!-- OWL CAROUSEL -->
    <script src="<?= base_url('assets/plugins/owl-carousel/owl.carousel.js'); ?>"></script>

    <!--  AUTONUMERIC  -->
    <script type="text/jscript" src="<?= base_url('assets/js/autoNumeric.min.js'); ?>"></script>

    <!-- MASK INPUT -->
    <script type="text/jscript" src="<?= base_url('assets/js/jquery.maskedinput.js'); ?>"></script>

    <!-- ALERTIFY -->
    <script src="<?= base_url('assets/plugins/alertify/js/alertify.js'); ?>"></script>

    <!--  PAGINATION  -->
    <script type="text/javascript" src="<?= base_url('assets/admin/js/jquery.simplePagination.js'); ?>"></script>

    <!-- VALIDATION -->
    <script src="<?= base_url('assets/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>

    <!--  SIMPLES  -->
    <script type="text/jscript" src="<?= base_url('assets/simples/js/simples.js'); ?>"></script>

    <!--  IMOVEL  -->
    <script type="text/jscript" src="<?= base_url('assets/simples/js/imovel.js'); ?>"></script>

    <!--  CLIENTE  -->
    <script type="text/jscript" src="<?= base_url('assets/simples/js/cliente.js'); ?>"></script>

    <!--  LOGIN CADASTRO CLIENTE  -->
    <script type="text/jscript" src="<?= base_url('assets/js/cliente/login.js'); ?>"></script>
    <script type="text/jscript" src="<?= base_url('assets/js/cliente/cadastro.js'); ?>"></script>
    <script type="text/jscript" src="<?= base_url('assets/js/cliente/dados.js'); ?>"></script>

    <!--  FILTRO  -->
    <script type="text/jscript" src="<?= base_url('assets/js/filtro.js'); ?>"></script>

    <!--  CONTATO  -->
    <script type="text/jscript" src="<?= base_url('assets/js/contato.js'); ?>"></script>

    <!-- INCC do gerenciador de conteudos -->
    <script type="text/jscript" src="<?= base_url('assets/js/jquery.cookie.js'); ?>"></script>
    <script type="text/jscript" src="<?= base_url('assets/js/incc.js'); ?>"></script>

    <!--  CUSTOM  -->
    <script type="text/jscript" src="<?= base_url('assets/js/custom.js'); ?>" ></script>

    <script>
        // JS start | Multiselect -->
        $('.selectpicker').selectpicker({
            style: 'btn-multselect',
            //size: 44
        });
    </script>

<!--Google Analytics-->

    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107478848-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments)};
        gtag('js', new Date());

        gtag('config', 'UA-107478848-1');
    </script>



</html>