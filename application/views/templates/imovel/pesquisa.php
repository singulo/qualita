<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/filtro-rapido/filtro-rapido.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/imovel/pesquisa/pesquisa.css'); ?>">

<div class="filtro-imovel col-xs-12">
    <div class="container">
        <div class="col-md-3">
            <h2>
                REALIZE <br> <strong>O SEU </strong> SONHO <br> COM A <strong><br>QUALITÀ</strong>
            </h2>
        </div>
        <form class="form-filtro col-md-9" action="<?= base_url('imovel/pesquisar'); ?>" method="GET">
            <div class="col-md-4">
                <select name="filtro-cidades[]" class="selectpicker" data-live-search="true" title="CIDADE">
                    <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                        <option value="<?= $cidade->f_cidade; ?>"><?= $cidade->f_cidade; ?></option>
                    <? endforeach; ?>
                </select>
            </div>

            <div class="col-md-4">
                <input type="number" name="dormitorios" class="form-control" placeholder="QUARTOS" min="0">
            </div>

            <div class="col-md-4">
                <input type="number" name="suites" class="form-control" placeholder="SUÍTES" min="0">
            </div>
            <div class="col-md-4">
                <input type="number" name="banheiros" class="form-control" placeholder="BANHEIROS" min="0">
            </div>

            <div class="col-md-4">
                <select name="complementos" class="selectpicker" data-live-search="true" title="COMPLEMENTOS" multiple>
                    <option value="">Lareira</option>
                    <option value="">Mobiliado</option>
                </select>
            </div>

            <div class="col-md-4">
                <input type="hidden" name="filtro-preco-min">
                <input type="hidden" name="filtro-preco-max">
                <input type="text" class="slider-filtro span2" value="" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[0,3000000]" data-slider-ticks-labels='["R$0", "R$3.000.000"]'/>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-info btn-lg">PROCURAR IMÓVEL</button>
            </div>
        </form>
    </div>
</div>