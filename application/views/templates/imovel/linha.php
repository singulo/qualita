<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/imovel/linha/linha.css'); ?>">

<!-- IMOVEIS_LINHA_TITULO -->
<div class="imoveis-linha container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <img src="<?= base_url('assets/images/arrow-up.png'); ?>" class="img-responsive">
                <?= $imoveis_linha_titulo; ?>
                <hr>

                <div class="pull-right">
                    <button type="button" class="btn btn-default prev" aria-label="Left Align">
                        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default next" aria-label="Left Align">
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
        </div>
        <div class="row owl-imoveis-linha">
            <? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
            <? require_once MODULESPATH . 'simples/libraries/CondominioTipos.php'; ?>
            <? foreach($imoveis_linha as $condominio) : ?>
                <div class="imovel item">
                    <? if(isset($eh_imovel) && $eh_imovel == TRUE) : ?>
                        <a href="<?= base_url('imovel?id=' . $condominio->id); ?>">
                            <figure>
                                <img src="<?= $_SESSION['filial']['fotos_imoveis'] . $condominio->foto; ?>" class="img-responsive" onerror="this.src='<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                                <figcaption>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4><?= $_SESSION['filial']['tipos_imoveis'][$condominio->id_tipo]->tipo; ?><br><small><?= format_valor($condominio->valor, 'R$'); ?></small></h4>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span class="glyphicon glyphicon-map-marker"></span> <?= $condominio->cidade; ?></p>
                                            <p><span class="glyphicon glyphicon-bed"></span> <?= $condominio->suites; ?> suites</p>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </a>
                    <? else : ?>
                        <a href="<?= base_url('condominio?id=' . $condominio->id); ?>">
                            <figure>
                                <img src="<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>" class="img-responsive" onerror="this.src='<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                                <figcaption>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4><?= $condominio->nome; ?></h4>
                                        </div>
                                        <div class="col-md-6">
                                            <h4><span class="glyphicon glyphicon-map-marker"></span> <?= $condominio->cidade; ?></h4>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </a>
                    <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

<script type="text/jscript" src="<?= base_url('assets/pages/templates/imovel/linha/linha.js'); ?>"></script>