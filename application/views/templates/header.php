<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Qualitá Imóveis - Imobiliária em Capão da Canoa / RS</title>
    <meta name="description" content="Venda de Imóveis em Capão da Canoa, Atlântida e praias de Xangri-lá. Venda de Apartamentos, Casas e Sobrados em condomínios fechados.">
    <meta name="keywords" content="imoveis em capão da canoa, imobiliaria capao da canoa, imobiliaria capão da canoa, imoveis capao da canoa, imobiliarias em capão da canoa, imobiliárias em capão da canoa rs, imoveis em capao da canoa, apartamento em capão da canoa, imobiliarias capão da canoa rs, apartamento capao da canoa, imobiliarias de capão da canoa, apartamentos a venda em capão da canoa rs, casas em capão da canoa, imobiliarias de capao da canoa rs, capão da canoa imobiliarias, casas a venda em capão da canoa, apartamentos a venda em capao da canoa, imoveis a venda em capão da canoa rs, capao da canoa imoveis">

    <!--    <link rel="icon" href="--><?//= base_url('assets/images/favicon.ico'); ?><!--" type="image/x-icon">-->

    <!-- CSS Bootstrap | Arquivos padrões -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/bootstrap-3.3.6/css/bootstrap.min.css'); ?>">

    <!--  BOOTSTRAP MULTSELECT -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/bootstrap-select/bootstrap-select.min.css'); ?>">

    <!--  BOOTSTRAP VALOR SLIDER  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/bootstrap-slider/bootstrap-slider.css'); ?>">

    <!--  ALERTIFY  -->
    <link href="<?= base_url('assets/plugins/alertify/css/alertify.css'); ?>" rel="stylesheet" type="text/css"/>

    <!--  PAGINATION  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/simplePagination/simplePagination.css'); ?>">

    <!-- FANCYBOX -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/fancybox/jquery.fancybox.css'); ?>">

    <!-- OWL CAROUSEL -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/owl-carousel/assets/owl.carousel.css'); ?>">
<!--    <link rel="stylesheet" href="--><?//= base_url('assets/plugins/owl-carousel/assets/owl.theme.css'); ?><!--">-->

    <!--  CUSTOM  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/custom.css'); ?>">

    <!-- font-awesome -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>">

    <!--    JS -->
    <script type="text/jscript" src="<?= base_url('assets/js/jquery-2.2.0.min.js'); ?>" ></script>

    <!--  Font Google  -->
    <link href='https://fonts.googleapis.com/css?family=Istok+Web:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<!--    <link rel="apple-touch-icon" href="--><?//= base_url('assets/images/icones-mobile/Icon-Small-50@2x.png'); ?><!--">-->
    <body id="page-wrap">

    <? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>
    <input id="base_url" type="hidden" value="<?= base_url()?>">
    <input id="filial_fotos_imoveis" type="hidden" value="<?= $_SESSION['filial']['fotos_imoveis']; ?>">
    <input id="usuario" type="hidden" value='<?= json_encode($this->session->userdata('usuario')); ?>'>
    <input id="filial_sessao" type="hidden" value="<? if(isset($_SESSION['filial']['chave'])) {echo  $_SESSION['filial']['chave'];}?>">

    <? $this->load->library('simples/Finalidades'); ?>

    <script>
        var imoveis_tipos = <?= json_encode($_SESSION['filial']['tipos_imoveis']); ?>;
        var finalidades = <?= json_encode(array_flip(Finalidades::getConstants())); ?>
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '300947127075013');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=300947127075013&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->