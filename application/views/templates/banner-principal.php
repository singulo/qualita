<div id="banner-principal" class="hidden-xs hidden-sm">
    <? foreach($_SESSION['filial']['banners'] as $banner) : ?>
        <div class="item" style="background-image: url(<?= $_SESSION['filial']['banners_uri'] . $banner->id . '.jpg'; ?>);">

        </div>
    <? endforeach; ?>
</div>

<style>
    #banner-principal .owl-controls
    {
        margin-top: 0px;
    }

    #banner-principal div.item
    {
        height: 480px;
        background-size: 100%;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<script>
    $(document).ready(function() {
        $('#banner-principal').owlCarousel({
            items: 1,
            loop: $('#banner-principal div.item').length > 1,
            margin: 0,
            dots : false
        });
    });

</script>

