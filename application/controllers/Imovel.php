<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "../modules/simples/controllers/Base_imovel_controller.php";

class Imovel extends Base_imovel_controller
{
    protected $quantidade_fotos_principais = 3;
    protected $quantidade_sugestoes = 4;

    public function index()
    {
        $data = parent::index();

        if($this->session->has_userdata('usuario') && isset($_SESSION['quantidade_imoveis_visualizados']))
        {
            $_SESSION['quantidade_imoveis_visualizados'] ++;
        }else if($this->session->has_userdata('usuario') && (! isset($_SESSION['quantidade_imoveis_visualizados'])))
        {
            $this->load->model('simples/clientes_imoveis_log_model');
            $_SESSION['quantidade_imoveis_visualizados'] = count($this->clientes_imoveis_log_model->imoveis_visualizados($this->session->userdata('usuario')->email));
            $_SESSION['quantidade_imoveis_visualizados'] ++;
        }

        $this->load->view('imovel/detalhes', $data);
    }

    public function pesquisar()
    {
        $data = parent::pesquisar();

        if(is_null($data['filtro']->preco_min) || $data['filtro']->preco_min == '')
            $data['filtro']->preco_min = 0;

        if(is_null($data['filtro']->preco_max) || $data['filtro']->preco_max == '')
            $data['filtro']->preco_max = 3000000;



        $this->load->view('imovel/pesquisa', $data);
    }

    public function buscar()
    {
        $data = parent::buscar();
        $data['status'] = true;

        echo json_encode($data);
    }

    public function obter_fotos()
    {
        if(!$this->session->has_userdata('usuario'))
            echo json_encode(array('status' => false, 'msg' => 'Usuário não logado'));
        else if(isset($_GET['cod_imovel']))
        {
            $this->load->model('simples/imovel_model');

            echo json_encode(array('status' => true, 'fotos' => $this->imovel_model->fotos($_GET['cod_imovel'])));
        }
        else
            echo json_encode(array('status' => false, 'msg' => 'Código do imóvel não informado.'));
    }

    public function condominios()
    {
        $data = parent::condominios();

        /*foreach($data['condominios'] as $condominio)
        {
            $data['cidades'][$condominio->f_cidade][] = $condominio;
        }*/

        $this->load->view('condominio/lista', $data);
    }

    public function lancamentos()
    {
        $data = parent::lancamentos();

        $this->load->view('lancamento/lista', $data);
    }
}
