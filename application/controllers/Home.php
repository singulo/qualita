<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Imovel_Model imovel_model
 */
class Home extends Base_Controller {

	public function index()
	{
		//var_dump($_SESSION['filial']['chave']); die;

		$this->load->model('simples/imovel_model');

		$data['destaques'] = $this->imovel_model->destaques(6);

		//var_dump($data['destaques'][0]->foto); die;

		$this->load->view('home', $data);
	}
}
