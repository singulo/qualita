<?php


function armazena_filial_em_sessao()
{
    $CI = get_instance();

    $filiais = $CI->config->item('filiais');
    $CI->session->set_userdata('filial', $filiais[$_GET['filial']]);

    $CI->load->model('admin/imobiliaria_model');
    $imobs = $CI->imobiliaria_model->listar(0,1);

    if(isset($imobs[0]))
        $CI->session->set_userdata('filial',  ((array)$imobs[0] + $CI->session->userdata('filial')));

    $CI->load->model('admin/banner_model');
    $_SESSION['filial']['banners'] = $CI->banner_model->listar_todos();

    $CI->load->model('admin/imoveis_model');

    $cod_imoveis = array();
    foreach ($_SESSION['filial']['banners'] as $banner)
        $cod_imoveis[] = $banner->codigo_imovel;

    /** @var Imoveis_Model $imoveis_model */
    $imoveis = $CI->imoveis_model->pelos_codigos($cod_imoveis);

    foreach($imoveis as $imovel)
    {
        foreach($_SESSION['filial']['banners'] as $banner)
        {
            if($banner->codigo_imovel == $imovel->f_codigo)
                $banner->imovel = $imovel;
        }
    }

    //CIDADES
    $_SESSION['filial']['cidades_imoveis'] = $CI->imoveis_model->consulta_cidades_imoveis();
    //TIPOS DOS IMÓVEIS
    $_SESSION['filial']['tipos_imoveis'] = $CI->imoveis_model->consulta_tipos_imoveis();

    //ARMAZENADO EM
    $_SESSION['filial']['armazenada_em'] = date('Y-m-d H:i:s');

    //CORRETORES
    $CI->load->model('admin/corretores_model');

    foreach($CI->corretores_model->listar_todos() as $corretor)
        $_SESSION['filial']['corretores'][$corretor->id_corretor] = $corretor;

    //PARA ADICIONAR FUNCIONALIDADES ESPECIFICAS DE CADA SITE NECESSÁRIAS AO CARREGAMENTO
    if(file_exists(APPPATH."helpers/carregar_na_filial_helper.php"))
    {
        $CI->load->helper('carregar_na_filial_helper');
        if (function_exists('carregar_na_filial'))
            carregar_na_filial();
    }

    $_SESSION['filial_usuario'] = $_SESSION['filial'];
}