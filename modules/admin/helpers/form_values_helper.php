﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function form_set_value($obj, $property, $default = '')
	{
		return (isset($obj->$property)) ? $obj->$property : $default;
	}

	function form_checkbox_set_value($obj, $property)
	{
		return (isset($obj->$property) && $obj->$property == true) ?
			'checked' : '';
	}

	function form_text_set_value($value, $default = '')
	{
		return (isset($value)) ? $value : $default;
	}

	function form_select_options(array $options, $valueSelected = '')
	{
		$selectOptions = '';

		foreach($options as $key => $value)
		{
			$selected = '';
			if($value == $valueSelected)
				$selected = 'selected';

			$selectOptions .= "<option value='$value' $selected >$key</option>";
		}

		return $selectOptions;
	}

	function form_options_month()
	{
		$selectOptions = '';

		foreach(get_months() as $key => $month)
		{
			$selectOptions .= "<option value=\"" . $key . "\">" . $month . "</option>";
		}

		return $selectOptions;
	}

	function get_months()
	{
		return  array ( 1 => 'Janeiro', 2 => 'Fevereiro',3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
	}