<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('format_valor'))
{

    function format_valor($valor, $cifrao = '')
    {
		if($valor == '1.00' || $valor == '0.00'){
			return 'Consulte';
		}
		else{
			$valor = number_format($valor, 2, ',', '.');

			return $cifrao . $valor;
		}
	}
}
