<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('valor_imovel_formater_helper.php');


function enviar_email($para, $assunto, $corpo, $remetente = '', $copia_remetente = false)
{
    $CI =& get_instance();

    $CI->load->library('admin/F_PHPMailer');

    $mail = new PHPMailer();
    //$mail->SMTPDebug  = 1;
    $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
    $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
    $mail->Host = $_SESSION['filial']['email_sender_host']; //smtp
    $mail->Port = $_SESSION['filial']['email_sender_porta']; //Estabelecemos a porta utilizada pelo servidor de email.
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
    $mail->Username = $_SESSION['filial']['email_sender']; //Usuário do gMail
    $mail->Password = $_SESSION['filial']['email_sender_senha']; //Senha do gMail

    if($remetente == '')
        $remetente = $_SESSION['filial']['email_padrao'];

    $mail->SetFrom($remetente, '=?UTF-8?B?'.base64_encode($_SESSION['filial']['nome']).'?='); //Quem está enviando o e-mail.
    $mail->AddReplyTo($remetente, '=?UTF-8?B?'.base64_encode($_SESSION['filial']['nome']).'?='); //Para que a resposta será enviada.

    $mail->CharSet = "ISO-8859-1";
    $mail->Subject = '=?UTF-8?B?'.base64_encode($assunto).'?='; //Assunto do e-mail.

    $mail->AddBCC($_SESSION['filial']['email_gerente'], $assunto); // Cópia Oculta
    //$mail->AddBCC('register@singulo.com.br', $assunto); // Cópia Oculta

    if($copia_remetente)
        $mail->AddBCC($remetente, 'Cópia oculta - ' . $assunto);

    $mail->Body = utf8_decode($corpo);
    $mail->AddAddress($para);
    return $mail->Send();
}

function monta_corpo_email(array $trocar_palavras, $arquivo, $filial = '', $assinatura_url = NULL)
{
    $CI =& get_instance();

    $arquivo_caminho_completo = !is_null($CI->config->item($arquivo, 'email_corpos')) ? $CI->config->item($arquivo, 'email_corpos') : $CI->config->item('caminho_completo_corpo_emails') . $arquivo . '.html';

    $email_padrao = file_get_contents( !is_null($CI->config->item('email_padrao', 'email_corpos')) ? $CI->config->item('email_padrao', 'email_corpos') : $CI->config->item('caminho_completo_corpo_emails') . 'email_padrao.html');

    if($assinatura_url == NULL)
        $assinatura_url = base_url('assets/images/email/rodape.jpg');

    if($filial == '')
        $filial = strtolower($_SESSION['filial']['chave']);

    $trocar_palavras['imobiliaria_nome'] = $_SESSION['filial']['nome'];

    //SITE LINK
    $trocar_palavras['site_link'] = $_SESSION['filial']['link'];
    //FILIAL NOME
    $trocar_palavras['filial_nome'] = $filial;
    //RODAPE ASSINATURA
    $trocar_palavras['assinatura_url'] = $assinatura_url;
    //CABEÇALHO
    $trocar_palavras['cabecalho_url']  = base_url('assets/images/email/cabecalho.jpg');
    //INSTAGRAM
    $trocar_palavras['instagram_link'] = $_SESSION['filial']['instagram'];
    $trocar_palavras['instagram_icon_url']  = base_url('assets/images/email/icon-instagram.jpg');
    //FACEBOOK
    $trocar_palavras['facebook_link'] = $_SESSION['filial']['facebook'];
    $trocar_palavras['facebook_icon_url']  = base_url('assets/images/email/icon-facebook.jpg');

    $corpo = file_get_contents($arquivo_caminho_completo);
    foreach($trocar_palavras as $palavra => $valor)
    {
        $corpo = str_replace('$' . $palavra . '$', $valor, $corpo);

        $email_padrao = str_replace('$' . $palavra . '$', $valor, $email_padrao);
    }

    return str_replace('$corpo$', $corpo, $email_padrao);
}

function monta_div_imoveis_para_proposta($imoveis)
{
    $CI =& get_instance();
    $CI->load->helper('text');
    $CI->load->helper('url');

    $arquivo = 'imoveis';

    $arquivo_caminho_completo = !is_null($CI->config->item($arquivo, 'email_corpos')) ? $CI->config->item($arquivo, 'email_corpos') : $CI->config->item('caminho_completo_corpo_emails') . $arquivo . '.html';

    $div = file_get_contents($arquivo_caminho_completo);

    $div_imoveis = '';

    foreach($imoveis as $imovel)
    {
        $div_imovel = substr($div, 0);

        $div_imovel = str_replace('$link$', base_url('imovel?cod_imovel=' . $imovel->f_codigo . '&filial=' . strtolower($_SESSION['filial']['chave'])), $div_imovel);
        $div_imovel = str_replace('$src_img$', $_SESSION['filial']['fotos_imoveis'] . $imovel->foto . '.jpg', $div_imovel);
        $div_imovel = str_replace('$valor$', format_valor($imovel->f_valor, 'R$'), $div_imovel);
        $div_imovel = str_replace('$cidade$', $imovel->f_cidade, $div_imovel);
        $div_imovel = str_replace('$tipo$', $imovel->f_tipo, $div_imovel);
        $div_imovel = str_replace('$codigo$', $imovel->f_codigo, $div_imovel);
        $div_imovel = str_replace('$imovel_desc$', character_limiter($imovel->f_descricao, 200), $div_imovel);

        $div_imoveis .= $div_imovel;
    }

    return $div_imoveis;
}
?>