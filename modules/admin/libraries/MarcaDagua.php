<?php

class MarcaDagua
{
    public static function quantidade_arquivos_diretorio($dir)
    {
        $fi = new FilesystemIterator($dir, FilesystemIterator::SKIP_DOTS);
        return  iterator_count($fi);
    }
}