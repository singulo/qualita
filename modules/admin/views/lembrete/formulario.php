<link href="<?= base_url('assets/admin/pages/lembrete/css/formulario.css'); ?>" rel="stylesheet"/>

<div id="modal-lembrete-formulario" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Lembrete</h4>
            </div>
            <div class="modal-body text-center">
                <form onsubmit="novo_lembrete(); return false;">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <input name="titulo" type="text" class="form-control" placeholder="Título" maxlength="25">
                    </div>
                    <div class="form-group">
                        <textarea name="descricao" class="form-control" placeholder="Descrição" style="height:130px; resize: vertical;"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="input-group input-append">
                                    <input name="dia" type="text" class="form-control date-picker" placeholder="Dia">
                                    <span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-xs-5" style="padding-left: 5px;">
                                <div class="input-group input-append bootstrap-timepicker">
                                    <input name="hora" type="text" class="form-control hour-picker" data-minute-step="5" placeholder="Hora">
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="pull-left">Para: </label>
                            </div>

                            <div class="col-xs-12">
                                <select name="destinatarios" data-selected="<?= $this->session->userdata('admin')->id_corretor; ?>" class="select-2 js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%">
                                    <? foreach(ADMNivel::getConstants() as $key => $value) : ?>
                                        <? if(ADMNivel::Adm != $value) : ?>
                                            <optgroup data-nivel="<?= $value; ?>" label="<?= $key; ?>">
                                            </optgroup>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="pull-right">
                            <button type="submit" onclick="novo_lembrete();" class="btn btn-success" data-loading-text="Aguarde..." autocomplete="off">Salvar</button>
                            <button type="button" onclick="marcar_lembrete_como_lido();" class="btn btn-primary">Não lembrar mais</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/admin/pages/lembrete/js/formulario.js'); ?>"></script>