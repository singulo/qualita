<?php $this->load->view('admin/painel'); ?>
<link href="<?= base_url('assets/admin/pages/lembrete/css/lista.css'); ?>" rel="stylesheet"/>

    <div class="page-inner">
        <div id="main-wrapper" class="container">
            <div class="row m-t-md">
                <div class="col-md-12">
                    <div class="row mailbox-header">
                        <div class="col-md-2">
                            <button onclick="abrir_lembrete();" class="btn btn-success btn-block"><span class="glyphicon glyphicon-plus"></span> Lembrete</button>
                        </div>
                        <div class="col-md-6">
                            <h2>Lembretes</h2>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="text" id="pesquisar-campos-contenham" class="form-control input-search" placeholder="Pesquisar...">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="button" style="height: 34px;" onclick="lembretes_pesquisar();"><i class="fa fa-search"></i></button>
                            </span>
                            </div><!-- Input Group -->
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <ul class="list-unstyled mailbox-nav">
                        <li class="active"><a onclick="lembretes_ativos()"><i class="fa fa-toggle-on"></i>Ativos <span class="badge badge-success pull-right" id="quantidade-ativos">0</span></a></li>
                        <li><a onclick="lembretes_desativados()"><i class="fa fa-toggle-off"></i>Desativados <span class="badge badge-success pull-right" id="quantidade-desativados">0</span></a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="panel panel-white">
                        <div class="panel-body mailbox-content">
                            <table class="table" id="tabela-lembretes">
                                <thead>
                                <tr>
                                    <th class="text-right" colspan="4">
                                        <span class="text-muted m-r-sm" id="info-lembretes-encontrados"></span>
                                        <div class="btn-group">
                                            <a class="btn btn-default" id="btn_voltar_lembretes"><i class="fa fa-angle-left"></i></a>
                                            <a class="btn btn-default" id="btn_proximo_lembretes"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->
        </div><!-- Main Wrapper -->
        <div class="page-footer">
            <div class="container">
                <?php $this->load->view('admin/copyright.php'); ?>
            </div>
        </div>
    </div><!-- Page Inner -->

    <!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>
<script src="<?= base_url('assets/admin/pages/lembrete/js/lista.js'); ?>"></script>
