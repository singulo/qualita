
<div class="page-sidebar sidebar horizontal-bar">
    <div class="page-sidebar-inner">
        <ul class="menu accordion-menu">
            <li class="nav-heading"><span>Navegador</span></li>
            <li class="droplink"><a href="<?= base_url('admin'); ?>"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>Painel principal</p></a></li>
            <li class="droplink"><a href="<?= base_url('admin/cliente/lista'); ?>"><span class="menu-icon glyphicon glyphicon-user"></span><p>Clientes</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="<?= base_url('admin/cliente/formulario'); ?>">Cadastrar cliente</a></li>
                    <? if($this->session->userdata('admin')->nivel == ADMNivel::Adm) : ?>
                        <li><a href="<?= base_url('admin/cliente/trocar_corretor'); ?>">Trocar corretor</a></li>
                    <? endif; ?>
                </ul>
            </li>
            <li class="droplink"><a href="<?= base_url('admin/proposta/lista'); ?>"><span class="menu-icon glyphicon glyphicon-send"></span><p>Propostas</p></a>
                <ul class="sub-menu">
                    <li><a href="<?= base_url('admin/proposta/por_perfil'); ?>">Por perfil</a></li>
                </ul>
            </li>
            <? if($this->session->userdata('admin')->nivel <= ADMNivel::Secretaria) : ?>
                <!-- APENAS PARA GERENTE OU ADM -->
                <li class="droplink"><a href="<?= base_url('admin/corretor/'); ?>"><span class="menu-icon glyphicon glyphicon-user"></span><p>Corretores</p><span class="arrow"></span></a>
                    <ul class="sub-menu">
                        <li><a href="<?= base_url('admin/corretor/formulario'); ?>">Cadastrar corretor</a></li>
                    </ul>
                </li>
                <? if($this->session->userdata('admin')->nivel <= ADMNivel::Gerente) : ?>
                    <li>
                        <a href="<?= base_url('admin/relatorio/'); ?>"><span class="fa fa-line-chart"></span> Relatórios</a>
                    </li>
                <? endif; ?>
                <li class="droplink"><a href="#"><span class="menu-icon glyphicon glyphicon-cog"></span><p>Configuração</p><span class="arrow"></span></a>
                    <ul class="sub-menu">
                        <li><a href="<?= base_url('admin/configuracao_banners'); ?>">Banners</a></li>
                        <!-- APENAS PARA ADM -->
                        <? if($this->session->userdata('admin')->nivel <= ADMNivel::Gerente) : ?>
                            <li><a href="<?= base_url('admin/configuracao_meu_negocio'); ?>">Meu negócio</a></li>
                            <li><a href="<?= base_url('admin/meta'); ?>">Meta</a></li>
                            <li class="droplink">
                                <a href="#">
                                    <p>Cliente</p><span class="arrow"></span>
                                </a>
                                <ul class="sub-menu" style="display: none;">
                                    <li><a href="<?= base_url('admin/configuracao/status'); ?>">Status</a></li>
                                    <li><a href="<?= base_url('admin/configuracao/origem'); ?>">Origem</a></li>
                                </ul>
                            </li>
                        <? endif; ?>
                    </ul>
                </li>
            <? endif; ?>
        </ul>
    </div>
</div>