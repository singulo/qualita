<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-title">
        <div class="container">
            <h3>Notificação <small>responder</small></h3>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="col-xs-12 col-md-5 resposta-bloco">
                        <div class="row resposta-corpo-titulo">
                            <b>Corpo</b>
                            <a href="#" onclick="popula_modal_notificacao(notificacao)">Ver contato a ser respondido</a>
                        </div>
                        <div class="summernote">
                            <div style="text-align: center;">
                                <span><?= ($notificacao->tipo == 'normal') ? 'Em resposta ao seu contato.' : 'Em resposta ao seu interesse no imóvel abaixo.';?></span>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success pull-left" onclick="enviar_resposta()" style="margin-top: 10px;"><i class="fa fa-send m-r-xs"></i>Enviar</button>
                        <p class="text-center" style="margin-top: 16px;"><?= 'para <strong>' . $notificacao->nome . '</strong> (<strong>' . $notificacao->email . '</strong>)';  ?></p>
                        <div class="checkbox pull-right">
                            <label><input type="checkbox" checked id="copia_remetente"> Receber cópia oculta</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <h3>Prévia</h3>
                        <div id="resposta_previa"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- Page Inner -->

    <!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script>

    var notificacao = <?= json_encode($notificacao); ?>

    $(document).ready(function() {
        $('.summernote').summernote({
            height: 350,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            onChange: atualiza_resposta_email
        });

        atualiza_resposta_email();
    });

    var atualizando = false;

    function atualiza_resposta_email()
    {
        if(atualizando)
            return;

        atualizando = true;

        alertify.maxLogItems(1)
                .log("Atualizando Prévia...");

        setTimeout( function (){
            $.ajax({
                url: $('#base_url').val() + "admin/notificacao/visualizar_resposta",
                data: {
                    id : notificacao.id,
                    notificacao_tipo: notificacao.tipo,
                    assunto: $(".summernote").code()
                },
                type: "GET",
                dataType: "html",
                success: function (data) {
                    $('#resposta_previa').html(data);
                },
                error: function (xhr, status) {
                    alertify.error("Ocorreu um erro ao atualizar a Prévia!");
                },
                complete: function (xhr, status) {
                    atualizando = false;
                    alertify.success("Prévia Atualizada");
                }
            });
        }, 2500);
    }

    function enviar_resposta()
    {
        alertify
            .okBtn("Sim")
            .cancelBtn("Não")
            .confirm("Tem certeza que deseja enviar está resposta para " + notificacao.email + "?", function () {

                alertify.maxLogItems(1)
                    .log("Enviando sua resposta, aguarde...");

                $.ajax({
                    url: $('#base_url').val() + "admin/notificacao/enviar_resposta",
                    data: {
                        id : notificacao.id,
                        notificacao_tipo: notificacao.tipo,
                        assunto: $(".summernote").code(),
                        copia_remetente: $("#copia_remetente").prop('checked')
                    },
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        if (data.status){
                            alertify.success("Resposta enviada.");
                            setTimeout(function () {
                                window.location.href = $('#base_url').val() + 'admin/notificacao/lista';
                            }, 1500);
                        }
                        else
                            alertify.error("Ocorreu um erro ao enviar a resposta, tente novamente mais tarde.");
                    },
                    error: function (xhr, status) {
                        alertify.error("Ocorreu um erro ao enviar a resposta!");
                    }
                });

        }, function() {
            // user clicked "cancel"
        });
    }

</script>

<style>

    .resposta-bloco
    {
        border-right: 1px solid rgba(0, 0, 0, 0.4);
    }

    #resposta_previa
    {
        overflow: auto;
    }

    .resposta-corpo-titulo
    {
        margin-top: 20px;
        margin-left: 0px;
    }

    .resposta-corpo-titulo a
    {
        float: right;
        margin-right: 15px;
    }

    .resposta-corpo-titulo b
    {
        font-size: 16px
    }
</style>
