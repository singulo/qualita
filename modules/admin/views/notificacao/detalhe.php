<div id="notificacao-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="notificacaoModalLabel">Notificação</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <h3>Dados do cliente</h3>
                    <p><b>Nome:</b> <span id="notificacao_modal_nome"></span></p>
                    <p><b>Email:</b> <span id="notificacao_modal_email"></span></p>
                    <p><b>Telefone:</b> <span id="notificacao_modal_telefone"></span></p>
                </div>
                <div id="notificacao_tipo_normal" class="col-md-6" style="display: none">
                    <h3>Assunto</h3>
                    <p id="notificacao_modal_assunto"></p>
                    <h3>Mensagem</h3>
                    <p id="notificacao_modal_mensagem"></p>
                </div>
                <div id="notificacao_tipo_interesse_em_imovel" class="col-md-6" style="display: none">
                    <h3>Interesse em imóvel</h3>
                    <p><b>Código imóvel:</b> <a href="#" target="_blank" id="notificacao_modal_cod_imovel"></a></p>
                    <h3>Observação</h3>
                    <div class="col-md-12">
                        <div class="col-md-8 text-left"><p id="notificacao_modal_obs"></p></div>
                    </div>
                </div>
                <small>
                    <span class="text-left pull-left" id="notificacao_modal_criado_em"></span>
                    <span class="text-right pull-right" id="notificacao_modal_respondido_em"></span>
                </small>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="row">
                        <div id="notificacao-botoes" class="pull-right">
                            <button id="btn-marcar-como-nao-respondida" onclick="notificacao_marcar_como_nao_respondida()" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Marcar como não respondido</button>
                            <button onclick="ir_para_responder()" class="btn btn-warning botoes-resposta"><i class="fa fa-pencil-square-o"></i> Responder</button>
                            <button type="button" class="btn btn-success botoes-resposta" onclick="notificacao_marcar_como_respondida();"><i class="fa fa-check"></i> Respondido</button>
                            <button id="btn-fecha-modal-notificacao" type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-notificacao-corretor" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Notificação</h4>
            </div>
            <div class="modal-body text-center">
                <p class="mensagem">Um cliente foi vinculado a você.</p>
                <a class="link_perfil" href="#">Ir para o perfil do cliente.</a>
                <small>
                    <span class="text-left pull-left criado_em">enviado: 22/06/16 09:31</span>
                    <span class="text-right pull-right respondido_em">resposta: 22/06/16 09:37</span>
                </small>
            </div>
            <div class="modal-footer">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary marcar_notificacao_nao_lida" onclick="notificacao_marcar_como_nao_respondida()" ><i class="fa fa-check-square-o"></i> Marcar como não lido</button>
                            <button type="button" class="btn btn-success marcar_notificacao_lida" onclick="notificacao_marcar_como_respondida();"><i class="fa fa-check"></i> Lida</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var notificacoes_atualizadas = {
        successCallback : []
    };

    function notificacao_marcar_como_respondida()
    {
        alertify
            .cancelBtn("Cancelar")
            .confirm("Você deseja marcar como respondido está notificação?"
            , function () {
                    requisicao_notificacao(
                    $('#base_url').val() + 'admin/notificacao/marcar_respondida',
                    function (data) {
                        if(data.status) {
                            alertify.success('Notificação marcada como respondida!');

                            $.each( notificacoes_atualizadas.successCallback, function( key, action ) {
                                action(data.respondido_em);
                            });

                            //FECHA TODAS MODAIS ABERTAS
                            $('.modal').modal('hide');
                        }
                        else
                            alertify.error('Falha ao marcar notificação como respondida');
                    },
                    function (jqXhr, textStatus, errorThrown) {
                        alertify.error('Ocorreu um erro ao marcar notificação como respondida!');
                    });
        });
    }

    function ir_para_responder()
    {
        window.location.href = $('#base_url').val() + 'admin/notificacao/responder?id=' + notificacao_atual.id;
    }

    var notificacao_atual;
    var notificacao_tipo_atual;

    function popula_modal_notificacao(notificacao)
    {
        alertify.log('Obtendo detalhe da notificação...');

        if(notificacao.tipo == 'lembrete')
        {
            abrir_lembrete_pelo_id(notificacao.id);
            return;
        }

        notificacao_atual = notificacao;
        notificacao_tipo_atual = notificacao.tipo;

        $('#notificacao_tipo_normal').hide();
        $('#notificacao_tipo_interesse_em_imovel').hide();

        requisicao_notificacao($('#base_url').val() + 'admin/notificacao/obter',
            function(data){//successCallback
                notificacao_atual = data;

                if(notificacao_tipo_atual == 'cliente_vinculado')
                {
                    $('#modal-notificacao-corretor .mensagem').text(notificacao_atual.mensagem);
                    if(notificacao_atual.id_cliente != null)
                        $('#modal-notificacao-corretor .link_perfil').attr('href', $('#base_url').val() + 'admin/cliente/perfil?id=' + notificacao_atual.id_cliente);
                    else
                        $('#modal-notificacao-corretor .link_perfil').hide();

                    $('#modal-notificacao-corretor .criado_em').text('enviado: ' + $.format.date(notificacao_atual.enviado_em, 'dd/MM/yy hh:mm'));

                    if (notificacao_atual.lida_em != null) {
                        $('#modal-notificacao-corretor .marcar_notificacao_nao_lida').show();
                        $('#modal-notificacao-corretor .marcar_notificacao_lida').hide();
                        $('#modal-notificacao-corretor .respondido_em').text('lida: ' + $.format.date(notificacao_atual.lida_em, 'dd/MM/yy hh:mm'));
                    }
                    else {
                        $('#modal-notificacao-corretor .marcar_notificacao_lida').show();
                        $('#modal-notificacao-corretor .marcar_notificacao_nao_lida').hide();
                        $('#modal-notificacao-corretor .respondido_em').hide();
                    }

                    $('#modal-notificacao-corretor').modal('show');
                }
                else
                {
                    if (notificacao_tipo_atual == 'normal')
                        obter_notificacao_normal(notificacao_atual);
                    else if (notificacao_tipo_atual == 'interesse_em_imovel')
                        obter_notificacao_interesse_em_imovel(notificacao_atual);

                    $('#notificacao_modal_nome').text(notificacao_atual.nome);
                    $('#notificacao_modal_email').text(notificacao_atual.email);
                    $('#notificacao_modal_telefone').text(notificacao_atual.telefone);

                    $('#notificacao_modal_criado_em').text('enviado: ' + $.format.date(notificacao_atual.criado_em, 'dd/MM/yy hh:mm'));

                    if (notificacao_atual.respondido_em != null) {
                        $('#notificacao_modal_respondido_em').text('respondido: ' + $.format.date(notificacao_atual.respondido_em, 'dd/MM/yy hh:mm'));
                        $('#btn-marcar-como-nao-respondida').show();
                        $('.botoes-resposta').hide();
                    }
                    else {
                        $('#btn-marcar-como-nao-respondida').hide();
                        $('.botoes-resposta').show();
                    }

                    //abre a modal
                    $('#notificacao-modal').modal('toggle');
                }
            },
            function(jqXhr, textStatus, errorThrown){//errorCallback
                alertify.error('Erro ao obter detalhes da notificação!');
            }
        );
    }

    function requisicao_notificacao(url, successCallback, failCallback)
    {
        blockUI($('.modal .modal-content'));

        $.ajax({
            url: url,
            type: 'POST',
            data: {id : notificacao_atual.id, tipo: notificacao_tipo_atual},
            dataType: "json",
            success: function (data) {
                successCallback(data);
                $('#notificacao-botoes *').prop("disabled", false);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                failCallback(jqXhr, textStatus, errorThrown);
            },
            complete : function(){
                unblockUI($('.modal .modal-content'));
            }
        });
    }

    function obter_notificacao_interesse_em_imovel(notificacao)
    {
        $('#notificacao_tipo_interesse_em_imovel').show();
        $('#notificacao_modal_cod_imovel').attr('href', $('#base_url').val() + 'imovel?cod_imovel=' + notificacao.cod_imovel + '&filial=' + '<?= strtolower($_SESSION['filial']['chave']);?>');
        $('#notificacao_modal_cod_imovel').text(notificacao.cod_imovel);
        $('#notificacao_modal_obs').text(notificacao.obs);
    }

    function obter_notificacao_normal(notificacao)
    {
        $('#notificacao_tipo_normal').show();
        $('#notificacao_modal_assunto').text(notificacao.assunto);
        $('#notificacao_modal_mensagem').text(notificacao.mensagem);
    }

    function notificacao_marcar_como_nao_respondida()
    {
        requisicao_notificacao($('#base_url').val() + 'admin/notificacao/marcar_como_nao_respondida',
            function(data){//successCallback
                if(data.status) {
                    alertify.success('Notificação marcada como não lida');

                    $.each(notificacoes_atualizadas.successCallback, function (key, action) {
                        action('');
                    });

                    //FECHA TODAS MODAIS
                    $('.modal').modal('hide');
                }
                else
                    alertify.error('Erro ao marcar notificação como não lida, tente novamente mais tarde.');
            },
            function(jqXhr, textStatus, errorThrown){//errorCallback
                alertify.error('Erro ao marcar notificação como não lida!');
            }
        );
    }
</script>