<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div id="main-wrapper" class="container">
        <div class="row m-t-md">
            <div class="col-md-12">
                <div class="row mailbox-header">
                    <div class="col-md-6">
                        <h2>Notificações</h2>
                    </div>
                    <div class="col-xs-2">
                        <select class="form-control m-b-sm" id="filtro-notifcacao-tipo">
                            <option value="">Todos tipos</option>
                            <option value="normal">Normal</option>
                            <option value="interesse_em_imovel">Interesse em imóvel</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" id="pesquisar-campos-contenham" class="form-control input-search" placeholder="Pesquisar...">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="button" style="height: 34px;" onclick="notificacoes_pesquisar();"><i class="fa fa-search"></i></button>
                            </span>
                        </div><!-- Input Group -->
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <ul class="list-unstyled mailbox-nav">
                    <li class="active"><a onclick="notificacoes_nao_respondidas()"><i class="fa fa-question"></i>Não respondidas <span class="badge badge-success pull-right" id="quantidade-nao-respondidas">0</span></a></li>
                    <li><a onclick="notificacoes_respondidas()"><i class="fa fa-check"></i>Respondidas <span class="badge badge-success pull-right" id="quantidade-respondidas">0</span></a></li>
                    <li><a onclick="notificacoes_todas()"><i class="fa fa-asterisk"></i>Todas <span class="badge badge-success pull-right" id="quantidade-total">0</span></a></li>
                </ul>
            </div>
            <div class="col-md-10">
                <div class="panel panel-white">
                    <div class="panel-body mailbox-content">
                        <table class="table" id="tabela-notificacoes">
                            <thead>
                                <tr>
                                    <th class="text-right" colspan="7">
                                        <span class="text-muted m-r-sm" id="info-notificacoes-encontradas"></span>
                                        <div class="btn-group">
                                            <a class="btn btn-default" id="btn_voltar_notificacoes"><i class="fa fa-angle-left"></i></a>
                                            <a class="btn btn-default" id="btn_proximo_notificacoes"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- Row -->
    </div><!-- Main Wrapper -->
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>
<script src="<?= base_url('assets/admin/js/inbox.js'); ?>"></script>

<script>

    notificacoes_atualizadas.successCallback.push(atualizar_quantidade_notificacoes);

    var pagina = 0;

    //Adiciona classe de ativo ao clique
    $('.mailbox-nav li').on('click', function(){

        $('.mailbox-nav li').removeClass('active');

        $(this).addClass('active');
    });

    var trNoficacao;

    notificacoes_atualizadas.successCallback.push(function(respondido_em){
        //ATUALIZA A LISTA, MODIFICADA UM NOTIFICAÇÃO DE RESPONDIDAS OU NÃO RESPONDIDAS
        if($('ul.list-unstyled.mailbox-nav li.active a').text().indexOf('Respondidas') > -1 ||
           $('ul.list-unstyled.mailbox-nav li.active a').text().indexOf('Não respondidas') > -1)
        {
            $(trNoficacao).remove();
        }

        if(respondido_em == '')
            $(trNoficacao).find('td:last').text('');
        else
            $(trNoficacao).find('td:last').text(formata_data_notificacao(respondido_em));
    });

    $(document).ready(function() {
        notificacoes_nao_respondidas();

        atualizar_quantidade_notificacoes();
    });

    var filtro;
    function obter_notificacoes($filtro)
    {
        filtro = $filtro;
        filtro.limite = 15; //resultados por pagina

        $("#tabela-notificacoes").find('tbody').empty();

        blockUI($("#tabela-notificacoes"));

        $('#info-notificacoes-encontradas').text('');
        $('#btn_voltar_notificacoes').attr('disabled', 'disabled');
        $('#btn_proximo_notificacoes').attr('disabled', 'disabled');

        $.ajax({
            url: $('#base_url').val() + 'admin/notificacao/pesquisar',
            type: 'GET',
            dataType: "json",
            data: filtro,
            success: function (data) {
                $("#tabela-notificacoes").find('tbody').empty();

                if(data.notificacoes.length > 0)
                {
                    var mostrando = 0;

                    $.each( data.notificacoes, function( key, value ) {
                        mostrando ++;

                        var criado_em = formata_data_notificacao(value.criado_em);

                        var respondido_em = '';
                        if(value.respondido_em != null)
                            respondido_em = formata_data_notificacao(value.respondido_em);

                        var itemJson = JSON.stringify(value).replace(/"/g, "\'");

                        $('#tabela-notificacoes').find('tbody').append('<tr onclick="abre_modal_notificacao(' + itemJson + ', this);"><td>' + value.assunto + '</td><td>' + value.nome + '</td><td>' + value.email + '</td><td>' + criado_em + '</td><td>' + respondido_em + '</td></tr>');
                    });

                    $('#info-notificacoes-encontradas').text('Mostrando ' + ((filtro.pagina * filtro.limite) + 1) + '-' + ((filtro.pagina * filtro.limite) + mostrando) + ' de ' + data.total);

                    if(filtro.pagina > 0)
                        $('#btn_voltar_notificacoes').removeAttr('disabled');
                    if(filtro.pagina + 1 < Math.ceil(data.total / filtro.limite) && Math.ceil(data.total / filtro.limite) >= 2)
                        $('#btn_proximo_notificacoes').removeAttr('disabled');
                }
                else
                {
                    $('#tabela-notificacoes').find('tbody').append('<tr><td colspan="5" class="text-center">Nenhum resultado encontrado.</td></tr>');
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(jqXhr, textStatus, errorThrown);
            },
            complete : function(){
                unblockUI($("#tabela-notificacoes"));
            }
        });
    }

    function abre_modal_notificacao(notificacao, element)
    {
        trNoficacao = element;
        popula_modal_notificacao(notificacao);
    }

    function formata_data_notificacao(data)
    {
        return $.datepicker.formatDate('dd/mm/y', new Date(data)) + " " + data.split(" ")[1].substring(0, 5);
    }

    function atualizar_quantidade_notificacoes()
    {
        $.ajax({
            url: $('#base_url').val() + 'admin/notificacao/total',
            type: 'POST',
            dataType: "json",
            success: function (total) {
                $('#quantidade-nao-respondidas').text(total.nao_respondidas);
                $('#quantidade-respondidas').text(total.respondidas);
                $('#quantidade-total').text(total.geral);
            },
            error: function () {
                alertify.error('Ocorreu um erro ao tentar obter a quantidade das notificações!');
            }
        });
    }

    $('#btn_voltar_notificacoes').on('click', function(){

        if($('#btn_voltar_notificacoes').attr('disabled') == 'disabled')n
            return;

        filtro.pagina --;

        obter_notificacoes(filtro);
    });

    $('#btn_proximo_notificacoes').on('click', function(){

        if($('#btn_proximo_notificacoes').attr('disabled') == 'disabled')
            return;

        filtro.pagina ++;

        obter_notificacoes(filtro);
    });

    function notificacoes_nao_respondidas()
    {
        $('#pesquisar-campos-contenham').attr("placeholder", "Pesquisar nas notificações não respondidas...");

        obter_notificacoes({pagina: 0, nao_respondidas: true});
    }

    function notificacoes_respondidas()
    {
        $('#pesquisar-campos-contenham').attr("placeholder", "Pesquisar nas notificações respondidas...");

        obter_notificacoes({pagina: 0, respondidas: true});
    }

    function notificacoes_todas()
    {
        $('#pesquisar-campos-contenham').attr("placeholder", "Pesquisar em todas notificações...");

        obter_notificacoes({pagina: 0});
    }

    $('#pesquisar-campos-contenham').on('keydown', function(e){
        if(e.which == 13) {
            notificacoes_pesquisar();
        }
    });

    function notificacoes_pesquisar()
    {
        filtro.campos_contenham = $('#pesquisar-campos-contenham').val();

        filtro.tipo = $('#filtro-notifcacao-tipo').val();

        obter_notificacoes(filtro);
    }
</script>
