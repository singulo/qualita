<? require_once(APPPATH . '../modules/admin/helpers/form_values_helper.php'); ?>

<? $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li><a href="<?= base_url('admin/corretor/lista'); ?>">Corretores</a></li>
            <li class="active">Formulário</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Corretor</h3>
        </div>
    </div>
    <br/>
    <div class="panel panel-white col-md-offset-3 col-md-6 col-xs-offset-1 col-xs-10">
        <form id="corretor_formulario" enctype="multipart/form-data">

            <input type="hidden" id="id_corretor" name="id_corretor" value="<? if(isset($corretor)){echo $corretor->id_corretor;} ?>">

            <div class="form-group col-xs-12" align="center">
                <img src="<? if(isset($corretor)){echo $_SESSION['filial']['fotos_corretores'] . $corretor->id_corretor .'.jpg?random=' . rand() ;} ?>" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';" id="preview" height="100px" width="100px" class="min-img" style="border-radius:50%; margin-right:10px;"/><br/>
                <div class="fileUpload btn btn-default">
                    <input type="file" onChange="previewImage(this, 'preview')" id="foto-upload" name="foto-upload" accept=".jpg"/>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="nome">Nome completo</label>
                <input type="text" class="form-control" name="nome" value="<?= form_set_value($corretor, 'nome'); ?>" id="nome">
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="email">Email principal / Login</label>
                <div class="input-group m-b-sm">
                    <input type="text" class="form-control" name="email" id="email" <? if(isset($corretor)){echo 'readonly';} ?> value="<? if(isset($corretor)){ echo substr($corretor->email, 0, strpos($corretor->email, "@")); } ?>">
                    <span class="input-group-addon" id="basic-addon2">@<?= $this->config->item('admin')['imobiliaria']['dominio']; ?></span>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="senha">Senha</label>
                <input type="password" class="form-control" name="senha" id="senha" value="<?= form_set_value($corretor, 'senha'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="senha">Repetir Senha</label>
                <input type="password" class="form-control" name="repetir_senha" id="repetir_senha" value="<?= form_set_value($corretor, 'senha'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <label>CRECI</label>
                <input type="text" class="form-control" name="creci" id="creci" value="<?= form_set_value($corretor, 'creci'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <label >CPF</label>
                <input type="text" class="form-control" name="cpf" id="cpf" value="<?= form_set_value($corretor, 'cpf'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <label>RG</label>
                <input type="text" class="form-control" name="rg" id="rg" value="<?= form_set_value($corretor, 'rg'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="telefone">Telefone</label>
                <input type="text" class="form-control telefone" name="telefone" value="<?= form_set_value($corretor, 'telefone'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="celular">Celular</label>
                <input type="text" class="form-control telefone" name="celular" value="<?= form_set_value($corretor, 'celular'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="endereco">Endereço</label>
                <input type="text" class="form-control" name="endereco" id="endereco" value="<?= form_set_value($corretor, 'endereco'); ?>">
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="nascimento">Data de nascimento</label>
                <input type="text" class="form-control" name="data_nasc" id="nascimento"  value="<? if(isset($corretor)){echo date_format(date_create($corretor->data_nasc), 'd/m/Y');} ?>">
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="nascimento">Nível</label>
                <select class="form-control m-b-sm" id="nivel" name="nivel">
                    <?= form_select_options(ADMNivel::getConstants(), form_set_value($corretor, 'nivel', ADMNivel::Corretor)); ?>
                </select>
            </div>
            <div class="checkbox col-xs-12 col-md-6" style="margin-top: 32px;">
                <label>
                    <input type="checkbox" <? if(isset($corretor) && $corretor->exibir_site) echo 'checked'; ?> onchange="$('#exibir_site').val(($(this).prop('checked') ? 1 : 0));"> Mostrar no site
                    <input type="hidden" name="exibir_site" id="exibir_site" value="<? if(isset($corretor) && $corretor->exibir_site) echo 1; else echo 0; ?>">
                </label>
            </div>
            <div class="form-group col-xs-12" align="center">
                <label class="col-xs-12">Assinatura <small>exibida no rodapé dos emails enviados</small></label>
                <img src="<? if(isset($corretor)){echo $_SESSION['filial']['fotos_corretores_assinaturas'] . $corretor->id_corretor .'.png' ;} ?>" onError="this.src=$('#base_url').val() + 'assets/images/email/rodape.jpg';" id="preview-assinatura" width="450px"/><br/>
                <div class="fileUpload btn btn-default">
                    <input type="file" onChange="previewImage(this, 'preview-assinatura')" id="assinatura-upload" name="assinatura-upload" accept=".png"/>
                </div>
            </div>
            <div class="col-xs-12 form-group">
                <button type="button" id="btn-salvar" class="btn btn-success" onclick="salvar_corretor()" data-loading-text="Aguarde..." autocomplete="off">Salvar</button>
                <? if(isset($corretor) && ADMNivel::AuthLessThan(ADMNivel::Secretaria) && $corretor->id_corretor != $this->session->userdata('admin')->id_corretor) : ?>
                    <button type="button" class="btn btn-danger pull-right" onclick="excluir_corretor()" data-loading-text="Aguarde..." autocomplete="off">Excluir</button>
                <? endif; ?>
                <? if(isset($corretor) && $this->session->userdata('admin')->id_corretor == $corretor->id_corretor) : ?>
                    <div class="alert alert-warning" role="alert">
                        <strong>Aviso!</strong> Para que suas modificações sejam aplicadas, você deve sair e entrar novamente.
                    </div>
                <? endif; ?>
            </div>
        </form>
    </div>
</div>
<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script>

    var $validator;
    $(document).ready(function() {

        $validator = $("#corretor_formulario").validate({
            ignore: "input[type='file']",
            rules: {
                nome: {
                    required: true,
                    minlength : 4
                },
                email: {
                    required: true,
                    minlength : 3
                },
                cpf: {
                    required: true
                },
                rg: {
                    required: true
                },
                nascimento: {
                    required: true
                },
                senha: {
                    required: true
                },
                creci: {
                    required: true
                },
                repetir_senha: {
                    required: true,
                    equalTo: '#senha'
                },
                telefone: {
                    require_from_group: [1, ".telefone"]
                },
                celular: {
                    require_from_group: [1, ".telefone"]
                },
            }
        })
    });

    function salvar_corretor()
    {
        var $valid = $("#corretor_formulario").valid();

        if(!$valid) {
            $validator.focusInvalid();
            return false;
        }
        else {

            $('#btn-salvar').button('loading');

            var formData = new FormData($('#corretor_formulario')[0]);

            $.ajax({

                url: $('#base_url').val() + 'admin/corretor/cadastrar',
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    console.log(data);

                    data = JSON.parse(data);

                    if(data.status == true)
                    {
                        alertify.success('Corretor salvo com sucesso.');
                        setTimeout(function (){
                            window.location.href =  $('#base_url').val() + 'admin/corretor';
                        }, 500);
                    }
                    else
                    {
                        if(data.msg != undefined)
                            alertify.error(data.msg);
                        else
                            alertify.error('Ocorreu um erro ao tentar salvar o corretor! Certifique-se de ter modificado pelo menos um campo');
                    }
                },
                error: function (request, error) {
                    console.log(request);
                    console.log(error);
                    alertify.error('Ocorreu um erro ao tentar salvar o corretor!');
                },
                complete: function(){
                    $('#btn-salvar').button('reset')
                }
            });
        }
    }

    function excluir_corretor()
    {
        alertify.confirm('Você tem certeza que deseja excluir esse corretor?',
            function(){

                $('#corretor_formulario button.btn-danger').button('loading');

                ajaxPost(
                    { id_corretor: $('#id_corretor').val() },
                    $('#base_url').val() + 'admin/corretor/excluir',
                    {
                        successCallback: function(data){
                            alertify.success('Corretor excluido com sucesso.');
                            setTimeout(function (){
                                window.location.href =  $('#base_url').val() + 'admin/corretor';
                            }, 500);
                        },
                        failureCallback: function(data){
                            if(data.msg != undefined)
                                alertify.error(data.msg);
                            else
                                alertify.error('Ocorreu um erro ao excluir o corretor.');
                        },
                        errorCallback: function(){
                            alertify.error('Ocorreu um erro ao excluir o corretor. Tente novamente mais tarde.');
                        },
                        completeCallback: function(){
                            $('#corretor_formulario button.btn-danger').button('reset');
                        }
                    }
                );
        });
    }
</script>