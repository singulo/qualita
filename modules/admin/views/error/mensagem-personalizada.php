<?php $this->load->view('admin/painel'); ?>
        <div class="page-inner">
            <div id="main-wrapper">
                <div class="row">
                    <div class="col-md-4 center">
                        <h1 class="text-xxl text-primary text-center"><?= (isset($codigo)) ? $codigo : '404'; ?></h1>
                        <div class="details text-center">
                            <h3>Oops ! Algo de errado aconteceu</h3>
                            <? if(isset($mensagem)) : ?>
                                <p><?= $mensagem; ?></p>
                            <? else : ?>
                                <p>O que está procurando provavelmente <strong>não</strong> foi encontrado ou você não tem <strong>permissão</strong> para acessar.</p>
                            <? endif; ?>
                        </div>
                    </div>
                </div><!-- Row -->
            </div><!-- Main Wrapper -->
        </div><!-- Page Inner -->

    <?php $this->load->view('admin/footer'); ?>

</body>