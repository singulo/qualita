<?php $this->load->view('admin/painel'); ?>

<div class="overlay"></div>
    <div class="page-inner">
        <div class="page-breadcrumb">
            <ol class="breadcrumb container">
                <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
                <li class="active">Clientes</li>
            </ol>
        </div>
        <div class="page-title">
            <div class="container">
                <h3>Clientes</h3>
            </div>
        </div>
        <br/>
        <div class="container">
            <div class="col-md-12 col-sm-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <form onsubmit="return false;" id="filtro-pesquisa">
                            <? if($this->session->userdata('admin')->nivel <= ADMNivel::Secretaria) : ?>
                                <div class="col-xs-12 form-group">
                                    <label>Corretor</label>
                                    <select id="filtro_corretor" name="filtro_corretor" class="form-control">
                                        <option value="" selected>--Selecione--</option>
                                        <? foreach($this->session->userdata('filial')['corretores'] as $corretor) : ?>
                                            <option value="<?= $corretor->id_corretor; ?>"><?= $corretor->nome; ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                            <? endif; ?>
                            <div class="col-xs-2 form-group">
                                <label>Filtrar</label>
                                <select id="filtro_campo" name="filtro_campo" class="form-control filtro-select">
                                    <option value="nome">Nome</option>
                                    <option value="email">Email</option>
                                </select>
                            </div>
                            <div class="col-xs-2 form-group" style="margin-left: -79px">
                                <label>Valor</label>
                                <input type="text" id="filtro_valor" name="filtro_valor" class="form-control">
                            </div>
                            <div class="col-xs-2 form-group">
                                <label>Período</label>
                                <select id="filtro_periodo_campo" name="filtro_periodo_campo" class="form-control filtro-select">
                                    <option value="cadastrado_em">Cadastro</option>
                                    <option value="ultimo_acesso">Último acesso</option>
                                </select>
                            </div>
                            <div class="col-xs-2 col-md-2 form-group" style="margin-left: -79px">
                                <label>Início</label>
                                <input type="text" class="form-control date-picker" id="filtro_periodo_inicio" name="filtro_periodo_inicio">
                            </div>
                            <div class="col-xs-2 col-md-2 form-group">
                                <label>até</label>
                                <input type="text" class="form-control date-picker" id="filtro_periodo_fim" name="filtro_periodo_fim">
                            </div>
                            <div class="col-xs-2 form-group">
                                <label>Status</label>
                                <select id="filtro_status" name="filtro_status" class="form-control">
                                    <option value="">-- Selecione --</option>
                                    <? foreach($status as $stat) : ?>
                                        <option value="<?= $stat->id; ?>"><?= $stat->status; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                            <button id="btn-buscar" onclick="seta_filtro_busca()" class="btn btn-success" data-loading-text="Buscando..." autocomplete="off" style="margin-top: 24px;">Buscar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="total_resultados" class="col-xs-12"></div>
        </div>
        <div id="main-wrapper" class="container">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="tabela-clientes" class="table">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Telefone</th>
                                    <th>Cadastro</th>
                                    <th>Último Acesso</th>
                                    <th><!-- Proposta --></th>
                                    <th><!-- Bloqueado --></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="pagination" class="pagination-holder clearfix">
                    <div id="light-pagination" class="pagination light-theme simple-pagination">
                    </div>
                </div>
            </div>
        </div>

        <!-- COMEÇO MODAL -->
        <div id="modal-cliente-info" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body" style="min-height: 508px;">
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-12 no-border-bottom">
                                    <h4> Dados principais</h4>
                                </div>
                                <input type="hidden" id="id_cliente_ajax">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Nome:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="nome"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Email:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="email"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Cidade:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="cidade"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Telefone:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="telefone telefone_1"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 no-border-bottom">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Aniversário:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="aniversario"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 no-border-bottom">
                                    <h4>Já possui imóvel no litoral</h4>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Já possui imóvel no litoral:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="imovel_litoral"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Imóvel no negócio:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="imovel_negocio"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Ano aquisição:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="ano_aquisicao"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Valor:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="valor_imovel"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 no-border-bottom">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <label>Previsão de investimento:</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="previsao_investimento"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="col-xs-12 form-group">
                                <div class="col-xs-6">
                                    <h4>Corretor vinculado</h4>
                                </div>
                            </div>
                            <div class="col-xs-12 form-group">
                                <div class="col-xs-6">
                                    <img class="medio-img" id="foto_corretor_ajax" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';">
                                </div>
                                <div class="col-xs-6">
                                    <label id="nome_corretor_ajax"></label>
                                    <label id="email_corretor_ajax"></label>
                                    <label id="telefone_corretor_ajax"></label>
                                </div>
                            </div>
                            </br>
                            <div>
                                <div class="col-xs-12">
                                    <h4>Anotações</h4>
                                </div>
                                <textarea readonly style="height: 178px; resize: none;" type="text" class="form-control obs"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-info" onClick="window.location.href = $('#base_url').val() + 'admin/cliente/perfil?id=' + $('#id_cliente_ajax').val()">Ver perfil completo</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- FIM MODAL -->

        <div class="page-footer">
            <div class="container">
                <?php $this->load->view('admin/copyright.php'); ?>
            </div>
        </div>
    </div>
    </div><!-- Page Inner -->

<?php $this->load->view('admin/footer'); ?>

<script src="<?= base_url('assets/simples/js/imovel.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/cliente.js'); ?>"></script>

<script>

    $('#filtro_valor').keypress(function(e) {
        if(e.which == 13) {
            seta_filtro_busca();
        }
    });

    var filtro = '';

    function seta_filtro_busca()
    {
        filtro = '';

        seta_url_get_param('#filtro_campo', 'filtro_campo', '#filtro_valor', 'filtro_valor');

        seta_url_get_param('#filtro_periodo_campo', 'filtro_periodo_campo_inicio', '#filtro_periodo_inicio', 'filtro_periodo_valor_inicio');

        seta_url_get_param('#filtro_periodo_campo', 'filtro_periodo_campo_fim', '#filtro_periodo_fim', 'filtro_periodo_valor_fim');

        seta_url_get_param_campo_preenchido('#filtro_status', 'filtro_status');

        if($('#filtro_corretor').length)
            seta_url_get_param_campo_preenchido('#filtro_corretor', 'filtro_corretor');

        obter_clientes(0, clienteLimite);
    }

    function seta_url_get_param(seletor_campo, seletor_campo_param, seletor_valor, seletor_valor_param)
    {
        if($(seletor_valor).val().length > 0){
            filtro += '&' + seletor_campo_param + '=' + $(seletor_campo).val();
            filtro += '&' + seletor_valor_param + '=' + $(seletor_valor).val();
        }
    }

    function seta_url_get_param_campo_preenchido(seletor, url_param)
    {
        if($(seletor).val() != null && $(seletor).val().length > 0)
        {
            filtro += '&' + url_param + '=' + $(seletor).val();
        }
    }

    function obter_clientes(pagina, limite, forcarTotal = 0)
    {
        localStorage.setItem('filtro', JSON.stringify({ filtro: $('#filtro-pesquisa').jsonify(), pagina: pagina, limite: limite }));

        $('#btn-buscar').button('loading');

        ajaxGet(
            {},
            $('#base_url').val() + 'admin/cliente/filtro?pagina=' + pagina + '&limite=' + limite + '&forcar_total=' + forcarTotal + filtro,
            {
                successCallback: function (data) {

                    $("#tabela-clientes").find('tbody').empty();

                    if(data.clientes.length > 0)
                    {
                        $.each( data.clientes, function( key, value ) {

                            var data = '-';
                            if(value.ultimo_acesso != null) {
                                data = value.ultimo_acesso.substr(0, 10).split("-");
                                data = data[2] +'/' + data[1] + '/' + data[0];
                            }

                            var cadastro = value.cadastrado_em.substr(0, 10).split("-");
                            cadastro = cadastro[2] +'/' + cadastro[1] + '/' + cadastro[0];

                            <? if($this->session->userdata('admin')->nivel == 6) : ?>
                                $('#tabela-clientes').find('tbody').append('<tr><td class="hover-hand" onclick="dados_modal_cliente(' + value.id + ');">' + value.nome + '</td><td>' + value.email + '</td><td>' + value.telefone_1 + '</td><td>' + cadastro + '</td><td>' + data + '</td><td><a href="' + $('#base_url').val() + 'admin/proposta/formulario?id=' + value.id + '" title="Enviar proposta"><i class="fa fa-send"></i></a></td><td>' + cliente_monta_elemento_bloqueio(value) + '</td></tr>');
                            <? else : ?>
                                $('#tabela-clientes').find('tbody').append('<tr><td class="hover-hand" onclick="dados_modal_cliente(' + value.id + ');">' + value.nome + '</td><td>' + value.email + '</td><td>' + value.telefone_1 + '</td><td>' + cadastro + '</td><td>' + data + '</td><td><a href="' + $('#base_url').val() + 'admin/proposta/formulario?id=' + value.id + '" title="Enviar proposta"><i class="fa fa-send"></i></a></td><td>' + cliente_monta_elemento_bloqueio(value) + '</td><td>' + cliente_monta_elemento_deletar(value) + '</td></tr>');
                            <? endif; ?>
                            $('#tabela-clientes').find('tbody').append('<input id="cliente-' + value.id + '" type="hidden" value=\'' + JSON.stringify(value) + '\'>');

                        });
                    }
                    else
                    {
                        $('#tabela-clientes').find('tbody').append('<tr><td colspan="6" class="text-center">Nenhum resultado encontrado.</td></tr>');
                    }

                    if(data.total != undefined)
                    {
                        $('#pagination').pagination('updateItems', data.total);
                        $('#total_resultados').html(data.total + ' cliente(s) encontrado(s)');
                    }
                    else if(pagina == 0)
                        $('#total_resultados').html('Nenhum cliente encontrado');
                },
                errorCallback: function (request, error) {
                    alertify.error('Ocorreu um erro ao tentar obter os clientes!');
                },
                completeCallback: function(){
                    $('#btn-buscar').button('reset');
                },
                blockElement: $("#tabela-clientes")
        });
    }

    function seta_corretor_vinculado(id_corretor)
    {
        $('#foto_corretor_ajax').attr('src', $('#fotos_corretores_url').val() + id_corretor + '.jpg');
        $.each( corretores, function( key, value ) {
            if(value.id_corretor == id_corretor)
            {
                $('#nome_corretor_ajax').text(value.nome);
                $('#email_corretor_ajax').text(value.email);
                $('#telefone_corretor_ajax').text(value.telefone);
            }
        });
    }

    var clienteLimite = 20;
    $(document).ready(function()
    {
        var paginaAtual = 0;
        if(localStorage.getItem('filtro') != null)
        {
            var _filtro = JSON.parse(localStorage.getItem('filtro'));
            $('#filtro-pesquisa').dejsonify(_filtro.filtro);
            seta_filtro_busca();
            obter_clientes(_filtro.pagina, _filtro.limite, 1);
            paginaAtual = _filtro.pagina;
        }
        else
        {
            obter_clientes(0, clienteLimite);
        }

        $(function() {
            $('#pagination').pagination({
                items: 0,
                itemsOnPage: clienteLimite,
                cssStyle: 'light-theme',
                nextText: 'Próximo',
                prevText: 'Anterior',
                currentPage: paginaAtual,
                onPageClick : function(pageNumber){
                    obter_clientes((pageNumber-1), clienteLimite);
                }
            });
        });
    });

    function dados_modal_cliente(id)
    {
        var cliente = JSON.parse($('#cliente-' + id).val());

        var dados_cliente_display = $('#modal-cliente-info .modal-body');

        $('#id_cliente_ajax').val(id);
        dados_cliente_display.find('.nome').text(cliente.nome);
        dados_cliente_display.find('.email').text(cliente.email);
        dados_cliente_display.find('.cidade').text(cliente.cidade);
        dados_cliente_display.find('.telefone_1').text(cliente.telefone_1);
        dados_cliente_display.find('.aniversario').text(cliente.aniversario);
        dados_cliente_display.find('.obs').text(cliente.obs);
        dados_cliente_display.find('.imovel_litoral').text((cliente.imovel_litoral != null ? (cliente.imovel_litoral? 'Sim' : 'Não') : ''));
        dados_cliente_display.find('.imovel_negocio').text((cliente.imovel_negocio != null ? (cliente.imovel_negocio? 'Sim' : 'Não') : ''));
        dados_cliente_display.find('.ano_aquisicao').text((cliente.ano_aquisicao != null ? (cliente.ano_aquisicao? 'Sim' : 'Não') : ''));
        dados_cliente_display.find('.valor_imovel').text(cliente.valor_imovel);
        dados_cliente_display.find('.previsao_investimento').text(cliente.previsao_investimento);

        seta_corretor_vinculado(cliente.id_corretor);

        $('#modal-cliente-info').modal('show');
    }

</script>

<style>
    #visu_accordion img
    {
        vertical-align: middle;
        max-height: 120px;
    }

    .message-attachment img {
        display: block;
        width: 100%;
    }

    .message-attachment {
        width: 197px;
        margin: 5px;
        position: relative;
        float: left;
        border: 1px solid #f1f1f1;
    }

    .medio-img
    {
        border-radius:50%;
        margin-right:10px;
        height: 125px;
        width: 125px;
    }

    .filtro-select
    {
        min-width: 100px;
        width: 100px;
    }

    .filtro-date
    {
        width: 145px;
    }

    #modal-cliente-info .modal-body .col-xs-6:first-child div.col-xs-12
    {
        border-bottom: 1px solid rgb(221, 221, 221);
        margin-bottom: 10px;
    }

    .no-border-bottom
    {
        border-bottom: 0px !important;
    }
</style>