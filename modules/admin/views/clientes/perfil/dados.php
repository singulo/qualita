<link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin/pages/cliente/perfil/css/dados.css'); ?>">

<div role="tabpanel">
    <form id="form-dados" onsubmit="return false;">
        <input type="hidden" name="id" value="<?= form_set_value($cliente, 'id', 0); ?>">
        <div class="col-md-7">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab-dados-pessoais" role="tab" data-toggle="tab">Pessoais</a></li>
                <li role="presentation"><a href="#tab-potencial-compra" role="tab" data-toggle="tab">Potencial de compra</a></li>
                <li role="presentation"><a href="#tab-interesse" role="tab" data-toggle="tab">Interesse</a></li>
                <li role="presentation"><a href="#tab-negociacao" role="tab" data-toggle="tab">Negociação</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active fade in" id="tab-dados-pessoais">
                    <div class="row">
                        <div class="form-group">
                            <label for="nome">Nome completo</label>
                            <input type="text" class="form-control" name="nome" required minlength="5">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="email_alternativo">Email alternativo</label>
                            <input type="email" class="form-control" name="email_alternativo">
                        </div>
                        <div class="form-group">
                            <label for="telefone_1">Telefone 1</label>
                            <input type="text" class="form-control telefone" name="telefone_1">
                        </div>
                        <div class="form-group">
                            <label for="telefone_2">Telefone 2</label>
                            <input type="text" class="form-control telefone" name="telefone_2">
                        </div>
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" class="form-control" name="cidade">
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-potencial-compra">
                    <div class="row">
                        <div class="form-group">
                            <label for="imovel_litoral">Possui imóvel</label>
                            <select class="form-control" name="imovel_litoral">
                                <option value="" disabled selected>--Selecione--</option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cidade_imovel">Cidade</label>
                            <input type="text" class="form-control" name="cidade_imovel">
                        </div>
                        <div class="form-group">
                            <label for="imovel_negocio">Este imóvel no negócio</label>
                            <select class="form-control" name="imovel_negocio">
                                <option value="" disabled selected>--Selecione--</option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ano_aquisicao">Ano da aquisição</label>
                            <input type="text" class="form-control ano" name="ano_aquisicao">
                        </div>
                        <div class="form-group">
                            <label for="valor_imovel">Valor do imóvel</label>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon">R$</span>
                                <input type="text" class="form-control dinheiro" name="valor_imovel">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="potencia_previsao_investimento">Previsão de investimento</label>
                            <select class="form-control" name="potencia_previsao_investimento">
                                <?php
                                if(strlen($_SESSION['filial']['previsao_investimento']) > 0) : ?>
                                    <option disabled selected>--Selecione--</option>
                                    <?php foreach(explode(',', $_SESSION['filial']['previsao_investimento']) as $previsao) : ?>
                                        <option value="<?= trim($previsao); ?>"><?= trim($previsao); ?></option>
                                    <?php endforeach;
                                else : echo '<option disabled selected>--Nenhuma previsao cadastrada--</option>';
                                endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-interesse">
                    <div class="row">
                        <div class="form-group">
                            <label for="interesse_cidade">Cidade</label>
                            <select multiple class="form-control" name="interesse_cidade"></select>
                        </div>
                        <div class="form-group">
                            <label for="interesse_tipo">Tipo de imóvel</label>
                            <select multiple class="form-control" name="interesse_tipo"></select>
                        </div>
                        <div class="form-group">
                            <label for="interesse_valor">Valor do imóvel</label>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon">R$</span>
                                <input type="text" class="form-control dinheiro" name="interesse_valor">
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-negociacao">
                    <div class="row">
                        <div class="form-group">
                            <label for="interesse_entrada">Valor da entrada</label>
                            <div class="input-group m-b-sm">
                                <span class="input-group-addon">R$</span>
                                <input type="text" class="form-control dinheiro" name="interesse_entrada">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="interesse_saldo">Saldo</label>
                            <select class="form-control" name="interesse_saldo">
                                <?
                                if(strlen($_SESSION['filial']['opcoes_saldo']) > 0) : ?>
                                    <option disabled selected>--Selecione--</option>
                                    <? foreach(explode(',', $_SESSION['filial']['opcoes_saldo']) as $saldo) : ?>
                                        <option value="<?= trim($saldo); ?>"><?= trim($saldo); ?></option>
                                        <?
                                    endforeach;
                                else : echo '<option disabled selected>--Nenhum saldo cadastrado--</option>';
                                endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <h3 class="text-center">Corretor vinculado</h3>
            <br>
            <div class="col-md-5">
                <img src="<?= $_SESSION['filial']['fotos_corretores'] . form_set_value($cliente, 'id_corretor', $this->session->userdata('admin')->id_corretor) . '.jpg';?>" class="img-circle img-responsive" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';">
            </div>
            <div class="col-md-7">
                <div class="row">
                    <select class="form-control" name="id_corretor" required <? if($this->session->userdata('admin')->nivel > ADMNivel::Secretaria) echo 'disabled'; ?>>
                        <? foreach($corretores as $corretor) : ?>
                            <option value="<?= $corretor->id_corretor;?>" data-corretor='<?= json_encode($corretor);?>' <? if(is_null($cliente) && $corretor->id_corretor == $this->session->userdata('admin')->id_corretor){ echo 'selected'; }?>><?= $corretor->nome;?></option>
                        <? endforeach; ?>
                    </select>
                    <input type="hidden" name="id_corretor_original" value="<?= form_set_value($cliente, 'id_corretor', $this->session->userdata('admin')->id_corretor);?>">
                    <br>
                    <label><b>Nome:</b> <span class="corretor-nome"></span></label>
                    <br>
                    <label><b>Creci:</b> <span class="corretor-creci"></span></label>
                    <br>
                    <label><b>Email:</b> <span class="corretor-email"></span></label>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <hr>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="id_status">Status</label>
                        <select name="id_status" class="form-control">
                            <option value="" disabled selected>-- Selecione --</option>
                            <? foreach($status as $stat) : ?>
                                <option value="<?= $stat->id; ?>"><?= $stat->status; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="id_status">Origem</label>
                        <select name="id_origem" class="form-control">
                            <option value="" disabled selected>-- Selecione --</option>
                            <? foreach($origens as $origem) : ?>
                                <option value="<?= $origem->id; ?>"><?= $origem->origem; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <textarea name="obs" placeholder="Neste campo você pode colocar informações importantes relacionadas a este cliente, edite sempre que necessário." class="form-control" required minlength="5" style="height: 215px; margin-bottom: 10px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <button type="button" class="btn btn-success pull-right" data-loading-text="Aguarde..." autocomplete="off" onclick="salvar_dados_cliente();">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?= base_url('assets/admin/pages/cliente/perfil/js/dados.js'); ?>"></script>

<script>

    $('#form-dados [name="id_corretor_original"]').val(<?= form_set_value($cliente, 'id_corretor', $this->session->userdata('admin')->id_corretor);?>);

</script>