<? if(count($notificacoes) == 0) : ?>
    <div class="alert alert-warning" role="alert">
        Nenhum contato recebido desse cliente.
    </div>
<? else : ?>
    <div class="alert alert-info" role="alert">
        Todos contatos enviados pelo cliente: Interesse em imóveis, dúvidas e outros.
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Assunto</th>
                <th>Enviado em</th>
                <th>Respondido em</th>
                <th>Corretor</th>
            </tr>
        </thead>
        <tbody>
            <? foreach($notificacoes as $notificacao): ?>
                <tr>
                    <td><?= $notificacao->assunto; ?></td>
                    <td><? $date = date_create($notificacao->criado_em); echo date_format($date, 'd/m/Y H:i');?></td>
                    <td><? $date = date_create($notificacao->respondido_em); echo date_format($date, 'd/m/Y H:i');?></td>
                    <td><?= $corretores[$notificacao->id_corretor]->nome; ?></td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>
<? endif; ?>