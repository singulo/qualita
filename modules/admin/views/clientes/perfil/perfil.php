<? require_once(MODULESPATH . 'admin/helpers/form_values_helper.php'); ?>
<? if(!isset($cliente)) { $cliente = null; } ?>

<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li><a href="<?= base_url('admin/cliente/lista'); ?>">Clientes</a></li>
            <li class="active">Perfil</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Perfil do cliente <?= strtok(form_set_value($cliente, 'nome'), ' '); ?></h3>
        </div>
    </div>

    <div id="main-wrapper" class="container">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h3 class="panel-title"><span class="fa fa-newspaper-o"></span> Dados</h3>
                    </div>
                    <div class="panel-body">
                        <div class="tabs-left" role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab-dados" role="tab" data-toggle="tab"><span class="fa fa-newspaper-o"></span> Dados</a></li>
                                <? if(isset($cliente)) : ?>
                                    <li role="presentation"><a href="#tab-historico" role="tab" data-toggle="tab"><span class="fa fa-history"></span> Histórico</a></li>
                                    <li role="presentation"><a href="#tab-contatos" role="tab" data-toggle="tab"><span class="fa fa-envelope"></span> Contatos</a></li>
                                    <li role="presentation"><a href="#tab-propostas" role="tab" data-toggle="tab"><span class="fa fa-send"></span> Propostas</a></li>
                                <? endif; ?>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade in" id="tab-dados">
                                    <div class="col-md-10">
                                        <? $this->load->view('admin/clientes/perfil/dados'); ?>
                                    </div>
                                </div>
                                <? if(isset($cliente)) : ?>
                                    <div role="tabpanel" class="tab-pane fade" id="tab-historico">
                                        <div class="col-md-10">
                                            <? $this->load->view('admin/clientes/perfil/historico'); ?>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab-contatos">
                                        <div class="col-md-10">
                                            <? $this->load->view('admin/clientes/perfil/contatos'); ?>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab-propostas">
                                        <div class="col-md-10">
                                            <? $this->load->view('admin/clientes/perfil/propostas'); ?>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

<?php $this->load->view('admin/footer'); ?>

<style>
    .panel > .panel-body .tabs-left > .tab-content
    {
        padding-top: 0px;
    }
</style>

<script>

    var cliente = <?= json_encode($cliente); ?>;

    $('.tabs-left ul li').on('click', function(){

        console.log($(this).find('span')[0].outerHTML);

        $('h3.panel-title').html($(this).find('span')[0].outerHTML + ' ' + $(this).find('a').text());
    });
</script>
