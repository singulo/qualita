<div role="tabpanel">
    <div class="col-xs-12">
        <div class="row">
            <div class="alert alert-info" role="alert">
                Acompanhe ao vivo os imóveis visualizados e o histórico
            </div>
            <div class="panel-group" id="imoveis-visualizados" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/simples/js/imovel.js'); ?>"></script>
<script src="<?= base_url('assets/admin/pages/cliente/perfil/js/historico.js'); ?>"></script>