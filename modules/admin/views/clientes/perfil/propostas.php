<? if(count($propostas) == 0) : ?>
    <div class="alert alert-warning" role="alert">
        Nenhuma proposta e emails enviados para esse cliente.
    </div>
<? else : ?>
    <div class="alert alert-info" role="alert">
        Todas propostas emails enviados para esse cliente.
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Corretor</th>
                <th>Enviado em</th>
                <th>Imóvel</th>
            </tr>
        </thead>
        <tbody>
            <? foreach($propostas as $proposta): ?>
                <tr>
                    <td><?= isset($corretores[$proposta->id_corretor]) ? $corretores[$proposta->id_corretor]->nome : 'Não informado'; ?></td>
                    <td><? $date = date_create($proposta->data); echo date_format($date, 'd/m/Y H:i');?></td>
                    <td>
                        <?
                        $imoveis = explode(',', $proposta->cod_imoveis);
                        if(count($imoveis) > 1)
                            echo count($imoveis) . ' imóveis';
                        else
                            echo $proposta->cod_imoveis;
                        ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>
<? endif; ?>