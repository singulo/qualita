<? require_once(APPPATH . '../modules/admin/helpers/form_values_helper.php'); ?>

<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li class="active">Metas</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Metas</h3>
        </div>
    </div>
    <br/>
    <div id="main-wrapper" class="container">
        <div class="col-md-12">
            <a href="#modal-nova-meta" data-toggle="modal" class="btn btn-success btn-addon m-b-sm" ><i class="fa fa-plus"></i> Meta</a>
        </div>
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="table-responsive">
                        <input id="metas_total" type="hidden" value="<?= $metas_total; ?>">
                        <table id="tabela-metas" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Mês</th>
                                <th>Ano</th>
                                <th>Valor</th>
                                <th>Progresso</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="pagination" class="pagination-holder clearfix">
                <div id="light-pagination" class="pagination light-theme simple-pagination">
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

<div id="modal-nova-meta" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Nova meta</h4>
            </div>
            <div class="modal-body">
                <form onsubmit="return false;">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Mês</label>
                                    <select name="mes" class="form-control">
                                        <?= form_options_month(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Ano</label>
                                    <input type="text" name="ano" class="form-control ano">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Valor equipe</label>
                                    <div class="input-group m-b-sm">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control dinheiro" name="valor_equipe">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label control-label">Valor individual</label>
                                    <div class="input-group m-b-sm">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control dinheiro" name="valor_individual">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-loading-text="Aguarde..." autocomplete="off" onclick="adicionar_meta();"><i class="fa fa-plus"></i> Adicionar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script>

    $(function() {
        listar_metas(0);

        var hoje = new Date();
        $('#modal-nova-meta select[name="mes"]').val(hoje.getMonth() + 1);
        $('#modal-nova-meta input[name="ano"]').val(hoje.getFullYear());

        $('#pagination').pagination({
            items: $('#metas_total').val(),
            itemsOnPage: 10,
            cssStyle: 'light-theme',
            nextText: 'Próximo',
            prevText: 'Anterior',
            onPageClick : function(pageNumber){
                listar_metas((pageNumber-1));
            }
        });

        $validator = $("#modal-nova-meta form").validate({
            rules: {
                mes: {
                    required: true
                },
                ano: {
                    required: true
                },
                valor_individual: {
                    required: true
                },
                valor_equipe: {
                    required: true
                },
            }
        });
    });

    function listar_metas(pagina)
    {
        ajaxPost(
            {
                pagina: pagina,
                limite: 20,
            },
            $('#base_url').val() + 'admin/meta/lista',
            {
                successCallback: function (data) {

                    $("#tabela-metas").find('tbody').empty();

                    if(data.metas.length > 0)
                    {
                        var meta_url = $('#base_url').val() + 'admin/meta/corretores?id=';

                        $.each( data.metas, function( key, meta ) {
                            $('#tabela-metas').find('tbody').append('<tr onclick="window.location.href = \'' + meta_url + meta.id + '\'"><td>' + $.getMonthName(meta.mes) + '</td><td>' + meta.ano + '</td><td>' + meta.valor_equipe + '</td><td><div class="progress" style="margin-bottom: 0px;"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="' + meta.progresso + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + meta.progresso + '%">' + meta.meta_equipe + '(' + parseFloat(meta.progresso).toFixed(2) + '%)</div></div></td></tr>');
                        });
                    }
                    else
                        $('#tabela-metas').find('tbody').append('<tr><td colspan="4" class="text-center">Nenhum resultado encontrado.</td></tr>');
                },
                errorCallback: function (request, error) {
                    alertify.error('Ocorreu um erro ao tentar obter as metas!');
                },
                blockElement: $('#tabela-metas')
            }
        )
    }

    $.monthNames = [
        "Janeiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro",
        "Outubro", "Novembro", "Dezembro"
    ];

    $.getMonthName = function(month) {
        return $.monthNames[month - 1];
    };

    $.getShortMonthName = function (month) {
        return this.getMonthName(month).substr(0, 3);
    };

    function adicionar_meta()
    {
        if(!$("#modal-nova-meta form").valid())
            return;

        $('#modal-nova-meta .modal-footer button').button('loading');

        ajaxPost(
            $('#modal-nova-meta form').jsonify(),
            $('#base_url').val() + 'admin/meta/adicionar',
            {
                successCallback: function (data) {
                    $('#modal-nova-meta').modal('hide');
                    alertify.success('Meta adicionada com sucesso.');
                },
                failureCallback: function (data) {
                    if(data.msg != undefined)
                        alertify.error(data.msg);
                    else
                        alertify.error('Ocorreu um erro ao adicionar a meta. Tente novamente mais tarde.');
                },
                errorCallback: function () {
                    alertify.error('Ocorreu um erro ao adicionar a meta.');
                },
                completeCallback: function(){
                    $('#modal-nova-meta .modal-footer button').button('reset');
                }
            }
        );
    }

</script>
