<?php $this->load->view('admin/painel'); ?>
<link href="<?= base_url('assets/admin/plugins/x-editable/bootstrap3-editable/css/bootstrap-editable.css'); ?>" rel="stylesheet" type="text/css">

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li><a href="<?= base_url('admin/meta'); ?>">Metas</a></li>
            <li class="active">Meta <?= $meta->mes . '/' . $meta->ano; ?></li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Meta <?= $meta->mes . '/' . $meta->ano; ?></h3>
        </div>
    </div>
    <br/>
    <div id="main-wrapper" class="container">

        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Meta <?= $meta->mes . '/' . $meta->ano; ?></h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <p><b>Equipe:</b> R$ <?= number_format($meta->valor_equipe, 2, ',', '.'); ?><br><b>Individual:</b> R$ <?= number_format($meta->valor_individual, 2, ',', '.'); ?></p>
                        <input id="valor_meta_equipe" type="hidden" value="<?= $meta->valor_equipe; ?>">
                        <input id="valor_meta_individual" type="hidden" value="<?= $meta->valor_individual; ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="col-sm-6">
                            <label control-label">Progresso equipe</label>
                            <div class="progress" style="margin-bottom: 0px;">
                                <? $progresso_equipe = ($meta->meta_equipe * 100) / $meta->valor_equipe;?>
                                <? $progresso_equipe_width = $progresso_equipe > 100 ? 100 : $progresso_equipe;?>
                                <div class="progress-bar progress-bar-success meta_equipe" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progresso_equipe_width; ?>%;"><?= round($progresso_equipe, 2); ?>%</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label control-label">Valor equipe</label>
                                <br>
                                <a id="meta_equipe" href="#" data-title="Meta atual de equipe" data-value="<?= $meta->meta_equipe; ?>" data-pk="<?= $meta->id; ?>" data-url="<?= base_url('admin/meta/atualizar_meta_equipe'); ?>"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table id="example-editable" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                                <th>Corretor</th>
                                <th>Progresso</th>
                                <th>Meta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach($corretores as $corretor) : ?>
                                <tr>
                                    <td><?= $corretor->nome; ?></td>
                                    <? $progresso = ($corretor->meta * 100) / $meta->valor_individual;?>
                                    <? $progresso_width = $progresso > 100 ? 100 : $progresso;?>
                                    <td><div class="progress" style="margin-bottom: 0px;"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progresso_width; ?>%;"><?= $progresso; ?>%</div></div></td>
                                    <td><a href="#" data-title="Meta atual de <?= $corretor->nome; ?>" data-value="<?= $corretor->meta; ?>" data-pk="<?= $corretor->id_meta_corretor; ?>" data-url="<?= base_url('admin/meta/corretor_atualizar'); ?>"><?= $corretor->meta; ?></a></td>
                                </tr>
                            <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>
<script src="<?= base_url('assets/admin/plugins/x-editable/bootstrap3-editable/js/bootstrap-editable.js'); ?>"></script>
<script src="<?= base_url('assets/admin/pages/meta/js/corretores.js'); ?>"></script>