<? require_once(APPPATH . '../modules/admin/helpers/form_values_helper.php'); ?>

<?php $this->load->view('admin/painel'); ?>
<link href="<?= base_url('assets/admin/plugins/weather-icons-master/css/weather-icons.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/pages/home/css/home.css'); ?>" rel="stylesheet" type="text/css"/>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li class="active">Painel</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Painel principal</h3>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row">

            <? if(!ADMNivel::AuthBiggerOrEqualsThan(ADMNivel::Secretaria)) : ?>

                <div class="col-lg-4 col-sm-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body" style="border: solid #0C9 1px;">
                            <div class="info-box-stats">
                                <p class="counter"><?= $total['clientes_cadastrados_ontem']; ?></p>
                                <span class="pull-left info-box-title">Clientes cadastrados ontem</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-users"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body" style="border: solid #0C9 1px;">
                            <div class="info-box-stats">
                                <p class="counter"><?= $total['acessos_clientes_ontem']; ?></p>
                                <span class="info-box-title">Clientes que acessaram ontem</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-eye"></i>
                            </div>
                        </div>
                    </div>
                </div>

            <? else : ?>

                <div class="col-lg-4 col-sm-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body" style="border: solid #0C9 1px;">
                            <div class="info-box-stats">
                                <p class="counter"><?= $total['clientes']; ?></p>
                                <span class="info-box-title">Meus clientes</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-users"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body" style="border: solid #12AFCB 1px;">
                            <div class="info-box-stats">
                                <p class="counter"><?= $total['propostas']; ?></p>
                                <span class="info-box-title">Propostas enviadas</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-cursor"></i>
                            </div>
                        </div>
                    </div>
                </div>

            <? endif ;?>

            <div class="col-lg-4 col-sm-6">
                <div class="panel info-box panel-white">
                    <div class="panel-body" style="border: solid #f6d433 1px;">
                        <div class="info-box-stats">
                            <p class="counter"><?= $total['imoveis']; ?></p>
                            <span class="info-box-title">Imóveis cadastrados</span>
                        </div>
                        <div class="info-box-icon">
                            <i class="icon-home"></i>
                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="col-lg-4 col-sm-6">
                <div class="panel info-box panel-white">
                    <div class="panel-body" style="border: solid #f6d433 1px;">
                        <div class="info-box-stats">
                            <p class="counter"><?/*= $total['quantidade_fotos_sem_marca_dagua']; */?></p>
                            <span class="info-box-title">Fotos sem marca d'agua</span>
                        </div>
                        <div class="info-box-icon">
                            <i class="icon-picture"></i>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>

        <div class="row">
            <div class="col-md-12 metas">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h4 class="panel-title">Metas de <?= get_months()[(int)date('m')]; ?></h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-xs-4 col-md-2">
                                    <img src="<?= $_SESSION['filial']['fotos_corretores'] . $this->session->userdata('admin')->id_corretor . '.jpg'; ?>" class="center-block img-circle" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';">
                                </div>
                                <div class="col-xs-8 col-md-10">
                                    <p>Individual</p>
                                    <div class="progress">
                                        <? $progresso_individual = isset($meta) && isset($meta_corretor) ? ($meta_corretor->meta * 100) / $meta->valor_individual : 0;?>
                                        <? $progresso_individual_width = $progresso_individual > 100 ? 100 : $progresso_individual;?>
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progresso_individual_width; ?>%;"><?= round($progresso_individual, 2); ?>%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-xs-4 col-md-2 img-logo">
                                    <img src="<?= base_url('assets/images/logo.png'); ?>" class="center-block">
                                </div>
                                <div class="col-xs-8 col-md-10">
                                    <p>Equipe</p>
                                    <div class="progress">
                                        <? $progresso_equipe = isset($meta) ? ($meta->meta_equipe * 100) / $meta->valor_equipe : 0;?>
                                        <? $progresso_equipe_width = $progresso_equipe > 100 ? 100 : $progresso_equipe;?>
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progresso_equipe_width; ?>%;"><?= round($progresso_equipe, 2); ?>%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5 lembretes">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h4 class="panel-title">Lembretes</h4>
                        <div class="panel-control">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Novo lembrete" onclick="abrir_lembrete();"><i class="icon-plus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="inbox-widget slimscroll">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <iframe width="100%" height="416" src="https://www.youtube.com/embed/6WgPCFc11qU" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h4 class="panel-title">Últimos cadastros</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive project-stats">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Telefone</th>
                                    <th>Cadastro</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach($ultimos_clientes as $cliente) : ?>
                                    <tr>
                                        <th scope="row"><?= $cliente->id; ?></th>
                                        <td><a href="<?= base_url('admin/cliente/perfil?id=' . $cliente->id); ?>"><?= $cliente->nome; ?></a></td>
                                        <td><?= $cliente->email; ?></td>
                                        <td><?= $cliente->telefone_1; ?></td>
                                        <td><?= date("d/m/Y", strtotime($cliente->cadastrado_em));?></td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div>
<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>
<script src="<?= base_url('assets/admin/pages/home/js/home.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/dashboard.js'); ?>"></script>