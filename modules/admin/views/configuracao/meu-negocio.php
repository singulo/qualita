<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
	<div class="page-breadcrumb">
		<ol class="breadcrumb container">
			<li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
			<li>Configuração</li>
			<li class="active">Meu negócio</li>
		</ol>
	</div>
	<div class="page-title">
		<div class="container">
			<h3>Configuração / Meu negócio</h3>
		</div>
	</div>
	<br/>
	<form id="form-configuracao">
		<input type="hidden" id="imobiliaria_id" name="imobiliaria_id" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->id; } ?>">
		<div role="tabpanel" class="panel panel-white col-md-offset-2 col-md-8 col-xs-offset-1 col-xs-10">
			<ul class="nav nav-tabs nav-justified" role="tablist">
				<li role="presentation" class="active"><a href="#tab-cliente" role="tab" data-toggle="tab" aria-expanded="true">Cliente</a></li>
				<li role="presentation" class=""><a href="#tab-imobiliaria" role="tab" data-toggle="tab" aria-expanded="false">Imobiliária</a></li>
				<li role="presentation" class=""><a href="#tab-email" role="tab" data-toggle="tab" aria-expanded="false">Email</a></li>
				<li role="presentation" class=""><a href="#tab-redes-sociais" role="tab" data-toggle="tab" aria-expanded="false">Redes Sociais</a></li>
			</ul>

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab-cliente">
					<div class="form-group col-xs-12">
						<h3>Previsão de investimento</h3>
						<label >Opções disponíveis (<small>Separe por virgular as opções de investimento</small>)</label>
						<input type="text" class="form-control" name="previsao_investimento" id="previsao_investimento" placeholder="Ex: 60 vezes, 90 vezes" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->previsao_investimento; } ?>">
						<br>
						<label>Como o investimento vai ser exibido se o valor informado for "Imediato, 6 meses"</label>
						<select class="form-control">
							<option value="" disabled selected>--Selecione--</option>
							<option value="">Imediato</option>
							<option value="">6 meses</option>
						</select>
					</div>

					<div class="form-group col-xs-12">
						<h3>Saldo</h3>
						<label >Opções disponíveis (<small>Separe por virgular as opções de saldo</small>)</label>
						<input type="text" class="form-control" name="opcoes_saldo" id="opcoes_saldo" placeholder="Ex: 60 vezes, 90 vezes" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->opcoes_saldo; } ?>">
						<br>
						<label>Como o saldo vai ser exibido se o valor informado for "30/60/90, 60 vezes, 90 vezes"</label>
						<select class="form-control">
							<option value="" disabled selected>--Selecione--</option>
							<option value="">30/60/90</option>
							<option value="">60 vezes</option>
							<option value="">90 vezes</option>
						</select>
					</div>

					<div class="col-xs-12">
						<h3>Corretor padrão</h3>
						<label>Quando o cliente se cadastrar será vinculo automaticamente o corretor selecionado</label>
						<select id="corretor_padrao" name="corretor_padrao" class="form-control">
						</select>
					</div>

				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab-imobiliaria">

						<div class="form-group col-xs-12">
							<label >Nome da imobiliária</label>
							<input type="text" class="form-control" name="imobiliaria_nome" id="imobiliaria_nome" placeholder="Nome da imobiliária"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->nome; } ?>">
						</div>
						<div class="form-group col-xs-12">
							<label >Razão social</label>
							<input type="text" class="form-control" name="razao_social" id="razao_social" placeholder="Razão social" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->razao_social; } ?>">
						</div>
						<div class="form-group col-xs-6">
							<label >CNPJ</label>
							<input type="text" class="form-control" name="cnpj" id="cnpj" placeholder="CNPJ" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->cnpj; } ?>">
						</div>
						<div class="form-group col-xs-6">
							<label >CRECI</label>
							<input type="text" class="form-control" name="creci" id="creci" placeholder="CRECI" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->creci; } ?>">
						</div>
						<div class="form-group col-xs-4">
							<label >Telefone 1</label>
							<input type="text" class="form-control telefone" name="telefone_1" id="telefone_1" placeholder="Telefone 1" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->telefone_1; } ?>">
						</div>
						<div class="form-group col-xs-4">
							<label >Telefone 2</label>
							<input type="text" class="form-control telefone" name="telefone_2" id="telefone_2" placeholder="Telefone 2" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->telefone_2; } ?>">
						</div>
						<div class="form-group col-xs-4">
							<label >Telefone 3</label>
							<input type="text" class="form-control telefone" name="telefone_3" id="telefone_3" placeholder="Telefone 3" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->telefone_3; } ?>">
						</div>
						<div class="form-group col-xs-6">
							<label>Cidade</label>
							<input type="text" class="form-control" name="cidade" id="cidade" placeholder="Cidade" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->cidade; } ?>">
						</div>
						<div class="form-group col-xs-6">
							<label>Estado</label>
							<select class="form-control" name="estado" id="estado" tabindex="-1" aria-hidden="true">
								<option value="0" disabled selected>--Selecione--</option>
								<option value="ac">Acre</option>
								<option value="al">Alagoas</option>
								<option value="am">Amazonas</option>
								<option value="ap">Amapá</option>
								<option value="ba">Bahia</option>
								<option value="ce">Ceará</option>
								<option value="df">Distrito Federal</option>
								<option value="es">Espírito Santo</option>
								<option value="go">Goiás</option>
								<option value="ma">Maranhão</option>
								<option value="mt">Mato Grosso</option>
								<option value="ms">Mato Grosso do Sul</option>
								<option value="mg">Minas Gerais</option>
								<option value="pa">Pará</option>
								<option value="pb">Paraíba</option>
								<option value="pr">Paraná</option>
								<option value="pe">Pernambuco</option>
								<option value="pi">Piauí</option>
								<option value="rj">Rio de Janeiro</option>
								<option value="rn">Rio Grande do Norte</option>
								<option value="rs">Rio Grande do Sul</option>
								<option value="ro">Rondônia</option>
								<option value="rr">Roraima</option>
								<option value="sc">Santa Catarina</option>
								<option value="se">Sergipe</option>
								<option value="sp">São Paulo</option>
								<option value="to">Tocantins</option>
							</select>
						</div>
						<div class="form-group col-xs-12">
							<label>Endereço</label>
							<input type="text" class="form-control" name="endereco" id="endereco" placeholder="Endereco" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->endereco; } ?>">
						</div>
						<div class="form-group col-xs-6">
							<label >Domínio padrão</label>
							<input type="text" class="form-control" name="dominio_padrao" id="dominio_padrao" placeholder="Ex: singulo.com.br" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->dominio_padrao; } ?>">
						</div>
						<div class="form-group col-xs-12">
							<label >Está é uma</label>
							<select class="form-control">
								<option>Matriz</option>
								<option>Filial</option>
							</select>
						</div>
						<div class="form-group col-xs-12">
							<label>Latitude e longitude</label>
							<input type="latitude_longitude" class="form-control" name="latitude_longitude" id="latitude_longitude" placeholder="Ex: -30.032760, -51.217852" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->latitude_longitude; } ?>">
							<a href="#" class="ver-no-google-maps" onclick="window.open('http://maps.google.com/maps?q=' + $('#latitude_longitude').val().replace(' ', ''), '_blank');">Ver no google maps</a>
						</div>
						<div class="form-group col-xs-6">
							<label>E-mail padrão <small>(Exibido no site)</small></label>
							<input type="email" class="form-control" name="email_padrao" id="email_padrao" placeholder="Ex: adm@singulo.com.br" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->email_padrao; } ?>">
						</div>
						<div class="form-group col-xs-6">
							<label>E-mail gerente<small>(Para receber cópia oculta de contatos com o cliente)</small></label>
							<input type="email" class="form-control" name="email_gerente" id="email_gerente" placeholder="Ex: gerente@singulo.com.br" value="<?php if(isset($imobiliaria)){ echo $imobiliaria->email_gerente; } ?>">
						</div>

				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab-email">
					<div class="form-group col-xs-6">
						<label>Email sender</label>
						<input type="email" class="form-control" name="email_sender" id="email_sender" placeholder="Ex: site@singulo.com.br"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->email_sender; } ?>">
					</div>
					<div class="form-group col-xs-6">
						<label>Senha</label>
						<input type="password" class="form-control" name="email_sender_senha" id="email_sender_senha" placeholder="Email sender senha"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->email_sender_senha; } ?>">
					</div>
					<div class="form-group col-xs-6">
						<label>Host</label>
						<input type="text" class="form-control" name="email_sender_host" id="email_sender_host" placeholder="Ex: smtp.singulo.com.br"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->email_sender_host; } ?>">
					</div>
					<div class="form-group col-xs-6">
						<label>Porta</label>
						<input type="text" class="form-control" name="email_sender_porta" id="email_sender_porta" placeholder="Ex: 587"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->email_sender_porta; } ?>">
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab-redes-sociais">
					<div class="form-group col-xs-12">
						<label>Facebook</label>
						<input type="text" class="form-control" name="facebook" id="facebook" placeholder="Link do perfil do Facebook"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->facebook; } ?>">
					</div>
					<div class="form-group col-xs-12">
						<label>Instagram</label>
						<input type="text" class="form-control" name="instagram" id="instagram" placeholder="Link do perfil do Instagram"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->instagram; } ?>">
					</div>
					<div class="form-group col-xs-12">
						<label>Twitter</label>
						<input type="text" class="form-control" name="twitter" id="twitter" placeholder="Link do perfil do Twitter"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->twitter; } ?>">
					</div>
					<div class="form-group col-xs-12">
						<label>WhatsApp</label>
						<input type="text" class="form-control telefone-whatsapp" name="whatsapp" id="whatsapp" placeholder="Número do WhatsApp"  value="<?php if(isset($imobiliaria)){ echo $imobiliaria->whatsapp; } ?>">
					</div>
				</div>
			</div>
			<hr class="col-xs-12" style="height: 2px; width: 97%">
			<div class="col-xs-2 form-group">
				<div id="btn-salvar" class="form-group col-xs-12">
					<p><a href="#" class="btn btn-success" onclick="salvar();">Salvar</a></p>
				</div>
				<div id='carregando' style="display:none;" >
					<img src="<?= base_url('assets/admin/loading.gif'); ?>" class="img-responsive center-block loading-gif"/>
				</div>
			</div>
			<div class="col-xs-9 form-group">
				<br>
				<p style="color: orange">Ao modificar as configurações do meu negócio faça o login novamente para que as mudanças sejam aplicadas corretamente.</p>
			</div>
		</div>
	</form>

</div>
<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<style>
	.nav-tabs.nav-justified {
		margin-top: 15px;
	}

	.btn-success
	{
		margin-top: 15px;
	}

	.ver-no-google-maps {
		margin-top: 30px;
	}

	#latitude_longitude
	{
		width: 31.5%;
	}
</style>

<script>

	$("#cnpj").mask("99.999.999/9999-99");

	$.validator.addMethod("latitude_longitude", function(value, element) {
		return /^-?([0-8]?[0-9]|90)\.[0-9]{1,6},-?((1?[0-7]?|[0-9]?)[0-9]|180)\.[0-9]{1,6}$/.test(value.replace(' ', ''));
	}, "Latitude e longitude incorreta");

	$('#estado').val('<?php if(isset($imobiliaria)){ echo $imobiliaria->estado; } ?>');

	var $validator;
	$(document).ready(function() {

		obter_todos_corretores(
			function (data) {
				if(data.corretores.length > 0)
				{
					$('#corretor_padrao').append('<option value="0" disabled selected>--Selecione--</option>');

					$.each( data.corretores, function( key, value ) {
						$('#corretor_padrao').append($('<option>', {value:value.id_corretor, text:value.nome}));
					});

					$('#corretor_padrao').val('<?php if(isset($imobiliaria)){ echo $imobiliaria->corretor_padrao_id; } ?>');
				}
				else
				{
					$('#corretor_padrao').append('<option value="0" disabled selected>Nenhum resultado encontrado.</option>');
				}
			},
			function (jqXhr, textStatus, errorThrown) {
				alertify.error('Ocorreu um erro ao tentar obter os corretores!');
			}
		);

		$validator = $("#form-configuracao").validate({
			rules: {
				imobiliaria_nome: {
					required: true,
					maxlength: 25,
					minlength: 2,
				},
				cnpj: {
					required: true,
					maxlength: 18,
				},
				creci: {
					required: true,
					maxlength: 6,
				},
				cidade: {
					required: true
				},
				estado: {
					required: true
				},
				endereco: {
					required: true,
					maxlength: 300,
					minlength: 5
				},
				telefone_1: {
					require_from_group: [1, ".telefone"]
				},
				telefone_2: {
					require_from_group: [1, ".telefone"]
				},
				telefone_3: {
					require_from_group: [1, ".telefone"]
				},
				dominio_padrao: {
					required: true,
					maxlength : 33
				},
				latitude_longitude: {
					required: true,
				},
				email_padrao: {
					required: true,
				},
				email_sender: {
					required: true,
				},
				email_sender_senha: {
					required: true,
					maxlength: 50,
					minlength: 5,
				},
				email_sender_host: {
					required: true,
					maxlength: 100,
					minlength: 5,
				},
				email_sender_porta: {
					required: true,
				},
			}
		})
	});

	function salvar() {
		var $valid = $("#form-configuracao").valid();

		if(!$valid) {
			$validator.focusInvalid();
			return false;
		}
		else
		{
			$('#btn-salvar').hide();
			$('#carregando').show();

			var formData = new FormData($('#form-configuracao')[0]);

			$.ajax({

				url: $('#base_url').val() + 'admin/configuracao_imobiliaria_cadastrar',
				data: formData,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function (data) {
					console.log(data);

					data = JSON.parse(data);

					if(data.status == true)
					{
						alertify.success('Configurações salvas com sucesso.');

						//SETA A INPUT COM O ID DA IMOBILIARIA CASO SEJA EDITADO APOS A PRIMEIRA INSERÇÂO, PARA QUE SEJA EDITADO NAO CRIADO UMA NOVA CONFIG
						$('#imobiliaria_id').val(data.imobiliaria_id);
					}
					else
					{
						alertify.error('Ocorreu um erro ao tentar salvar as configurações! Certifique-se de ter modificado pelo menos um campo');
					}
				},
				error: function (request, error) {
					console.log(request);
					console.log(error);
					alertify.error('Ocorreu um erro ao tentar salvar as configurações!');
				},
				complete: function(){
					$('#btn-salvar').show();
					$('#carregando').hide();
				}
			});
		}
	}
</script>
