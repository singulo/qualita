<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
	<div class="page-breadcrumb">
		<ol class="breadcrumb container">
			<li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
			<li>Configuração</li>
			<li class="active">Banners</li>
		</ol>
	</div>
	<div class="page-title">
		<div class="container">
			<h3>Configuração / Banners</h3>
		</div>
	</div>
	<br/>
    <div id="main-wrapper" class="container">
    	<div class="col-md-12">
            <div role="tabpanel" class="panel panel-white">
                <?php for($x = 0; $x <= 4; $x++) : ?>
                    <form id="form-banners-<?= $x; ?>" enctype="multipart/form-data">
                        <input type="hidden" id="foto-id" name="foto-id" value="<?= $x; ?>"/>
                        <input class="foto-upload" id="foto-upload-<?= $x; ?>" onChange="<?='previewImage(this, \'foto-'.$x.'\');';?>" name="foto-upload" accept=".jpg" type="file" style="display:none;">
                        <br/><br/>
                        <div class="container">
                            
                            <div class="col-md-6">
                                <div class="foto">
									<a href="#" title="Trocar foto" type="button" onclick="abrir_file_directory(<?= $x; ?>)" class="btn-troca-foto">
										<img id="foto-<?= $x; ?>" onError="this.src=$('#base_url').val() + 'assets/images/banner/imagem-indisponivel.jpg';" src="<?= $_SESSION['filial']['banners_uri'] . $x; ?>.jpg">
									</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                            <h4>Banner principal - <?= $x+1; ?></h4>
                                <div class="col-md-11">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="mensagem" id="mensagem-<?= $x; ?>" placeholder="Descrição em até 40 caracteres." maxlength="40" value="<?php if(isset($banners[$x])){ echo $banners[$x]->mensagem; } ?>">
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <div class="col-md-6">
                                            <input type="text" class="form-control decimal" name="valor" id="valor-<?= $x; ?>" placeholder="Valor" value="<?php if(isset($banners[$x])){ echo $banners[$x]->valor; } ?>">
                                            </div>
                                            <div class="col-md-6">
                                            <select class="form-control col-md-6" name="valor-decimal" id="valor-decimal-<?= $x; ?>">
                                                <option value="mil" <?php if(isset($banners[$x]) && ($banners[$x]->valor_decimal == 'mil')){ echo 'selected'; } ?>>mil</option>
                                                <option value="mi" <?php if(isset($banners[$x]) && ($banners[$x]->valor_decimal == 'mi')){ echo 'selected'; } ?>>mi</option>
                                            </select>
                                            </div>
                                        </div>
										<div class="checkbox col-md-6">
											<label><input type="checkbox" id="valor-consulte-<?= $x; ?>" name="valor-consulte" <?php if(isset($banners[$x]) && $banners[$x]->valor == 'Consulte') {echo 'checked';} ?> >Consulte</label>
										</div>
										<div class="col-md-12">
											<div class="form-group col-md-3">
												<input type="text" maxlength="10" class="form-control" name="codigo-imovel" id="codigo-imovel-<?= $x; ?>" placeholder="Cod." value="<?php if(isset($banners[$x])){ echo $banners[$x]->codigo_imovel; } ?>">
											</div>
											<div class="form-group col-md-4">
												<a class="form-control" href="#" onclick="ver_imovel(<?= $x; ?>)" style="border: 0px;">Ver link do imóvel</a>
											</div>
										</div>
                                    </div>
                                    <div class="col-md-12">
                                        <button title="Salvar banner <?= $x + 1;?>" onclick="upload_foto(<?= $x;?>)" type="button" class="col-md-1 btn btn-success pull-right">
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                        </button>
                                        <button title="Remover banner <?= $x + 1;?>" onclick="deletarFoto(<?= $x;?>)" type="button" class="col-md-1 btn btn-danger pull-right">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                <? endfor; ?>
            </div>
        </div>
    </div>
</div>
<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script>
	function abrir_file_directory(fotoId)
	{
		$('#foto-upload-' + fotoId).attr('data-foto-num', fotoId);
		$('#foto-upload-' + fotoId).click();
	}

	function upload_foto(id)
	{
		console.log('uploading...');
		var formData = new FormData($('#form-banners-' + id)[0]);

		fotoElem = $('#foto-' + id);
		loadingElem = $("#foto-" + id).closest('.foto');
		blockUI(loadingElem);

		$.ajax({
			url: $('#base_url').val() + 'admin/configuracao_banners_cadastrar',
			data: formData,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (data) {
				console.log(data);

				data = JSON.parse(data);

				if(data.status)
				{
					alertify.success('Banner salvo com sucesso!');
				}
			},
			error: function(error){
				console.log('Error');
				console.log(error);
			},
			complete: function (){
				unblockUI(loadingElem);
			}
		});
	}

	function deletarFoto(id)
	{
		if(!confirm('Tem certeza que deseja remover o Banner ' + (id + 1) + '?'))
		{
			return;
		}

		fotoElem = $('#foto-' + id);
		loadingElem = $("#foto-" + id).closest('.foto');
		blockUI(loadingElem);

		$.ajax({
			url: $('#base_url').val() + 'admin/configuracao_banners_deletar',
			dataType: "json",
			data: { id : id},
			type: 'POST',
			success: function (data) {
				console.log(data);

				//data = JSON.parse(data);

				if(data.status)
				{
					alertify.success('Banner removido com sucesso!');

					//recarrega foto upload
					fotoElem.attr('src', $('#base_url').val() + '_assets/images/imagem-indisponivel.jpg');

					$('#mensagem-' + id).val('');
					$('#valor-' + id).val('');
					$('#valor-decimal-' + id).val('mil');
					$('#link-' + id).val('');
					$('#codigo-imovel-' + id).val('');
				}
				else {
					alertify.error('Nenhum banner removido!');
				}
			},
			error: function(error){
				console.log('Error');
				console.log(error);
			},
			complete: function (){
				unblockUI(loadingElem);
			}
		});
	}

	function ver_imovel(id)
	{
		window.open($('#base_url').val() + 'imovel?cod_imovel=' + $('#codigo-imovel-' + id).val() + '&filial=<?= strtolower($_SESSION['filial']['chave']); ?>', '_blank');
	}
</script>
