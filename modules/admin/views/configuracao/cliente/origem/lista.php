<? require_once(APPPATH . '../modules/admin/helpers/form_values_helper.php'); ?>

<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li>Configurações</li>
            <li>Clientes</li>
            <li class="active">Origem</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Origem</h3>
        </div>
    </div>
    <br/>
    <div id="main-wrapper" class="container">
        <div class="col-md-12">
            <a href="#modal-origem" data-origem-id="0" data-toggle="modal" class="btn btn-success btn-addon m-b-sm" ><i class="fa fa-plus"></i> Origem</a>
        </div>
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tabela-origem" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Origem</th>
                            </tr>
                            </thead>
                            <tbody>
                                <? foreach($origens as $origem) : ?>
                                    <tr>
                                       <td><?= $origem->id; ?></td>
                                       <td><?= $origem->origem; ?></td>
                                       <td><a href="#modal-origem" data-origem-id="<?= $origem->id; ?>" data-origem-nome="<?= $origem->origem; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

<div id="modal-origem" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Origem</h4>
            </div>
            <div class="modal-body">
                <form id="form-origem" onsubmit="return false;">
                    <div class="col-xs-12">
                        <div class="row">
                            <input type="hidden" name="id" value="0">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label control-label">Origem</label>
                                    <input type="text" class="form-control" name="origem">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-loading-text="Aguarde..." autocomplete="off" onclick="origem_salvar();">Salvar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script>

    function origem_salvar()
    {
        if(!$("#form-origem").valid())
            return;

        $('#modal-origem .modal-footer button').button('loading');

        ajaxPost(
            $('#form-origem').jsonify(),
            $('#base_url').val() + 'admin/configuracao/origem_salvar',
            {
                successCallback: function (data) {
                    $('#modal-origem').modal('hide');
                    alertify.success('Origem salvo com sucesso.');
                    setTimeout(function(){
                        location.reload();
                    }, 500)
                },
                failureCallback: function (data) {
                    if(data.msg != undefined)
                        alertify.error(data.msg);
                    else
                        alertify.error('Ocorreu um erro ao adicionar a origem. Tente novamente mais tarde.');
                },
                errorCallback: function () {
                    alertify.error('Ocorreu um erro ao adicionar a origem.');
                },
                completeCallback: function(){
                    $('#modal-origem .modal-footer button').button('reset');
                }
            }
        );
    }

    $('#modal-origem').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        var modal = $(this);
        modal.find('input[name="id"]').val(button.data('origem-id'));
        modal.find('input[name="origem"]').val(button.data('origem-nome'));
    });
</script>
