<? require_once(APPPATH . '../modules/admin/helpers/form_values_helper.php'); ?>

<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li>Configurações</li>
            <li>Clientes</li>
            <li class="active">Status</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Status</h3>
        </div>
    </div>
    <br/>
    <div id="main-wrapper" class="container">
        <div class="col-md-12">
            <a href="#modal-status" data-status-id="0" data-toggle="modal" class="btn btn-success btn-addon m-b-sm" ><i class="fa fa-plus"></i> Status</a>
        </div>
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tabela-status" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($status as $stat) : ?>
                                <tr>
                                    <td><?= $stat->id; ?></td>
                                    <td><?= $stat->status; ?></td>
                                    <td><a href="#modal-status" data-status-id="<?= $stat->id; ?>" data-status-nome="<?= $stat->status; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a></td>
                                </tr>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

<div id="modal-status" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Status</h4>
            </div>
            <div class="modal-body">
                <form id="form-status" onsubmit="return false;">
                    <div class="col-xs-12">
                        <div class="row">
                            <input type="hidden" name="id" value="0">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label control-label">Status</label>
                                    <input type="text" class="form-control" name="status">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="pull-right">
                            <button type="button" class="btn btn-success" data-loading-text="Aguarde..." autocomplete="off" onclick="status_salvar();">Salvar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script>

    function status_salvar()
    {
        if(!$("#form-status").valid())
            return;

        $('#modal-status .modal-footer button').button('loading');

        ajaxPost(
            $('#form-status').jsonify(),
            $('#base_url').val() + 'admin/configuracao/status_salvar',
            {
                successCallback: function (data) {
                    $('#modal-status').modal('hide');
                    alertify.success('Status salvo com sucesso.');
                    setTimeout(function(){
                        location.reload();
                    }, 500)
                },
                failureCallback: function (data) {
                    if(data.msg != undefined)
                        alertify.error(data.msg);
                    else
                        alertify.error('Ocorreu um erro ao adicionar o status. Tente novamente mais tarde.');
                },
                errorCallback: function () {
                    alertify.error('Ocorreu um erro ao adicionar o status.');
                },
                completeCallback: function(){
                    $('#modal-status .modal-footer button').button('reset');
                }
            }
        );
    }

    $('#modal-status').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        var modal = $(this);
        modal.find('input[name="id"]').val(button.data('status-id'));
        modal.find('input[name="status"]').val(button.data('status-nome'));
    });
</script>
