<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
	<div class="page-title">
		<div class="container">
			<h3>Configuração / Banners</h3>
		</div>
	</div>
	<br/>
    <div id="main-wrapper" class="container">
    	<div class="col-md-12">
            <div role="tabpanel" class="panel panel-white">
				<form class="container" id="form-banner-descricao">
					<div class="col-xs-12 form-group" style="margin-top: 25px; max-width: 94%;">
						<input type="text" class="form-control" name="titulo" placeholder="Título em até 50 caracteres" maxlength="50" value="<? if(isset($banner_descricao)) echo $banner_descricao->titulo; ?>">
					</div>
					<div class="col-xs-12 form-group" style="max-width: 94%;">
						<input type="text" class="form-control" name="descricao" placeholder="Descrição em até 250 caracteres" maxlength="250" value="<? if(isset($banner_descricao)) echo $banner_descricao->descricao; ?>">
					</div>
					<div class="col-md-4 form-group">
						<input type="text" class="form-control" name="valor" placeholder='Valor em até 15 caracteres. Use "Consulte" para imóveis sem valor.' maxlength="15" value="<? if(isset($banner_descricao)) echo $banner_descricao->valor; ?>">
					</div>
					<div class="col-md-2 form-group">
						<input type="text" class="form-control" name="codigo_imovel" id="codigo_imovel" placeholder="Cod." maxlength="10" value="<? if(isset($banner_descricao)) echo $banner_descricao->codigo_imovel; ?>">
					</div>
					<div class="col-md-2 form-group">
						<a class="form-control" href="#" onclick="ver_imovel()" style="border: 0px;">Ver link do imóvel</a>
					</div>
					<div class="col-xs-4">
						<button title="Salvar descrição do banner" id="btn-banner-descricao" onclick="salvar_descricao()" type="button" class="btn btn-success">
							<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						</button>
						<div class="loading" id="loading-descricao" style="display:none;">
							<img src="<?= base_url('assets/admin/loading.gif'); ?>" class="col-md-12"/>
						</div>
					</div>
				</form>

                <?php for($x = 0; $x <= 2; $x++) : ?>
                    <form id="form-banners-<?= $x; ?>" enctype="multipart/form-data">
                        <input type="hidden" id="foto-id" name="foto-id" value="<?= $x; ?>"/>
                        <input class="foto-upload" id="foto-upload-<?= $x; ?>" onChange="<?='previewImage(this, \'foto-'.$x.'\');';?>" name="foto-upload" accept=".jpg" type="file" style="display:none;">
                        <br/><br/>
                        <div class="container">
                            
                            <div class="col-md-6">
                                <div class="foto">
                                <div class="loading" id="loading-<?= $x; ?>" style="display:none;">
                                	<img src="<?= base_url('assets/admin/loading.gif'); ?>" class="col-md-12"/>
                            	</div>
                                <a href="#" title="Trocar foto" type="button" onclick="abrir_file_directory(<?= $x; ?>)" class="btn-troca-foto">
                                    <img id="foto-<?= $x; ?>" onError="this.src=$('#base_url').val() + 'assets/images/banner/imagem-indisponivel.jpg';" src="<?= $_SESSION['filial']['banners_uri'] . $x; ?>.jpg">
                                </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                            <h4>Banner principal - <?= $x+1; ?></h4>
                                <div class="col-md-11">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="mensagem" id="mensagem-<?= $x; ?>" placeholder="Descrição em até 40 caracteres." maxlength="40" value="<?php if(isset($banners[$x])){ echo $banners[$x]->mensagem; } ?>">
                                    </div>
                                    <div class="col-md-12">
                                        <button title="Salvar banner <?= $x + 1;?>" onclick="upload_foto(<?= $x;?>)" type="button" class="col-md-1 btn btn-success pull-right">
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                        </button>
                                        <button title="Remover banner <?= $x + 1;?>" onclick="deletarFoto(<?= $x;?>)" type="button" class="col-md-1 btn btn-danger pull-right">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                <? endfor; ?>
			</div>
		</div>
    </div>
</div>
<!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script>
	function abrir_file_directory(fotoId)
	{
		$('#foto-upload-' + fotoId).attr('data-foto-num', fotoId);
		$('#foto-upload-' + fotoId).click();
	}

	function upload_foto(id)
	{
		console.log('uploading...');
		var formData = new FormData($('#form-banners-' + id)[0]);

		fotoElem = $('#foto-' + id);
		loadingElem = $('#loading-' + id);
		loadingElem.show();

		$.ajax({
			url: $('#base_url').val() + 'admin/configuracao_banners_cadastrar',
			data: formData,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (data) {
				console.log(data);

				data = JSON.parse(data);

				if(data.status)
				{
					alertify.success('Banner salvo com sucesso!');
				}
			},
			error: function(error){
				console.log('Error');
				console.log(error);
			},
			complete: function (){
				loadingElem.hide();
			}
		});
	}

	function deletarFoto(id)
	{
		if(!confirm('Tem certeza que deseja remover o Banner ' + (id + 1) + '?'))
		{
			return;
		}

		fotoElem = $('#foto-' + id);
		loadingElem = $('#loading-' + id);
		loadingElem.show();

		$.ajax({
			url: $('#base_url').val() + 'admin/configuracao_banners_deletar',
			dataType: "json",
			data: { id : id},
			type: 'POST',
			success: function (data) {
				console.log(data);

				//data = JSON.parse(data);

				if(data.status)
				{
					alertify.success('Banner removido com sucesso!');

					//recarrega foto upload
					fotoElem.attr('src', $('#base_url').val() + '_assets/images/imagem-indisponivel.jpg');

					$('#mensagem-' + id).val('');
					$('#valor-' + id).val('');
					$('#valor-decimal-' + id).val('mil');
					$('#link-' + id).val('');
					$('#codigo-imovel-' + id).val('');
				}
				else {
					alertify.error('Nenhum banner removido!');
				}
			},
			error: function(error){
				console.log('Error');
				console.log(error);
			},
			complete: function (){
				loadingElem.hide();
			}
		});
	}

	function ver_imovel()
	{
		window.open($('#base_url').val() + 'imovel?cod_imovel=' + $('#codigo_imovel').val() + '&filial=<?= strtolower($_SESSION['filial']['chave']); ?>', '_blank');
	}

	function salvar_descricao()
	{
		$('#btn-banner-descricao').hide();
		$('#loading-descricao').show();

		$.ajax({
			url: $('#base_url').val() + 'admin/configuracao_banner_descricao_salvar',
			dataType: "json",
			data: $("#form-banner-descricao").jsonify(),
			type: 'POST',
			success: function (data) {
				if(data.status)
					alertify.success('Descrição salvo com sucesso.');
				else
					alertify.error('Erro ao salvar a descrição do banner. Certifique-se de ter alterado um dos campos.');
			},
			error: function(error){
				alertify.error('Ocorreu um erro ao salvar a descrição do banner!');
			},
			complete: function (){
				$('#loading-descricao').hide();
				$('#btn-banner-descricao').show();
			}
		});
	}
</script>
