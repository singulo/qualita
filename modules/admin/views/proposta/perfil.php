<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li><a href="<?= base_url('admin/proposta/lista'); ?>">Propostas</a></li>
            <li class="active">Por perfil</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Selecionar perfil</h3>
        </div>
    </div>
    <div id="main-wrapper" class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div id="rootwizard">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active" data-function-valid="clientes_selecionados();"><a href="#buscar-perfil" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Buscar perfil</a></li>
                                <li role="presentation" data-function-valid="imoveis_adicionados();"><a href="#imoveis" data-toggle="tab"><i class="fa fa-home m-r-xs"></i>Imóveis</a></li>
                                <li role="presentation" data-function-valid="mensagem_valida();"><a href="#mensagem" data-toggle="tab"><i class="fa fa-edit m-r-xs"></i>Mensagem</a></li>
                                <li role="presentation"><a href="#finalizar" data-toggle="tab"><i class="fa fa-send m-r-xs"></i>Finalizar</a></li>
                            </ul>

                            <div class="progress progress-sm m-t-sm">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                </div>
                            </div>
                            <form id="wizardForm">
                                <div class="tab-content">
                                    <div class="tab-pane active fade in" id="buscar-perfil">
                                        <div class="row m-b-lg">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="alert alert-info" role="alert">
                                                        <strong>Informação</strong> este filtro usa como base o perfil do cliente preenchido pelo corretor em primeiro lugar. Caso o perfil do usuário não esteja preenchido é efetuada uma consulta nos imóveis visualizados e montado um perfil.
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Cidade</label>
                                                            <select multiple class="form-control" name="cidades[]"></select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Tipo de imóvel</label>
                                                            <select multiple class="form-control" name="tipos[]"></select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Valor do imóvel de interesse</label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="input-group m-b-sm">
                                                                        <span class="input-group-addon">R$</span>
                                                                        <input type="text" class="form-control dinheiro" name="valor_min" placeholder="mínimo">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="input-group m-b-sm">
                                                                        <span class="input-group-addon">R$</span>
                                                                        <input type="text" class="col-md-6 form-control dinheiro" name="valor_max" placeholder="máximo">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-success" type="button" style="margin-top: 23px;" onclick="buscar();" data-loading-text="Buscando..." autocomplete="off">Buscar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <table id="tabela-clientes" class="table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nome</th>
                                                                <th>Email</th>
                                                                <th>Telefone</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="4" class="text-center">Faça a busca pelo perfil que deseja enviar a proposta.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="alert alert-warning text-center" role="alert">
                                                        <strong>Aviso</strong> selecione pelo menos 1 cliente e no máximo 10.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="imoveis">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table id="tabela-imoveis" class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Código</th>
                                                            <th>Tipo</th>
                                                            <th>Cidade</th>
                                                            <th>Valor</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4" class="text-center">Imóveis que deseja enviar a proposta.</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="alert alert-warning text-center" role="alert">
                                                    <strong>Aviso</strong> selecione pelo menos 1 imóvel e no máximo 5.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="mensagem">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-12">
                                                    <label>Digite uma mensagem para os clientes</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="summernote">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="alert alert-warning text-center" role="alert">
                                                <strong>Aviso</strong> digite pelo menos 5 caracteres e no máximo 350.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="finalizar">
                                        <div class="row">
                                            <div class="col-md-offset-4 col-md-4 text-center">
                                                <div class="checkbox">
                                                    <a href="#" onclick="ver_email();">Visualizar email</a>
                                                    <label><input type="checkbox" checked id="copia_remetente"> Receber cópia oculta</label>
                                                </div>
                                            </div>

                                            <div class="col-md-offset-3 col-md-4 text-center">
                                                <p class="text-left msg-enviado">Enviado 0 de 0</p>

                                                <div class="progress progress-xs">
                                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-success pull-left" data-loading-text="Enviando..." autocomplete="off" onclick="enviar_propostas()"><i class="fa fa-send m-r-xs"></i>Enviar</button>
                                        </div>
                                    </div>
                                    <ul class="pager wizard">
                                        <li class="previous"><a href="#" class="btn btn-default">Voltar</a></li>
                                        <li class="next"><a href="#" class="btn btn-default">Próximo</a></li>
                                        <li class="finish hidden"><a href="<?= base_url('admin/painel') ;?>" class="btn btn-default">Fim</a></li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Row -->

    </div><!-- Main Wrapper -->
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

    <!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>

<script src="<?= base_url('assets/simples/js/imovel.js'); ?>"></script>
<script src="<?= base_url('assets/admin/pages/proposta/js/perfil.js'); ?>"></script>
