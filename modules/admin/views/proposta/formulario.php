<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-title">
        <div class="container">
            <h3>Cliente / Proposta</h3>
        </div>
    </div>
    <br/>
    <div class="panel panel-white col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">
        <div class="painel-body">
            <div class="row">
                <form id="form-proposta" class="col-xs-12" onsubmit="return false;">
                    <div class="form-group col-xs-12" style="margin-top: 20px">
                        <label>Cliente: <strong><?= $cliente->nome ;?></strong></label>
                        <input type="hidden" name="nome" value="<?= $cliente->nome ;?>">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Email: <strong><?= $cliente->email ;?></strong></label>
                        <input type="hidden" name="email" value="<?= $cliente->email ;?>">
                    </div>
                    <? require_once(APPPATH . '../modules/admin/helpers/form_values_helper.php'); ?>
                    <div class="form-group col-xs-12 col-md-5">
                        <label class="control-label">Assunto</label>
                        <div class="summernote">
                            <div style="text-align: center;">
                                <span><?= form_set_value($proposta, 'assunto', 'Selecionei alguns imóveis que talvez você goste.');?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <label class="control-label">Código do imóvel</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="cod_imovel" placeholder="Código do imóvel" value="<?= form_set_value($proposta, 'cod_imoveis');?>">
                            <span class="input-group-btn">
                                <button id="btn-busca-imovel" type="button" class="btn btn-success pull-right" onclick="buscar_imovel();" data-loading-text="Buscando..." autocomplete="off">Adicionar</button>
                            </span>
                        </div>
                        <table id="tabela-imoveis" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Tipo</th>
                                <th>Cidade</th>
                                <th>Valor</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4" class="text-center">Imóveis que deseja enviar a proposta.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="checkbox pull-left">
                                <label class="no-padding-left"><input type="checkbox" checked id="copia_remetente"> Receber cópia oculta</label>
                            </div>
                            <button type="button" id="btn-enviar" class="btn btn-success pull-right" onclick="enviar_proposta()"><i class="fa fa-send m-r-xs"></i><?= isset($proposta) ? 'Reenviar' : 'Enviar';?></button>
                            <button type="button" class="btn btn-default pull-right" onclick="visualizar_proposta();"><i class="fa fa-eye m-r-xs"></i>Visualizar proposta</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>
<script src="<?= base_url('assets/simples/js/imovel.js'); ?>"></script>
<script src="<?= base_url('assets/admin/pages/proposta/js/formulario.js'); ?>"></script>
