<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li><a href="<?= base_url('admin/painel'); ?>">Painel</a></li>
            <li class="active">Propostas</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Propostas</h3>
        </div>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row m-t-md">
            <div class="col-md-2">
                <ul class="list-unstyled mailbox-nav">
                    <li class="active"><a href="#"><i class="fa fa-sign-out"></i>Enviadas <span class="badge badge-success pull-right"><?= count($propostas); ?></span></a></li>
                </ul>
            </div>
            <div class="col-md-10">
                <div class="panel panel-white">
                    <div class="panel-body mailbox-content">
                        <table class="table">
                            <thead>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Data</th>
                                <th>Imóvel</th>
                            </thead>
                            <tbody>
                                <? foreach($propostas as $proposta) : ?>
                                    <tr class="unread" onclick="window.location.href = '<?= base_url('admin/proposta/formulario?email=' . $proposta->cliente_email . '&proposta=' . $proposta->id); ?>';">
                                        <td><?= $proposta->cliente_nome; ?></td>
                                        <td><?= $proposta->cliente_email; ?></td>
                                        <td><?= date('d/m/y h:i', strtotime($proposta->data)); ?></td>
                                        <td><?
                                            $imoveis = explode(',', $proposta->cod_imoveis);
                                            if(count($imoveis) > 1)
                                                echo count($imoveis) . ' imóveis';
                                            else
                                                echo $proposta->cod_imoveis;
                                            ?>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- Row -->
    </div><!-- Main Wrapper -->
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

    <!-- Javascripts -->
<?php $this->load->view('admin/footer'); ?>
<script src="<?= base_url('assets/admin/js/inbox.js'); ?>"></script>

<script>

    //Adiciona classe de ativo ao clique
    $('.mailbox-nav li').on('click', function(){

        $('.mailbox-nav li').removeClass('active');

        $(this).addClass('active');
    });
</script>
