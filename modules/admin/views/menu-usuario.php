<?php $this->load->view('admin/head'); ?>

<div class="navbar">
    <div class="navbar-inner container">
        <div class="sidebar-pusher">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="logo-box">
            <a href="<?= base_url('admin/painel'); ?>" class="logo-text"><span><?= $_SESSION['filial']['nome'] ?></span></a>
        </div><!-- Logo Box -->
        <div class="topmenu-outer">
            <div class="top-menu">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="#" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                    </li>
                    <li>
                        <a href="#" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                            <i class="fa fa-bell"></i>
                            <span id="notificacoes_quantidade" class="badge badge-success pull-right" style="display: none" ></span>
                        </a>
                        <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                            <li><p id="notificacao_titulo" class="drop-title">Notificações</p></li>
                            <li class="dropdown-menu-list slimscroll tasks">
                                <ul id="notificacoes" class="list-unstyled">
                                </ul>
                            </li>
                            <li class="text-center notificacao-desktop">
                                <a href="#" id="message-grant-notification" style="display: none;">
                                    <span><i class="fa  fa-bullhorn"></i></span>
                                    Habilitar notificações na área de trabalho!
                                </a>
                                <a href="#" style="display: none;" id="message-enable-notification" title="Desabilitar notificação da área de trabalho" onclick="$.cookie('notificacao-status-off', 'true', { expires: 1, path: '/'});"><span><i class="fa fa-toggle-on"></i></span> Notificações na área de trabalho</a>
                                <a href="#" style="display: none;" id="message-disable-notification" title="Habilitar notificação da área de trabalho" onclick="$.removeCookie('notificacao-status-off', { path: '/' });"><span><i class="fa fa-toggle-off"></i></span> Notificações na área de trabalho</a>
                            </li>
                            <li class="drop-all"><a href="<?= base_url('admin/notificacao/lista'); ?>" class="text-center">Ver todos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                            <!-- Nome do usuário -->
                            <span class="user-name"><?= $this->session->userdata('admin')->nome;?><i class="fa fa-angle-down"></i></span>
                            <!-- Foto do usuário -->
                            <img class="img-circle avatar" src="<?= $_SESSION['filial']['fotos_corretores'] . $this->session->userdata('admin')->id_corretor . ".jpg?random=" . rand(); ?>" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';" width="40" height="40" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-list" role="menu">
                            <li role="presentation"><a href="<?= base_url('admin/corretor/formulario?id=' . $this->session->userdata('admin')->id_corretor);?>"><i class="fa fa-edit"></i>Editar perfil</a></li>
                            <li role="presentation"><a href="<?= base_url('admin/lembrete/lista'); ?>"><i class="icon-pin"></i>Lembretes</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a href="<?= base_url('admin/logout'); ?>"><i class="fa fa-sign-out m-r-xs"></i>Sair</a></li>
                        </ul>
                    </li>
                </ul><!-- Nav -->
            </div><!-- Top Menu -->
        </div>
    </div>
</div>

<!-- MODAL DETALHE DA NOTIFICAÇÂO -->
<?php $this->load->view('admin/notificacao/detalhe'); ?>

<!-- MODAL LEMBRETE -->
<?php $this->load->view('admin/lembrete/formulario'); ?>

<script>

    var pagina_titulo_inicial = document.title;

    notificacoes_atualizadas.successCallback.push(obter_notificacoes_nao_respondidas);

    function obter_notificacoes_nao_respondidas()
    {
        $.ajax({
            url : $('#base_url').val() + 'admin/notificacao/nao_respondidas',
            type : 'POST',
            dataType: "json",
            success : function(data) {

                $('#notificacoes').html("");

                if(data.length > 0)
                {
                    $('#notificacoes_quantidade').show();
                    $('#notificacoes_quantidade').text(data.length);
                    $('#notificacao_titulo').text('Você tem ' + data.length + ' clientes não respondidos.');

                    document.title = '(' + data.length + ') ' + pagina_titulo_inicial;

                    $.each(data, function (i, item) {

                        var itemJson = JSON.stringify(item).replace(/"/g, "\'");

                        $('#notificacoes').append('<li><a href="#" onclick="popula_modal_notificacao(' + itemJson + ')">' + item.nome + ' - ' + item.assunto + '</a></li>');
                    });

                    var titulo = data.length > 1 ? data.length + ' novas notificações' : 'Nova notificação';

                    notifyDesktop(titulo, 'Clique para ir ao site.');
                }
                else
                {
                    $('#notificacoes_quantidade').hide();
                    $('#notificacao_titulo').text('Todos seus clientes foram respondidos');

                    document.title = pagina_titulo_inicial;
                }
            },
            error : function(request, error)
            {
                console.log('Erro ao obter as notificações');
                console.log(request);
                console.log(error);
            },
            complete: function (data) {
            }
        });
    }

    $(document).ready(function(){
        obter_notificacoes_nao_respondidas();
        setInterval(function(){ obter_notificacoes_nao_respondidas(); }, 30000);
    });
</script>
