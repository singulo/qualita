<? require_once(APPPATH . '../modules/admin/helpers/form_values_helper.php'); ?>

<?php $this->load->view('admin/painel'); ?>

<div class="page-inner">
    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <li class="active">Relatórios</li>
        </ol>
    </div>
    <div class="page-title">
        <div class="container">
            <h3>Relatórios</h3>
        </div>
    </div>

    <div id="main-wrapper" class="container">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Selecione o período do relatório</h4>
                    </div>
                    <div class="panel-body">

                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab-mes-ano" role="tab" data-toggle="tab">Mês/Ano</a></li>
                                <li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Período</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade in" id="tab-mes-ano">
                                    <div class="col-md-offset-4 col-md-4">
                                        <form class="form-inline" onsubmit="gerar_relatorio_mes_ano(); return false;">
                                            <div class="form-group">
                                                <label for="periodo">Mês</label>
                                                <select name="mes" class="form-control">
                                                    <?= form_options_month(); ?>
                                                </select>
                                                <label for="ano">Ano</label>
                                                <input type="text" class="form-control ano" name="ano" style="width: 50px;">
                                                <button class="btn btn-primary" onclick="gerar_relatorio_mes_ano();"><span class="fa fa-line-chart"></span> Gerar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab6">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="periodo">Período</label>
                                            <input type="text" class="form-control" name="periodo">
                                            <button class="btn btn-primary">Gerar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <hr>

                        <div class="tabs-left" role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab-clientes" role="tab" data-toggle="tab"><span class="fa fa-user"></span> Clientes</a></li>
                                <li role="presentation"><a href="#tab-corretores" role="tab" data-toggle="tab"><span class="fa fa-briefcase"></span> Corretores</a></li>
                                <li role="presentation"><a href="#tab-imoveis" role="tab" data-toggle="tab"><span class="fa fa-home"></span> Imóveis</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade in" id="tab-clientes">

                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Mês</th>
                                                <th>Cadastros</th>
                                                <th>Acessos</th>
                                                <th>Imóveis visualizados</th>
                                                <th>Interesses em imóveis</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="5" class="text-center">Nada gerado ainda.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab-corretores">

                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Clientes total</th>
                                                <th>Clientes no periodo</th>
                                                <th>Propostas enviadas</th>
                                                <th>Propostas distintas</th>
                                                <th>Acessos ao admin</th>
                                                <th>Interações no cadastro</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="7" class="text-center">Nada gerado ainda.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="tab-imoveis">
                                    <h4>Os 10 mais</h4>
                                    <div class="table-responsive">
                                        <div class="alert alert-info">
                                            Aqui são exibidos apenas os 10 imóveis mais acessado e que tiveram mais contato de interesse no período selecionado.
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-hover acessados">
                                                <thead>
                                                <tr>
                                                    <th><span class="fa fa-trophy"></span></th>
                                                    <th>Acessados</th>
                                                    <th><span class="fa fa-eye"></span></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="text-center">Nada gerado ainda.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-6">
                                            <table class="table table-hover interessantes">
                                                <thead>
                                                <tr>
                                                    <th><span class="fa fa-trophy"></span></th>
                                                    <th>Interessantes</th>
                                                    <th><span class="fa fa-star"></span></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="text-center">Nada gerado ainda.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="container">
            <?php $this->load->view('admin/copyright.php'); ?>
        </div>
    </div>
</div><!-- Page Inner -->

<?php $this->load->view('admin/footer'); ?>

<script>

    $(document).ready(function(){
        $validator = $("#tab-mes-ano form").validate({
            rules: {
                mes:{
                    required: true
                },
                ano: {
                    required: true
                }
            }
        });
    });

    function gerar_relatorio_mes_ano()
    {
        if(!$("#tab-mes-ano form").valid())
            return;

        ajaxPost(
            $('#tab-mes-ano form').jsonify(),
            $('#base_url').val() + 'admin/relatorio/mes_ano',
            {
                successCallback: function(data){
                    $('#tab-clientes table tbody').html('');

                    $('#tab-clientes table tbody').append('<tr><td>' + data.relatorio.mes + '/' + data.relatorio.ano + '</td><td>' + data.clientes.cadastrados + '</td><td>' + data.clientes.acessos + '</td><td>' + data.clientes.imoveis_visualizados + '</td><td>' + data.clientes.interesses_imoveis + '</td></tr>');

                    $('#tab-corretores table tbody').html('');

                    $.each(data.corretores, function(index, corretor){
                        $('#tab-corretores table tbody').append('<tr><td>' + corretor.nome + '</td><td>' + corretor.total_clientes + '</td><td> -- </td><td>' + corretor.propostas_enviadas + '</td><td>' + corretor.propostas_distintas + '</td><td> -- </td><td>' + corretor.interacoes_em_cadastros + '</td></tr>');
                    });

                    atualiza_tb_imoveis($('#tab-imoveis table.acessados tbody'), data.imoveis.acessados);

                    atualiza_tb_imoveis($('#tab-imoveis table.interessantes tbody'), data.imoveis.interessantes);
                },
                failureCallback: function(data){ console.log(data)},
                errorCallback: function(){
                    alertify.error('Ocorreu um erro ao gerar o relatório. Por favor tente novamente mais tarde');
                },
                blockElement: $('.tabs-left')
            }
        );
    }

    function atualiza_tb_imoveis(tb, imoveis)
    {
        $(tb).html('');

        $.each(imoveis, function(index, imovel){
            $(tb).append('<tr><td>' + (index + 1) + 'º</td><td><a href="' + $('#base_url').val() + 'imovel?cod_imovel=' + imovel.cod_imovel + '" target="_blank">' + imovel.cod_imovel + '</a></td><td>' + imovel.total + (imovel.total > 1 ? ' vezes' : ' vez') + '</td></tr>');
        });
    }

</script>
