<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');

/**
 * @property Relatorio_Model $relatorio_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Relatorio extends Admin_Controller
{
	protected $metodo_nivel_minimo = array(
		'index' 	=> 3, 		//ADMNivel::Gerente
		'mes_ano' 	=> 3, 		//ADMNivel::Gerente
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('relatorio_model');
	}

	public function index()
	{
		$this->load->view('admin/relatorio/relatorio');
	}

	public function mes_ano()
	{
		$mes = $_POST['mes'];
		$ano = $_POST['ano'];

		$data['status'] = true;

		$data['relatorio']['mes'] = $mes;
		$data['relatorio']['ano'] = $ano;
		$data['clientes']['cadastrados'] 			= $this->relatorio_model->cliente_cadastrados($mes, $ano);
		$data['clientes']['acessos'] 	 			= $this->relatorio_model->cliente_acessos($mes, $ano);
		$data['clientes']['imoveis_visualizados']	= $this->relatorio_model->cliente_imoveis_visualizados($mes, $ano);
		$data['clientes']['interesses_imoveis']		= $this->relatorio_model->cliente_interesses_imoveis($mes, $ano);

		$data['corretores'] = $this->relatorio_model->corretor_mes_ano($mes, $ano);

		$data['imoveis']['acessados'] 		= $this->relatorio_model->imoveis_acessados($mes, $ano, 10);
		$data['imoveis']['interessantes'] 	= $this->relatorio_model->imoveis_interessantes($mes, $ano, 10);

		echo json_encode($data);
	}
}