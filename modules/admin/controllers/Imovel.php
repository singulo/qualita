<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');

/**
 * @property Imoveis_Model $imoveis_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Imovel extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('imoveis_model');
	}

	public function pelo_codigo()
	{
		if(isset($_GET['cod_imoveis']))
		{
			$imoveis = $this->imoveis_model->pelos_codigos($_GET['cod_imoveis']);
			if(count($imoveis) > 0)
				echo json_encode(array('status' => true, 'imoveis' => $imoveis));
			else
				echo json_encode(array('status' => false, 'msg' => 'Nenhum imóvel não encontrado.'));
		}
		else
			echo json_encode(array('status' => false, 'msg' => 'Código não informado.'));

	}
}