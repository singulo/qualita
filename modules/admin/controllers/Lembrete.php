<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');

/**
 * @property Lembrete_Model $lembrete_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Lembrete extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('admin/lembrete_model');
	}

	public function lista()
	{
		$this->load->view('admin/lembrete/lista');
	}

	public function detalhes()
	{
		$lembrete = $this->lembrete_model->detalhe($_POST['id']);

		if(!is_null($lembrete))
			$resposta = array('status' => true, 'lembrete' => $lembrete);
		else
			$resposta = array('status' => false, 'msg' => 'Lembrete não encontrado.');

		echo json_encode($resposta);
	}

	public function salvar()
	{
		$this->load->library('RequestMapper');

		$mapper = array(
			'criado_em'  => array('default_value' => date("Y-m-d H:i:s")),
			'criado_por' => array('default_value' => $this->session->userdata('admin')->id_corretor)
		);

		/** @var Clientes_Model $cliente */
		$lembrete = RequestMapper::parseToObject($_POST, $mapper, new LembreteDomain());

		$lembrete->data = date_create($_POST['dia'] . ' ' . $_POST['hora'])->format('Y-m-d H:i:s');

		$destinatarios = is_array($_POST['destinatarios']) ? $_POST['destinatarios'] : array($_POST['destinatarios']);

		echo json_encode(array('status' => $this->lembrete_model->salvar($lembrete, $destinatarios), 'data' => $lembrete));
	}
	
	public function destinatarios()
	{
		$resposta = array('status' => false);
		
		if(isset($_POST['id']))
		{
			$destinatarios = array();
			foreach ($this->lembrete_model->destinatarios($_POST['id']) as $destinatario)
				$destinatarios[] = $destinatario->id_corretor;

			$resposta['status'] = true;
			$resposta['destinatarios'] = $destinatarios;
		}
		
		echo json_encode($resposta);
	}

	public function marcar_como_lido()
	{
		if(isset($_POST['id']))
			echo json_encode(array('status' => $this->lembrete_model->marcar_como_lido($_POST['id'], $this->session->userdata('admin')->id_corretor) > 0));
		else
			echo json_encode(array('status' => false));
	}

	public function nao_lidos()
	{
		$data['status'] = true;
		$data['lembretes'] = $this->lembrete_model->corretor($this->session->userdata('admin')->id_corretor);
		echo json_encode($data);
	}

	public function total()
	{
		echo json_encode($this->lembrete_model->total_por_corretor($this->session->userdata('admin')->id_corretor));
	}

	public function pesquisar()
	{
		$pagina = isset($_GET['pagina']) ? $_GET['pagina'] : 0;
		$limite = isset($_GET['limite']) ? $_GET['limite'] : 15;

		$filtro = $_GET;
		$filtro['id_corretor'] = $this->session->userdata('admin')->id_corretor;

		$resposta = $this->lembrete_model->pesquisar($filtro, $pagina, $limite);
		$resposta['status'] = true;

		echo json_encode($resposta);
	}
}