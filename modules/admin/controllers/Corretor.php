<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');
require_once(__DIR__ . '/../helpers/filial_helper.php');
require_once(__DIR__ . '/../helpers/email_helper.php');

/**
 * @property Clientes_Model $clientes_model
 * @property Clientes_imoveis_log_model $clientes_imoveis_log_model
 * @property Corretores_Model $corretores_model
 * @property Meta_Model $meta_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Corretor extends Admin_Controller
{
	protected $metodo_nivel_minimo = array(
		'index' 	=> 4, 		//ADMNivel::Secretaria
		'lista' 	=> 4, 		//ADMNivel::Secretaria
		'excluir' 	=> 3, 		//ADMNivel::Gerente
	);

	public function __construct()
	{
		$this->load->model('corretores_model');
	}

	public function index()
	{
		$data['corretores_total'] = $this->corretores_model->total();

		$this->load->view('admin/corretores/lista', $data);
	}

	public function formulario()
	{
		$admin = $this->session->userdata('admin');

		//VERIFICA SE O USUARIO COM NIVEL MENOR QUE GERENTE ESTA TENTANDO VISUALIZAR SEU PERFIL
		if($admin->nivel > ADMNivel::Secretaria && $_GET['id'] !=  $admin->id_corretor)
		{
			$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Você <strong>não</strong> tem permissão de acessar está página!', 'codigo' => 403));
		}

		$data['corretor'] = null;

		if(isset($_GET['id']))
			$data['corretor'] = $this->corretores_model->consulta_corretor_id($_GET['id']);

		$this->load->view('admin/corretores/formulario', $data);
	}

	public function lista()
	{
		$pagina = (isset($_GET['pagina']) ? $_GET['pagina'] : 0);
		$limite = (isset($_GET['limite']) ? $_GET['limite'] : 10);

		if(isset($_GET['todos']))
			$data['corretores'] = $this->corretores_model->listar_todos();
		else
			$data['corretores'] = $this->corretores_model->listar_todos_com_total_clientes_propostas($pagina, $limite);

		$data['status'] = true;

		echo json_encode($data);
	}

	public function cadastrar()
	{
		$resposta = array('status' => false);

		$this->load->library('RequestMapper');
		$mapper = array(
			'email' 	=> array('handle' => function($email){
				$config = $this->config->item('admin');
				return $email . '@' . $config['imobiliaria']['dominio'];
			}),
			'data_nasc' => array('handle' => function($data_nasc){
				if(strlen($data_nasc) != 0)
					return DateTime::createFromFormat('d/m/Y', $data_nasc)->format('Y-m-d');
			}));

		/** @var Corretores_Model $corretor*/
		$corretor = RequestMapper::parseToObject($_POST, $mapper, new CorretorDomain());

		$admin = $this->session->userdata('admin');

		if(!isset($_POST['id_corretor']) || strlen($_POST['id_corretor']) == 0)
		{
			$corretor->criado_em = date('Y-m-d H:i:s');
			$corretor->id_corretor = $this->corretores_model->novo($corretor);
			//VERIFICA SE O CORRETOR ESTA TENTANDO CADASTRAR ALGUEM COM O NIVEL SUPERIOR AO SEU
			if($corretor->nivel < $admin->nivel)
				$resposta['msg'] = 'Você não pode elevar o nível deste usuário!';
			else
				$resposta['status'] = ($corretor->id_corretor > 0);

			//COLOCA CORRETOR NA META
			$this->load->model('meta_model');
			$meta = $this->meta_model->obter_por_mes_ano(date('m'), date('Y'));
			if(!is_null($meta))
			{
				$resposta['status_adicionado_meta'] = $this->meta_model->adicionar_corretor($meta->id, array($corretor)) > 0;
			}
		}
		else
		{
			$corretor->id_corretor = $_POST['id_corretor'];
			//VERIFICA SE O CORRETOR ESTA TENTANDO ELEVAR O NIVEL SEU OU DE ALGUEM PARA MAIOR QUE O SEU
			if($corretor->nivel < $admin->nivel)
				$resposta['msg'] = 'Você não pode elevar o nível deste usuário!';
			else
			{
				unset($corretor->criado_em); //PARA NÃO SOBREESCREVER O CAMPO DATA DE CRIAÇÃO
				$resposta['status'] = ($this->corretores_model->editar($corretor) > 0);
			}
		}

		//FOTO
		if (!empty($_FILES["foto-upload"]['name'])) {

			$tipo = explode('.', $_FILES["foto-upload"]["name"]);
			$tipo = $tipo[count($tipo) - 1];
			$url = $_SESSION['filial']['fotos_corretores_upload'] . $corretor->id_corretor . '.' . strtolower($tipo);

			$resposta['status'] = (move_uploaded_file($_FILES["foto-upload"]["tmp_name"], $url));
		}

		//ASSINATURA
		if (!empty($_FILES["assinatura-upload"]['name'])) {

			$tipo = explode('.', $_FILES["assinatura-upload"]["name"]);
			$tipo = $tipo[count($tipo) - 1];
			$url = $_SESSION['filial']['fotos_corretores_assinaturas_upload'] . $corretor->id_corretor . '.' . strtolower($tipo);

			$resposta['status'] = (move_uploaded_file($_FILES["assinatura-upload"]["tmp_name"], $url));
		}

		echo json_encode($resposta);
	}

	public function excluir()
	{
		$total_clientes = $this->corretores_model->total_clientes($_POST['id_corretor']);

		if($total_clientes > 0 || !isset($_POST['id_corretor']))
			echo json_encode(array('status' => false, 'msg' => 'Existem ' . $total_clientes . ' vinculados a este corretor. Para excluir você precisa retirar todos esses vinculos.'));
		else
			echo json_encode(array('status' => $this->corretores_model->excluir($_POST['id_corretor'], $this->session->userdata('admin')->id_corretor)));
	}
}