<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');
require_once(__DIR__ . '/../helpers/filial_helper.php');
require_once(__DIR__ . '/../helpers/email_helper.php');

/**
 * @property Clientes_Model $clientes_model
 * @property Corretores_Model $corretores_model
 * @property Notificacao_Model $notificacao_model
 * @property Proposta_Model $proposta_model
 * @property Clientes_imoveis_log_model $clientes_imoveis_log_model
 * @property Imoveis_Model $imoveis_model
 * @property Cliente_Status_Model $cliente_status_model
 * @property Cliente_Origem_Model $cliente_origem_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Cliente extends Admin_Controller
{
	protected $metodo_nivel_minimo = array(
		'trocar_corretor' 					=> 1, 	//ADMNivel::Admn
		'transferir_clientes_de_corretor' 	=> 1, 	//ADMNivel::Admn
	);

	public function lista()
	{
		$this->load->model('cliente_status_model');
		$data['status'] = $this->cliente_status_model->listar_todos();

		$this->load->view('admin/clientes/lista', $data);
	}

	public function filtro()
	{
		$pagina = (isset($_GET['pagina']) ? $_GET['pagina'] : 0);
		$limite = (isset($_GET['limite']) ? $_GET['limite'] : 10);

		$filtro = array();

		if(isset($_GET['filtro_valor']))
		{
			$filtro[$_GET['filtro_campo']] =  $_GET['filtro_valor'];
		}

		if(isset($_GET['filtro_periodo_valor_inicio']))
		{
			$filtro[$_GET['filtro_periodo_campo_inicio'] . '_inicio'] =  DateTime::createFromFormat('d/m/Y', $_GET['filtro_periodo_valor_inicio'])->format('Y-m-d');
		}

		if(isset($_GET['filtro_periodo_valor_fim']))
		{
			$filtro[$_GET['filtro_periodo_campo_fim'] . '_fim'] =  DateTime::createFromFormat('d/m/Y', $_GET['filtro_periodo_valor_fim'])->format('Y-m-d');
		}

		if(isset($_GET['filtro_status']))
		{
			$filtro['id_status'] = $_GET['filtro_status'];
		}

		if(isset($_GET['filtro_corretor']))
		{
			$filtro['id_corretor'] = $_GET['filtro_corretor'];
		}

		$this->load->model('clientes_model');

		$resposta = array();

		$admin = $this->session->userdata('admin');
		if($admin->nivel > ADMNivel::Secretaria)
		{//NIVEL OPERACIONAL ADD FILTRO PELO CORRETOR LOGADO
			$filtro['id_corretor'] = $admin->id_corretor;
		}

		if($pagina == 0 || $_GET['forcar_total'] == 1)
		{//OBTER O TOTAL DE RESULTADOS PARA ATUALIZAR A PAGINAÇÃO
			$resposta['total'] = $this->clientes_model->filtro_total($filtro);
		}

		$resposta['clientes'] = $this->clientes_model->filtrar($filtro, $limite, $pagina);
		$resposta['status'] = true;

		echo json_encode($resposta);
	}

	public function formulario()
	{
		$data['cliente'] = null;

		$this->load->model('admin/corretores_model');
		$data['corretores'] = $this->corretores_model->listar_todos();
		
		$this->load->view('admin/clientes/perfil/perfil', $data);
	}

	public function perfil()
	{
		if(isset($_GET['id']))
		{
			$this->load->model('clientes_model');
			$data['cliente'] = $this->clientes_model->consulta_cliente_id($_GET['id']);

			if(is_null($data['cliente']) || ($data['cliente']->excluido))
			{
				$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Cliente <strong>não</strong> encontrado!'));

			}else
			{

				$this->load->model('admin/corretores_model');
				$corretores = $this->corretores_model->listar_todos();

				$data['corretores'] = array();
				foreach ($corretores as $corretor)
				{
					$data['corretores'][$corretor->id_corretor] = $corretor;
				}
	
				$this->load->model('cliente_status_model');
				$data['status'] = $this->cliente_status_model->listar_todos();

				$this->load->model('cliente_origem_model');
				$data['origens'] = $this->cliente_origem_model->listar_todos();

				if(is_null($data['cliente']) || $data['cliente']->excluido)
					$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Cliente <strong>não</strong> encontrado!'));
				else if(!$this->usuario_logado_esta_vinculado_ao_cliente($data['cliente']))
					$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Este cliente <strong>não</strong> está vinculado a você!', 'codigo' => 403));

				$this->load->model('admin/notificacao_model');
				$data['notificacoes'] = $this->notificacao_model->pelo_cliente($data['cliente']->email);

				$this->load->model('proposta_model');
				$data['propostas'] = $this->proposta_model->pelo_cliente($data['cliente']->email);

				$this->load->view('admin/clientes/perfil/perfil', $data);
			}
		}
		else
		{
			$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Cliente <strong>não</strong> informado!'));
		}
	}

	public function salvar_perfil()
	{
		$this->load->model('clientes_model');
        $this->load->model('notificacao_model');

        $resposta = array('status' => false);

		$cliente = $this->clientes_model->consulta_cliente_email($_POST['email']);
		if($cliente == null || ( $_POST['id'] > 0 && $_POST['id'] == $cliente->id )) // NOVO OU EDIÇÂO
		{
			$this->load->library('RequestMapper');

			$mapper = array(
				'atualizado_em' => array('default_value' => date("Y-m-d H:i:s")),
				'atualizado_por' => array('default_value' => $this->session->userdata('admin')->email));
			/** @var Clientes_Model $cliente */
			$cliente = RequestMapper::parseToObject($_POST, $mapper, new Clientes_Model());

			if($this->session->userdata('admin')->nivel <= ADMNivel::Secretaria)
				$cliente->id_corretor = $_POST['id_corretor'];
			else
				$cliente->id_corretor = $_POST['id_corretor_original'];


			if($cliente->id_corretor != $_POST['id_corretor_original'] && $cliente->id > 0)
			{
				$resposta['status_notificacao_corretor'] = $this->notificacao_model->cliente_vinculado($this->session->userdata('admin')->id_corretor, $cliente->id_corretor, $cliente->id) > 0;
			}

			if(isset($_POST['interesse_cidade']) && strlen($_POST['interesse_cidade'] > 0))
				$cliente->interesse_cidade = join(',', $_POST['interesse_cidade']);

			if(isset($_POST['interesse_tipo']) && strlen($_POST['interesse_tipo'] > 0))
				$cliente->interesse_tipo = join(',', $_POST['interesse_tipo']);

			if($cliente->id == 0)
			{
				$cliente->bloqueado = 0;
				$cliente->excluido = 0;
				$cliente->cadastrado_em = date("Y-m-d H:i:s");
				$resposta['cliente_id'] = $this->clientes_model->inserir($cliente);
				$resposta['status'] = ($resposta['cliente_id'] > 0);

				if($resposta['status'])
                {
                    $cliente->id = $resposta['cliente_id'];
                    $this->notificacao_model->cliente_vinculado(0, $cliente->id_corretor, $cliente->id);
                }
			}
			else
				$resposta['status'] = ($this->clientes_model->editar($cliente) > 0);
		}
		else
		{
			$resposta['msg'] = 'Email já cadastrado';
		}

		echo json_encode($resposta);
	}

	public function imoveis_visualizados_historico()
	{
		$this->load->model('clientes_imoveis_log_model');
		$data['imoveis'] = $this->clientes_imoveis_log_model->imoveis_visualizados($_POST['email'], $this->input->post('apartir_de'), $this->input->post('ate'));

		echo json_encode($data);
	}

	public function imoveis_visualizados()
	{
		$this->load->model('clientes_model');

		if(isset($_GET['id']))
			$data['cliente'] = $this->clientes_model->obter($_GET['id']);
		else
			$data['cliente'] = $this->clientes_model->consulta_cliente_email($_GET['email']);

		if(!$this->usuario_logado_esta_vinculado_ao_cliente($data['cliente']))
			$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Este cliente <strong>não</strong> está vinculado a você!', 'codigo' => 403));

		$this->load->view('clientes/imoveis-visualizados', $data);
	}

	public function imoveis_visualizados_apartir()
	{
		$this->load->model('clientes_imoveis_log_model');
		$imoveis = $this->clientes_imoveis_log_model->imoveis_visualizados($_POST['email'], $this->input->post('apartir_de'));

		$dias = array();
		foreach($imoveis as $imovel)
		{
			$dias[] = $imovel->dia_visualizacao;
		}

		array_unique($dias);

		foreach(array_unique($dias) as $dia)
		{
			$data['dias'][$dia] = array_filter($imoveis, function ($imovel) use ($dia){
				return $imovel->dia_visualizacao == $dia;
			});
		}

		echo json_encode($data);
	}

	public function bloquear()
	{
		$this->load->model('clientes_model');
		echo json_encode( array('status' => ($this->clientes_model->bloquear($_POST['id'], $this->session->userdata('admin')->email, true) > 0)));
	}

	public function desbloquear()
	{
		$this->load->model('clientes_model');
		echo json_encode( array('status' => ($this->clientes_model->bloquear($_POST['id'], $this->session->userdata('admin')->email, false) > 0)));
	}

	public function excluir()
	{
		$this->load->model('clientes_model');
		echo json_encode( array('status' => ($this->clientes_model->excluir($_POST['id'], $this->session->userdata('admin')->email,true) > 0)));
	}

	public function trocar_corretor()
	{
		$this->load->model('corretores_model');
		$data['corretores'] = $this->corretores_model->listar_todos();

		$this->load->view('admin/clientes/trocar_corretor', $data);
	}

	public function transferir_clientes_de_corretor()
	{
		$resposta['status'] = false;

		$this->load->model('clientes_model');
		
		$resposta['qnt_clientes'] = $this->clientes_model->trocar_corretor($_POST['corretor_envia_clientes'], $_POST['corretor_recebe_clientes']);

		if($resposta['qnt_clientes'] > 0)
		{
			$this->load->model('notificacao_model');
			$resposta['status_notificacao_corretor'] = $this->notificacao_model->cliente_vinculado($this->session->userdata('admin')->id_corretor, $_POST['corretor_recebe_clientes'], NULL, $resposta['qnt_clientes'] . ' clientes foram vinculados a você.') > 0;
			$resposta['status'] = true;
		}

		echo json_encode($resposta);
	}
}