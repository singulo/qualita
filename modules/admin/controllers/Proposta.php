<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');
require_once(__DIR__ . '/../helpers/filial_helper.php');
require_once(__DIR__ . '/../helpers/email_helper.php');

/**
 * @property Clientes_Model $clientes_model
 * @property Clientes_imoveis_log_model $clientes_imoveis_log_model
 * @property Imoveis_Model $imoveis_model
 * @property Proposta_Model $proposta_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Proposta extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('proposta_model');
	}

	public function lista()
	{
		$data['propostas'] = $this->proposta_model->pelo_corretor($this->session->userdata('admin')->id_corretor);

		$this->load->view('admin/proposta/lista', $data);
	}

	public function formulario()
	{
		$this->load->model('clientes_model');
		if(isset($_GET['id']))
			$data['cliente'] = $this->clientes_model->consulta_cliente_id($_GET['id']);
		else if(isset($_GET['email']))
			$data['cliente'] = $this->clientes_model->consulta_cliente_email($_GET['email']);

		$data['proposta'] = null;
		if(isset($_GET['proposta']))
		{
			$this->load->model('proposta_model');
			$data['proposta'] = $this->proposta_model->obter($_GET['proposta']);
		}

		if(is_null($data['cliente']) || $data['cliente']->excluido)
			$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Cliente <strong>não</strong> encontrado!'));
		else if(!$this->usuario_logado_esta_vinculado_ao_cliente($data['cliente']))
			$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Este cliente <strong>não</strong> está vinculado a você!', 'codigo' => 403));
		else
			$this->load->view('admin/proposta/formulario', $data);
	}

	public function visualizar()
	{
		$this->load->model('proposta_model');
		$proposta_model = $this->monta_proposta_dados($_GET);

		$data['proposta'] = $this->monta_proposta_html($proposta_model);

		$this->load->view('admin/proposta/visualizar', $data);
	}

	private function monta_proposta_dados($dados)
	{
		$this->load->model('proposta_model');
		$proposta_model = new Proposta_Model();
		$proposta_model->assunto 		= $dados['assunto'];
		$proposta_model->cliente_nome 	= $dados['cliente_nome'];
		$proposta_model->cliente_email 	= $dados['cliente_email'];

		if(!isset($dados['cod_imoveis']))
			$proposta_model->cod_imoveis = array();
		else if(is_array($dados['cod_imoveis']))
			$proposta_model->cod_imoveis = $dados['cod_imoveis'];
		else
			$proposta_model->cod_imoveis 	= array_map('trim', explode(',', $dados['cod_imoveis']));

		$proposta_model->id_corretor 	= $this->session->userdata('admin')->id_corretor;
		$proposta_model->data 			= date("Y-m-d H:i:s");

		return $proposta_model;
	}

	private function monta_proposta_html(Proposta_Model $proposta_model)
	{
		$this->load->model('imoveis_model');
		$imoveis_html = monta_div_imoveis_para_proposta($this->imoveis_model->pelos_codigos($proposta_model->cod_imoveis));

		$assinatura =  $_SESSION['filial']['fotos_corretores_assinaturas'].  $proposta_model->id_corretor . '.png';

		if(!@file_exists($_SESSION['filial']['fotos_corretores_assinaturas_upload'].  $proposta_model->id_corretor . '.png'))
			$assinatura = NULL;

		return monta_corpo_email(array(	'nome' 		=> $proposta_model->cliente_nome,
			'assunto'	=> $proposta_model->assunto,
			'imoveis' 	=> $imoveis_html),
			'proposta_para_cliente',
			'', $assinatura);
	}

	public function enviar()
	{
		$resposta['status'] = false;

		$this->load->model('proposta_model');
		$proposta_model = $this->monta_proposta_dados($_POST);

		$this->load->model('imoveis_model');
		if($this->imoveis_model->pelos_codigos($proposta_model->cod_imoveis) != null)
		{
			if(is_array($proposta_model->cod_imoveis))//Coloca codigos em string para salvar no banco
				$proposta_model->cod_imoveis = implode(',', $proposta_model->cod_imoveis);

			if($this->proposta_model->novo($proposta_model) > 0)
			{
				$proposta_model->cod_imoveis = array_map('trim', explode(',', $proposta_model->cod_imoveis));//transforma em array novamente para o email!

				$resposta['status'] = enviar_email(
					$proposta_model->cliente_email,
					'Proposta para você!',
					$this->monta_proposta_html($proposta_model),
					$this->session->userdata('admin')->email,
					(bool)$_POST['copia_remetente']);
			}
		}
		else
			$resposta['msg'] = 'Imóveis informados não foram encontrado. Por favor verifique.';

		echo json_encode($resposta);
	}

	public function por_perfil()
	{
		$this->load->view('admin/proposta/perfil');
	}

	public function perfil_filtro()
	{
		/* OBTER CLIENTES DE ACORDO COM O PERFIL PREENCHIDO */
		$this->load->model('clientes_model');

		$id_corretor = NULL;
		if($this->session->userdata('admin')->nivel > ADMNivel::Secretaria)//NIVEL OPERACIONAL ADD FILTRO PELO CORRETOR LOGADO
			$id_corretor = $this->session->userdata('admin')->id_corretor;

		$clientes = $this->clientes_model->pelo_perfil($_POST['cidades'], $_POST['tipos'], $_POST['valor_min'], $_POST['valor_max'], $id_corretor);

		$excluir_emails = array();
		foreach($clientes as $cliente)
			$excluir_emails[] = $cliente->email;

		/* OBTER CLIENTES DE ACORDO COM AS VISUALIZAÇÕES */
		$this->load->model('clientes_imoveis_log_model');
		$clientes += $this->clientes_imoveis_log_model->busca_perfil($_POST['cidades'], $_POST['tipos'], $excluir_emails, $_POST['valor_min'], $_POST['valor_max'], $id_corretor);

		$emails = array();
		foreach($clientes as $cliente)
			$emails[] = $cliente->email;

		/* OBTER IMOVEIS DE ACORDO COM O PERFIL DOS CLIENTS */
		$this->load->model('imoveis_model');
		$imoveis = $this->imoveis_model->pesquisar(array(	'f_tipo' 	=> $_POST['tipos'],
															'f_cidade' 	=> $_POST['cidades'],
															'preco_min' => $_POST['valor_min'],
															'preco_max' => $_POST['valor_max']),
													0, $this->imoveis_model->total());

		echo json_encode(array(	'status' 	=> true,
								'clientes' 	=> (count($emails) > 0 ? $this->clientes_model->com_os_email($emails) : array()),
								'imoveis' 	=> $imoveis));
	}
}