<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Controller.php');
require_once(__DIR__ . '/../helpers/filial_helper.php');
require_once(__DIR__ . '/../helpers/email_helper.php');

/**
 * @property Clientes_Model $clientes_model
 * @property Imoveis_Model $imoveis_model
 * @property Notificacao_Model $notificacao_model
 * @property Notificacao_Resposta_Model $notificacao_resposta_model
 * @property Contato_Imovel_Interesse_Model $contato_imovel_interesse_model
 * @property Contato_Model $contato_model
 * @property CI_Session $session
 */
class Notificacao extends Base_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('notificacao_model');
	}

	public function lista()
	{
		$this->load->view('admin/notificacao/lista');
	}

	public function nao_respondidas()
	{
		echo json_encode($this->notificacao_model->nao_respondidas($this->session->userdata('admin')->id_corretor));
	}

	public function obter()
	{
		switch($_POST['tipo'])
		{
			case 'normal':
				$this->load->model('contato_model');
				echo json_encode($this->contato_model->obter($_POST['id']));
			break;
			case 'interesse_em_imovel':
				$this->load->model('contato_imovel_interesse_model');
				echo json_encode($this->contato_imovel_interesse_model->obter($_POST['id']));
			break;
			case 'cliente_vinculado':
				echo json_encode($this->notificacao_model->do_corretor($_POST['id']));
			break;
		}
	}

	public function responder()
	{
		if(isset($_GET['id']))
		{
			$data['notificacao'] = $this->notificacao_model->obter_pelo_corretor($_GET['id'], $this->session->userdata('admin')->id_corretor);

			if(is_null($data['notificacao']))
				$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Notificação <strong>não</strong> encontrada!'));
			else
				$this->load->view('admin/notificacao/responder', $data);
		}
		else
			$this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Identificador da notificação <strong>não</strong> informado!'));
	}

	public function visualizar_resposta()
	{
		if($_GET['notificacao_tipo'] == 'normal')
			echo $this->monta_resposta_normal($_GET['id'], $_GET['assunto']);
		else if($_GET['notificacao_tipo'] == 'interesse_em_imovel')
			echo $this->monta_resposta_interesse_em_imovel($_GET['id'], $_GET['assunto']);
	}

	public function monta_resposta_normal($id, $resposta_corpo)
	{
		$this->load->model('contato_model');

		$contato = $this->contato_model->obter($id);

		$contato->assunto = $resposta_corpo;

		return $this->monta_resposta_normal_html($this->monta_resposta_dados((array)$contato));
	}

	/**
	 * Retorna o contato com o TIPO normal
	 * O tipo é o que diferencia as tabelas
	 * Tipos :
	 * 		normal = 'tb_contato',
	 * 		interesse_em_imovel = 'tb_contato_imovel_interesse'
	 */
	private function monta_resposta_interesse_em_imovel($id, $resposta_corpo)
	{
		$this->load->model('contato_imovel_interesse_model');

		$contato = $this->contato_imovel_interesse_model->obter($id);

		$contato->assunto = $resposta_corpo;

		return $this->monta_resposta_interesse_em_imovel_html($this->monta_resposta_dados((array)$contato));
	}

	private function monta_resposta_dados($dados)
	{
		$resposta = new stdClass();
		$resposta->assunto 			= $dados['assunto'];
		$resposta->cliente_nome 	= $dados['nome'];
		$resposta->cliente_email 	= $dados['email'];

		if(isset($dados['cod_imovel']))
			$resposta->cod_imoveis 		= array_map('trim', explode(',', $dados['cod_imovel']));

		$resposta->id_corretor 		= $this->session->userdata('admin')->id_corretor;
		$resposta->data 			= date("Y-m-d H:i:s");

		return $resposta;
	}

	private function monta_resposta_interesse_em_imovel_html($resposta)
	{
		$this->load->model('imoveis_model');
		$imoveis_html = monta_div_imoveis_para_proposta($this->imoveis_model->pelos_codigos($resposta->cod_imoveis));

		return monta_corpo_email(array(	'nome' 		=> $resposta->cliente_nome,
										'assunto'	=> $resposta->assunto,
										'imoveis' 	=> $imoveis_html),
										'resposta_para_cliente_interessado_em_imovel',
			'', $this->obter_assinatura_para_email($resposta->id_corretor));
	}

	private function monta_resposta_normal_html($resposta)
	{
		return monta_corpo_email(array(	'nome' 		=> $resposta->cliente_nome,
										'assunto'	=> $resposta->assunto),
										'resposta_para_cliente_normal',
			'', $this->obter_assinatura_para_email($resposta->id_corretor));
	}

	private function obter_assinatura_para_email($id_corretor)
	{
		$assinatura =  $_SESSION['filial']['fotos_corretores_assinaturas'].  $id_corretor . '.png';

		if(!file_exists($_SESSION['filial']['fotos_corretores_assinaturas_upload'].  $id_corretor . '.png'))
			$assinatura = NULL;

		return $assinatura;
	}

	public function enviar_resposta()
	{
		$this->load->model('notificacao_resposta_model');

		/** @var Notificacao_Resposta_Model $resposta */
		$resposta = $this->monta_resposta($_POST['id'], $_POST['notificacao_tipo'], $_POST['assunto']);

		if($_POST['notificacao_tipo'] == 'normal')
			$html = $this->monta_resposta_normal($resposta->contato_id, $resposta->resposta);
		else if($_POST['notificacao_tipo'] == 'interesse_em_imovel')
			$html = $this->monta_resposta_interesse_em_imovel($resposta->contato_id, $resposta->resposta);

		$enviado = enviar_email($resposta->cliente_email, 'Em resposta ao seu contato', $html, $resposta->corretor_email, (bool)$_POST['copia_remetente']);
		$salvo_resposta = $this->notificacao_resposta_model->novo($resposta) > 0;
		$salvo_notificacao = $this->notificacao_model->marcar_como_respondida($resposta->contato_id, $resposta->contato_tipo, $resposta->respondido_em) > 0;

		echo json_encode(array('status' => $enviado && $salvo_resposta && $salvo_notificacao));
	}

	private function monta_resposta($id , $notificacao_tipo, $resposta_corpo)
	{
		$this->load->model('notificacao_resposta_model');
		$this->load->model('contato_model');
		$this->load->model('contato_imovel_interesse_model');

		if($notificacao_tipo == 'normal')
			$notificacao = $this->contato_model->obter($id);
		else if($notificacao_tipo == 'interesse_em_imovel')
			$notificacao = $this->contato_imovel_interesse_model->obter($id);

		/** @var Notificacao_Resposta_Model $resposta */
		$resposta = new Notificacao_Resposta_Model();
		$resposta->id_corretor 		= $this->session->userdata('admin')->id_corretor;
		$resposta->cliente_email 	= $notificacao->email;
		$resposta->corretor_email 	= $this->session->userdata('admin')->email;
		$resposta->resposta 		= $resposta_corpo;
		$resposta->contato_id 		= $notificacao->id;
		$resposta->contato_tipo 	= $notificacao_tipo;
		$resposta->respondido_em 	= date("Y-m-d H:i:s");

		return $resposta;
	}

	public function marcar_respondida()
	{
		$data = date("Y-m-d H:i:s");
		$resposta = array('status' => false);

		if($this->notificacao_model->marcar_como_respondida($_POST['id'], $_POST['tipo'], $data) > 0)
		{
			$resposta['status'] = true;
			$resposta['respondido_em'] = $data;
		}

		echo json_encode($resposta);
	}

	public function marcar_como_nao_respondida()
	{
		echo json_encode(array('status' => $this->notificacao_model->marcar_como_nao_respondida($_POST['id'], $_POST['tipo']) > 0));
	}

	public function total()
	{
		echo json_encode($this->notificacao_model->total_por_corretor($this->session->userdata('admin')->id_corretor));
	}

	public function pesquisar()
	{
		$pagina = isset($_GET['pagina']) ? $_GET['pagina'] : 0;
		$limite = isset($_GET['limite']) ? $_GET['limite'] : 15;

		$filtro = $_GET;
		$filtro['id_corretor'] = $this->session->userdata('admin')->id_corretor;

		echo json_encode($this->notificacao_model->pesquisar($filtro, $pagina, $limite));
	}
}