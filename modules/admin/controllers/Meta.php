<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');

/**
 * @property Meta_Model $meta_model
 * @property Corretores_Model $corretores_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Meta extends Admin_Controller
{
	protected $metodo_nivel_minimo = array(
		'index' 					=> 3, 		//ADMNivel::Gerente
		'lista' 					=> 3, 		//ADMNivel::Gerente
		'adicionar' 				=> 3, 		//ADMNivel::Gerente
		'corretores' 				=> 3, 		//ADMNivel::Gerente
		'atualizar_meta_equipe' 	=> 3, 		//ADMNivel::Gerente
		'corretor_atualizar' 		=> 3, 		//ADMNivel::Gerente
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('meta_model');
	}

	public function index()
	{
		$data['metas_total'] = $this->meta_model->total();
		$this->load->view('admin/meta/lista', $data);
	}

	public function lista()
	{
		$pagina = (isset($_GET['pagina']) ? $_GET['pagina'] : 0);
		$limite = (isset($_GET['limite']) ? $_GET['limite'] : 10);

		$data['metas'] = $this->meta_model->listar($pagina, $limite);

		$data['status'] = true;

		echo json_encode($data);
	}

	public function adicionar()
	{
		$this->load->library('RequestMapper');

		$mapper = array(
			'meta_equipe' 	=> array('default_value' => 0),
			'valor_individual' => array('handle' => function($valor){
				return str_replace(',', '', $valor);
			}),
			'valor_equipe' => array('handle' => function($valor){
				return str_replace(',', '', $valor);
			}),);

		/** @var MetaDomain $meta*/
		$meta = RequestMapper::parseToObject($_POST, $mapper, new MetaDomain());

		if(is_null($this->meta_model->obter_por_mes_ano($meta->mes, $meta->ano)))
		{
			$this->load->model('admin/corretores_model');
			echo json_encode(array('status' => $this->meta_model->salvar($meta, $this->corretores_model->listar_todos())));
		}
		else
			echo json_encode(array('status' => false, 'msg' => 'Já existe uma meta para o mês e o ano escolhido.'));
	}

	public function corretores()
	{
		$data['meta'] = $this->meta_model->obter($_GET['id']);

		$data['corretores'] = $this->meta_model->obter_corretores_metas($_GET['id']);

		$this->load->view('admin/meta/corretores', $data);
	}

	public function atualizar_meta_equipe()
	{
		$meta = new stdClass();
		$meta->id = $_POST['pk'];
		$meta->meta_equipe = $_POST['value'];

		echo json_encode(array('status' => $this->meta_model->atualizar_meta_equipe($meta->id, $meta->meta_equipe) > 0));
	}

	public function corretor_atualizar()
	{
		$meta = new stdClass();
		$meta->id_meta_corretor = $_POST['pk'];
		$meta->meta = $_POST['value'];

		echo json_encode(array('status' => $this->meta_model->atualiza_meta_corretor($meta->id_meta_corretor, $meta->meta) > 0));
	}
}