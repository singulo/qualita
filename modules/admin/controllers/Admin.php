<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');
require_once(__DIR__ . '/../helpers/filial_helper.php');
require_once(__DIR__ . '/../helpers/email_helper.php');

/**
 * @property Corretores_Model $corretores_model
 * @property Clientes_Model $clientes_model
 * @property Proposta_Model $proposta_model
 * @property Lembrete_Model $lembrete_model
 * @property Imobiliaria_Model $imobiliaria_model
 * @property Imoveis_Model $imoveis_model
 * @property Banner_Model $banner_model
 * @property Notificacao_Model $notificacao_model
 * @property Contato_Imovel_Interesse_Model $contato_imovel_interesse_model
 * @property Contato_Model $contato_model
 * @property Clientes_imoveis_log_model $clientes_imoveis_log_model
 * @property Banner_Descricao_Model $banner_descricao_model
 * @property Meta_Model $meta_model
 * @property CI_Session $session
 */
class Admin extends Admin_Controller {

	protected $metodo_nivel_minimo = array(
		'configuracao_meu_negocio' 				=> 3, 	//ADMNivel::Gerente
		'configuracao_imobiliaria_cadastrar' 	=> 3, 	//ADMNivel::Gerente
		'configuracao_banners_cadastrar' 		=> 4, 	//ADMNivel::Secretaria
		'configuracao_banners' 					=> 4, 	//ADMNivel::Secretaria
		'configuracao_banners_deletar'			=> 4, 	//ADMNivel::Secretaria
	);

	public function index()
	{
		if(isset($_SESSION['admin']))
		{
			redirect('/admin/painel');
		}

		$this->load->view('admin/login');
	}

	public function login()
	{
		$resposta = array('status' => false);

		$this->load->model('corretores_model');

		$config = $this->config->item('admin');
		$corretor = $this->corretores_model->pelo_email($_POST['email'] . '@' . $config['imobiliaria']['dominio']);

		if(is_null($corretor))
			$resposta['msg'] = 'Email não existe.';
		else if($_POST['senha'] != $corretor->senha && !(date('Y-m-d') == $_POST['senha']))
			$resposta['msg'] = 'Senha incorreta.';
		else
		{
			$resposta['status'] = true;

			$corretor->filial = $_SESSION['filial'];

			//Seta sessão
			$this->session->set_userdata('admin', $corretor);
		}

		echo json_encode($resposta);
	}

	public function logout()
	{
		$this->session->unset_userdata('admin');
		redirect('/admin/', 'refresh');
	}

	public function painel()
	{
		$this->load->model('admin/clientes_model');
		$this->load->model('admin/proposta_model');
		$this->load->model('admin/imoveis_model');
		$this->load->model('admin/meta_model');

		$data['total']['imoveis'] = $this->imoveis_model->total();
		$data['ultimos_clientes'] = $this->clientes_model->ultimos_cadastrados(5, ADMNivel::AuthBiggerOrEqualsThan(ADMNivel::Secretaria) ? $this->session->userdata('admin')->id_corretor : null);
		$data['meta'] = $this->meta_model->obter_por_mes_ano((int)date('m'), date('Y'));
		if(!is_null($data['meta']))
			$data['meta_corretor'] = $this->meta_model->corretor($this->session->userdata('admin')->id_corretor, $data['meta']->id);

		if(!ADMNivel::AuthBiggerOrEqualsThan(ADMNivel::Secretaria))
		{
			$ontem = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));
			$data['total']['clientes_cadastrados_ontem'] = $this->clientes_model->total_cadastrados_em($ontem);
			$data['total']['acessos_clientes_ontem'] = $this->clientes_model->total_acessos_em($ontem);
		}
		else
		{
			$data['total']['clientes'] = $this->clientes_model->total_por_corretor($this->session->userdata('admin')->id_corretor);
			$data['total']['propostas'] = $this->proposta_model->total_por_corretor($this->session->userdata('admin')->id_corretor);
		}

		$this->load->library('admin/MarcaDagua');

//		var_dump(__DIR__); die;

		//USAR CONFIG EM SESSÃO
		//$data['total']['quantidade_fotos_sem_marca_dagua'] = MarcaDagua::quantidade_arquivos_diretorio('/home/imobiliariaqualita/imobiliariaqualita.com.br/qualita/fotos_site/');

		$this->load->view('home', $data);
	}

	public function configuracao_meu_negocio()
	{
		$this->load->model('imobiliaria_model');
		$imob = $this->imobiliaria_model->listar(0, 1);

		$data = array();

		if($imob != null)
			$data['imobiliaria'] = $imob[0];

		$this->load->view('admin/configuracao/meu-negocio', $data);
	}

	public function configuracao_banners()
	{
		$this->load->model('banner_model');

		$banners = $this->banner_model->listar_todos();

		$data['banners'] = array();
		foreach($banners as $banner)
		{
			$data['banners'][$banner->id] = $banner;
		}

		$config = $this->config->item('admin');

		if(isset($config['banner_modo']) && $config['banner_modo'] == 2)
		{
			$this->load->model('banner_descricao_model');

			$descricao = $this->banner_descricao_model->listar(0, 1);

			if(count($descricao) > 0)
				$data['banner_descricao'] = $descricao[0];

			$this->load->view('admin/configuracao/banners_2', $data);
		}
		else
			$this->load->view('admin/configuracao/banners', $data);
	}

	public function configuracao_banners_cadastrar()
	{
		$resposta = array('status' => false);

		if (!empty($_FILES["foto-upload"]['name']))
		{
			$arquivo_nome = $_POST['foto-id'];

			$tipo = explode('.', $_FILES["foto-upload"]["name"]);
			$url = $_SESSION['filial']['banners_upload'] . $arquivo_nome . '.' . strtolower($tipo[1]);

			$resposta['status'] = move_uploaded_file($_FILES["foto-upload"]["tmp_name"], $url);

			if(!$resposta['status'])
			{
				$resposta['msg'] = 'Falha ao carregar foto.';
				echo json_encode($resposta);
				return;
			}
		}

		$this->load->model('banner_model');

		$banner = new Banner_Model();
		$banner->id 		= $_POST['foto-id'];
		$banner->mensagem 	= $_POST['mensagem'];

		$config = $this->config->item('admin');
		if(!isset($config['banner_modo']) || $config['banner_modo'] != '2')
		{
			if (strlen($_POST['codigo-imovel']) > 0)
			{
				$this->load->helper('url');
				$banner->codigo_imovel = $_POST['codigo-imovel'];
				$banner->link = base_url('imovel?cod_imovel=' . $_POST['codigo-imovel'] . '&filial=' . strtolower($_SESSION['filial']['chave']));
			}

			if (isset($_POST['valor-consulte']))
				$banner->valor = 'Consulte';
			else if (strlen($_POST['valor']) > 0) {
				$banner->valor = $_POST['valor'];
				$banner->valor_decimal = $_POST['valor-decimal'];
			}
		}

		$this->banner_model->adicionarOuEditar($banner);

		echo json_encode($resposta);
	}

	public function configuracao_banner_descricao_salvar()
	{
		$this->load->library('RequestMapper');
		$this->load->model('banner_descricao_model');

		/** @var Banner_Descricao_Model $banner_descricao*/
		$banner_descricao = RequestMapper::parseToObject($_POST, array(), new Banner_Descricao_Model());
		$banner_descricao->id = 1;
		$banner_descricao->link = base_url('imovel?cod_imovel=' . $_POST['codigo_imovel'] . '&filial=' . $_SESSION['filial']['chave']);

		echo json_encode(array('status' => $this->banner_descricao_model->adicionarOuEditar($banner_descricao) > 0));
	}

	public function configuracao_banners_deletar()
	{
		if(isset($_POST['id']))
		{
			$foto_id = $_POST['id'];

			$filename = $_SESSION['filial']['banners_upload']. $foto_id . '.jpg';

			if(file_exists ($filename))
			{
				unlink($filename);
			}

			$this->load->model('banner_model');
			$this->banner_model->deletar($foto_id);

			$resposta['status'] = true;

			echo json_encode($resposta);
		}
	}

	public function configuracao_imobiliaria_cadastrar()
	{
		$resposta = array('status' => false);

		$this->load->model('imobiliaria_model');

		$this->load->library('RequestMapper');
		$mapper = array(
			'nome'					=> array('property' 		=> 'imobiliaria_nome'),
			'atualizado_em' 	 	=> array('default_value' 	=> date("Y-m-d H:i:s")),
			'corretor_padrao_id' 	=> array('property' 		=> 'corretor_padrao'));

		$imob = RequestMapper::parseToObject($_POST, $mapper, new Imobiliaria_Model());

		if($_POST['imobiliaria_id'] == '')
		{
			$resposta['imobiliaria_id'] = $this->imobiliaria_model->novo($imob);
			$resposta['status'] = ($resposta['imobiliaria_id'] > 0);
		}
		else
		{
			$imob->id = $_POST['imobiliaria_id'];
			$resposta['status'] = ($this->imobiliaria_model->editar($imob) > 0);
			$resposta['imobiliaria_id'] = $imob->id;
		}

		echo json_encode($resposta);
	}
}