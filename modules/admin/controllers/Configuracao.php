<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Admin_Controller.php');
require_once(MODULESPATH . 'admin/libraries/RequestMapper.php');

/**
 * @property Cliente_Status_Model $cliente_status_model
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Configuracao extends Admin_Controller
{
	protected $metodo_nivel_minimo = array(
		'status' 			=> 3, 		//ADMNivel::Gerente
		'status_salvar' 	=> 3, 		//ADMNivel::Gerente
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('cliente_status_model');
		$this->load->model('cliente_origem_model');
	}

	public function status()
	{
		$data['status'] = $this->cliente_status_model->listar_todos();
		$this->load->view('admin/configuracao/cliente/status/lista', $data);
	}

	public function status_salvar()
	{
		$status = RequestMapper::parseToObject($_POST, array(), new ClienteStatusDomain());

		$resultado = $this->cliente_status_model->salvar($status);

		echo json_encode(array('status' => $resultado, 'data' => $status));
	}

	public function origem()
	{
		$data['origens'] = $this->cliente_origem_model->listar_todos();
		$this->load->view('admin/configuracao/cliente/origem/lista', $data);
	}

	public function origem_salvar()
	{
		$origem = RequestMapper::parseToObject($_POST, array(), new ClienteOrigemDomain());

		$resultado = $this->cliente_origem_model->salvar($origem);

		echo json_encode(array('status' => $resultado, 'data' => $origem));
	}
}