<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Meta_Model extends Base_Model
{
    protected $table = 'tb_meta';

    public function salvar(MetaDomain $meta, array $corretores)
    {
        $this->db->trans_begin();

        $this->db->insert($this->table, $meta);

        $meta->id = $this->db->insert_id();

        if($meta->id <= 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            if($this->adicionar_corretor($meta->id, $corretores) > 0)
            {
                $this->db->trans_commit();
                return true;
            }
            else
            {
                $this->db->trans_rollback();
                return false;
            }
        }
    }

    public function adicionar_corretor($id_meta, array $corretores)
    {
        $meta_corretores = array();

        foreach($corretores as $corretor)
            $meta_corretores[] = array('id_meta' => $id_meta, 'id_corretor' => $corretor->id_corretor);

        $this->db->insert_batch('tb_meta_corretor', $meta_corretores);

        return $this->db->affected_rows();
    }

    public function listar($page, $limit)
    {
        $this->db->select("$this->table.*, (meta_equipe * 100) / valor_equipe  as progresso")
                ->order_by('mes', 'DESC')
                ->order_by('ano', 'DESC');
        return parent::listar($page, $limit);
    }

    public function obter_por_mes_ano($mes, $ano)
    {
        return $this->db
                    ->where('mes', $mes)
                    ->where('ano', $ano)
                    ->get($this->table)
                    ->first_row();
    }

    public function obter_corretores_metas($id_meta)
    {
        return $this->db->select("tb_corretores.*, tb_meta_corretor.id as id_meta_corretor, tb_meta_corretor.id_meta, tb_meta_corretor.meta")
                    ->join('tb_corretores', 'tb_corretores.id_corretor = tb_meta_corretor.id_corretor')
                    ->where('tb_meta_corretor.id_meta', $id_meta)
                    ->get('tb_meta_corretor')
                    ->result();
    }

    public  function atualizar_meta_equipe($id, $meta_equipe)
    {
        $this->db->set('meta_equipe', $meta_equipe)
            ->where('id', $id)
            ->update($this->table);

        return $this->db->affected_rows();
    }

    public function atualiza_meta_corretor($id_meta_corretor, $meta)
    {
        $this->db->set('meta', $meta)
                    ->where('id', $id_meta_corretor)
                    ->update('tb_meta_corretor');

        return $this->db->affected_rows();
    }

    public function corretor($id_corretor, $id_meta)
    {
        return $this->db
                    ->where('id_meta', $id_meta)
                    ->where('id_corretor', $id_corretor)
                    ->get('tb_meta_corretor')
                    ->first_row();
    }
}

class MetaDomain
{
    public $id;
    public $mes;
    public $ano;
    public $valor_individual;
    public $valor_equipe;
    public $meta_equipe;
}

class MetaCorretorDomain
{
    public $id;
    public $id_meta;
    public $id_corretor;
    public $meta;
}


