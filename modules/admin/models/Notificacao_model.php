<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Notificacao_Model extends Base_Model {

	public $id;
	public $tipo;
	public $nome;
	public $email;
	public $telefone;
	public $criado_em;
	public $respondido_em;
	public $assunto;
	public $id_corretor;

	protected $table = 'view_notificacoes';

	public function todas($id_corretor)
	{
		return $this->db->where('id_corretor', $id_corretor)
						->get($this->table)
						->result();
	}

	public function nao_respondidas($id_corretor)
	{
		return $this->db->query("
								SELECT * FROM $this->table WHERE id_corretor = $id_corretor AND respondido_em IS NULL
									UNION
								SELECT lemb.id, 'lembrete' as tipo, 'Lembrete' as nome, NULL as email, NULL as telefone, lemb.data, dest.lido_em as respondido_em, lemb.titulo, dest.id_corretor FROM tb_lembrete as lemb
									JOIN tb_lembrete_destinatario as dest ON lemb.id = dest.id_lembrete
									WHERE dest.id_corretor = $id_corretor AND lemb.data <= NOW() AND dest.lido_em IS NULL ORDER BY criado_em DESC")
								->result();

		/*return $this->db->where('id_corretor', $id_corretor)
						->where('respondido_em', null)
						->order_by('criado_em DESC')
						->get($this->table)
						->result();*/
	}

	public function marcar_como_respondida($id, $tipo)
	{
		return $this->trocar_status_notificacao($id, $tipo, true);
	}

	public function marcar_como_nao_respondida($id, $tipo)
	{
		return $this->trocar_status_notificacao($id, $tipo, false);
	}

	private function trocar_status_notificacao($id, $tipo, $respondida)
	{
		$table = '';
		$column = '';

		switch($tipo)
		{
			case 'normal':
				$table = 'tb_contato';
				$column = 'respondido_em';
				break;
			case 'interesse_em_imovel':
				$table = 'tb_contato_imovel_interesse';
				$column = 'respondido_em';
				break;
			case 'cliente_vinculado':
				$table = 'tb_corretor_notificacao';
				$column = 'lida_em';
				break;
		}

		$respondida ? $data = date("Y-m-d H:i:s") : $data = null;

		$this->db->where('id', $id);
		$this->db->update($table, array($column => $data));
		return $this->db->affected_rows();
	}

	public function obter($id, $campo = 'id')
	{
		$this->db->select('*, (SELECT id_corretor FROM tb_clientes WHERE tb_clientes.email = ' . $this->table . '.email LIMIT 1) as id_corretor');
		return parent::obter($id, $campo);
	}

	public function obter_pelo_corretor($id, $id_corretor)
	{
		$this->db->where('id_corretor', $id_corretor);
		return $this->obter($id);
	}

	public function pelo_cliente($email)
	{
		return $this->db->where('email', $email)->get($this->table)->result();
	}

	public function total_por_corretor($id_corretor)
	{
		$total = $this->db->query("select (SELECT count(*) FROM $this->table WHERE respondido_em IS NULL AND id_corretor = $id_corretor) as nao_respondidas,
				(SELECT count(*) FROM $this->table WHERE respondido_em IS NOT NULL AND id_corretor = $id_corretor) as respondidas")
				->first_row();

		$total->geral = $total->respondidas + $total->nao_respondidas;

		return $total;
	}

	public function pesquisar(array $filtro, $pagina, $limite)
	{
		$this->monta_filtro($filtro);

		$resultado['total'] = $this->db->count_all_results($this->table);

		$this->monta_filtro($filtro);

		$resultado['notificacoes'] = $this->db->limit($limite, ($pagina * $limite))
						->order_by('criado_em', 'DESC')
						->get($this->table)
						->result();

		return $resultado;
	}

	private function monta_filtro(array $filtros)
	{
		foreach($filtros as $campo => $filtro)
		{
			if(strlen($filtro) == 0)
				continue;

			switch($campo)
			{
				case 'id_corretor':
					$this->db->where('id_corretor', $filtro);
					break;
				case 'tipo':
					$this->db->where('tipo', $filtro);
					break;
				case 'nao_respondidas':
					$this->db->where('respondido_em', null);
					break;
				case 'respondidas':
					$this->db->where('respondido_em IS NOT NULL');
					break;
				case 'campos_contenham':
					$this->db->group_start();
					foreach(array('nome', 'email', 'assunto') as $coluna)
						$this->db->or_like($coluna, $filtro);

					$this->db->group_end();
					break;
			}
		}
	}

	public function cliente_vinculado($id_corretor_enviando, $id_corretor_para, $id_cliente = NULL, $mensagem = 'Um cliente foi vinculado a você.')
	{
		$notificacao = new NotificacaoCorretorDomain();
		$notificacao->id_corretor_enviou    = $id_corretor_enviando;
		$notificacao->id_corretor_para      = $id_corretor_para;
		$notificacao->id_cliente            = $id_cliente;
		$notificacao->tipo                  = 'cliente_vinculado';
		$notificacao->mensagem              = $mensagem;
		$notificacao->enviado_em            = date('Y-m-d H:i:s');

		$this->db->insert('tb_corretor_notificacao', $notificacao);
		return $this->db->insert_id();
	}

	public function do_corretor($id)
	{
		return $this->db->where('id', $id)
						->get('tb_corretor_notificacao')
						->first_row();
	}
}

class NotificacaoCorretorDomain
{
	public $id;
	public $id_corretor_enviou;
	public $id_corretor_para;
	public $id_cliente;
	public $mensagem;
	public $tipo;
	public $enviado_em;
	public $lida_em;
}