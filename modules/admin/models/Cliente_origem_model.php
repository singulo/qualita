<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');
require_once(MODULESPATH . 'admin/domain/ClienteOrigem.php');

class Cliente_Origem_Model extends Base_Model
{
    protected $table = 'tb_clientes_origem';

    public function salvar(ClienteOrigemDomain &$status)
    {
        if($status->id == 0)
        {
            $status->id = $this->inserir($status);
            return $status->id > 0;
        }

        else
            return $this->atualizar($status->id, $status) > 0;
    }
}

