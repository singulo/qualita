<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Imoveis_Model extends Base_Model {

	protected $table = 'tb_imoveis';

	public function apartamentos_destaque_mobiliados($limite)
	{
		$this->db->not_like('f_complementos_unidade', 'Semi-Mobiliado');
		$this->db->like('f_complementos_unidade', 'Mobiliado');
		return $this->destaques_pelo_tipo($limite, 'Apartamento');
	}

	public function condominios_destaque($limite)
	{
		return $this->destaques_pelo_tipo($limite, 'Casas em Condomínios');
	}

	public function salas_comerciais_destaque($limite)
	{
		return $this->destaques_pelo_tipo($limite, 'Sala Comercial');
	}

	/**
	 * @param string $tipo - Se $tipo for 'nulo' será consultado todos os tipos
	 * @return array
	 */
	public function destaques_pelo_tipo($limite, $tipos = NULL, $randomico = false, array $complementos_like = array(), array $complementos_not_like = array())
	{
		foreach($complementos_like as $complemento)
			$this->db->like('f_complementos_unidade', $complemento);

		foreach($complementos_not_like as $complemento)
			$this->db->not_like('f_complementos_unidade', $complemento);

		if(!is_array($tipos))
			$tipos = array($tipos);

		$this->db->limit($limite);
		if($tipos != NULL)
		{
			$this->db->group_start();
			foreach($tipos as $tipo)
			{
				$this->db->or_like('f_tipo', $tipo);
			}
			$this->db->group_end();
		}

		if($randomico)
			$this->db->order_by('RAND ()');

		$this->db->where('f_destaque', '1');
		return $this->obter_imovel_com_foto_destaque()
			->get($this->table)
			->result();
	}

	public function total_pelo_tipo($tipo)
	{
		$this->db->like('f_tipo', $tipo);
		return $this->db->count_all_results($this->table);
	}

	public function total()
	{
		return $this->db->count_all_results($this->table);
	}

	public function obter_alugueis($limit = NULL)
	{
		if($limit != NULL){
			$this->db->limit($limit);
		}

		return $this->db->where('f_finalidade', 2)
				->select('*, (SELECT f_id_foto FROM tb_imoveis_fotos WHERE tb_imoveis_fotos.f_id = tb_imoveis.f_id AND f_destaque = 1 LIMIT 1) as foto')
				->get($this->table)->result();
	}

	public function lancamentos($ordernar = "f_condominio ASC")
	{
		$this->obter_imovel_com_foto_destaque();
		return $this->db->where('f_lancamento', 1)
					->order_by($ordernar)
					->get($this->table)
					->result();
	}

	public function obter_lancamentos()
	{
		$this->db->where('f_lancamento', 1);
		$this->db->select('*, (SELECT f_id_foto FROM tb_imoveis_fotos WHERE tb_imoveis_fotos.f_id = tb_imoveis.f_id AND f_destaque = 1 LIMIT 1) as foto');
		$this->db->order_by("f_condominio", "ASC");
		return $this->db->get('tb_imoveis')->result();
	}

	private function obter_pelos_codigos(array $codigos)
	{
		if(count($codigos) > 0)
			$this->db->where_in('f_codigo', $codigos);

		$this->obter_imovel_com_foto_destaque();
		$this->db->order_by("f_valor", 'ASC');
		return $this->db->get('tb_imoveis');
	}

	private function obter_imovel_com_foto_destaque()
	{
		return $this->db->select('*, (SELECT f_id_foto FROM tb_imoveis_fotos WHERE tb_imoveis_fotos.f_id = tb_imoveis.f_id ORDER BY f_destaque DESC, ordem ASC LIMIT 1) as foto');
	}

	public function pelos_codigos(array $codigos)
	{
		return $this->obter_pelos_codigos($codigos)->result();
	}

	public function pelo_codigo($codigo)
	{
		return $this->obter_pelos_codigos(array($codigo))->first_row();
	}

	private function monta_filtro(array $filtro)
	{
		$valoresCond = array();
		if(isset($filtro['preco_min']) && $filtro['preco_min'] > 0)
			$valoresCond['f_valor >='] = $filtro['preco_min'];
		if(isset($filtro['preco_max']) && $filtro['preco_max'] > 0 && $filtro['preco_max'] != 3000000)
			$valoresCond['f_valor <='] = $filtro['preco_max'];

		unset($filtro['preco_min']);
		unset($filtro['preco_max']);

		$filtro = array_filter($filtro);

		$this->obter_imovel_com_foto_destaque();

		if(count($valoresCond) > 0)
			$this->db->where($valoresCond);

		foreach ($filtro as $key => $value)
		{
			if(is_array($value))
				$this->db->where_in($key, $value);
			else
				$this->db->like($key, $value);
		}
	}

	public function pesquisar(array $filtro, $pagina, $limite, $ordenar = '')
	{
		$this->monta_filtro($filtro);

		return $this->db->limit($limite, ($pagina * $limite))->order_by($ordenar)->get($this->table)->result();
	}

	public function pesquisar_total_resultados(array $filtro)
	{
		$this->monta_filtro($filtro);

		return $this->db->from($this->table)->count_all_results();
	}

	public function consulta_cidades_imoveis()
	{
		return $this->db->query('SELECT distinct(f_cidade) FROM tb_imoveis ORDER BY f_cidade ASC')->result();
	}

	public function consulta_bairros_imoveis()
	{
		return $this->db->query('SELECT distinct(f_bairro) FROM tb_imoveis ORDER BY f_bairro ASC')->result();
	}

	function consulta_tipos_imoveis()
	{
		return $this->db->query('SELECT distinct(f_tipo) FROM tb_imoveis ORDER BY f_tipo ASC')->result();
	}

	function consulta_imovel_pelo_id($id)
	{
		$this->db->where('f_id', $id);
		$this->db->select('*, (SELECT f_id_foto FROM tb_imoveis_fotos WHERE tb_imoveis_fotos.f_id = tb_imoveis.f_id AND f_destaque = 1 LIMIT 1) as foto');
		$query = $this->db->get('tb_imoveis');
		return $query->first_row();
	}

	function consulta_sugestao($id, $tipo, $condominio_nome, $limit)
	{
		//$this->db->order_by('id', 'RANDOM');
		$this->db->where('f_id !=', $id);
		$this->db->where('f_tipo', $tipo);
		$this->db->or_where('f_condominio', $condominio_nome);
		$this->db->limit($limit);
		$this->db->order_by('f_tipo ASC', 'f_condominio ASC');
		$this->db->select('*, (SELECT f_id_foto FROM tb_imoveis_fotos WHERE tb_imoveis_fotos.f_id = tb_imoveis.f_id AND f_destaque = 1 LIMIT 1) as foto');
		$query = $this->db->get('tb_imoveis');
		return $query->result();
	}

    public function fotos($codigo, $limite = null)
	{
		if(!is_null($limite))
			$this->db->limit($limite);

		$this->db->where('f_codigo',$codigo);
		$this->db->order_by("f_destaque DESC, ordem ASC");
		return $this->db->get('tb_imoveis_fotos')->result();
	}

	public function consulta_total_fotos($id_imovel)
	{
		$this->db->where('f_id', $id_imovel);
		$this->db->from('tb_imoveis_fotos');
		return $this->db->count_all_results();
	}

	public function consulta_imoveis_destaques()
	{
		$this->db->select('f_id,f_codigo,f_descricao,f_tipo,f_valor,(SELECT f_id_foto FROM tb_imoveis_fotos WHERE tb_imoveis_fotos.f_id = tb_imoveis.f_id AND f_destaque = 1 LIMIT 1) as foto');
		$this->db->limit(3);
		$query = $this->db->get('tb_imoveis');
		return $query->result();
	}

	public function consulta_sugestoes_randomizadas()
	{
		$this->obter_imovel_com_foto_destaque();
		$this->db->limit(6);
		$this->db->order_by('f_id','random');
		$query = $this->db->get('tb_imoveis');
		return $query->result();
	}

	public function consulta_imoveis_condominio_pelo_nome($condominio_nome, $ordernar = 'f_tipo ASC, f_valor ASC')
	{
		$this->obter_imovel_com_foto_destaque();
		$this->db->where("f_condominio", $condominio_nome);
		$this->db->not_like("f_tipo", "fechado");
		$this->db->order_by($ordernar);
		$query = $this->db->get('tb_imoveis');
		return $query->result();
	}

	public function consulta_valores_imoveis_em_condominio($condominio_nome)
	{
		$this->db->select('MIN( f_valor ) AS min, MAX( f_valor ) AS max');
		$this->db->where("f_condominio", $condominio_nome);
		$this->db->where_not_in('f_valor', array(0.00, 1.00));
		$query = $this->db->get($this->table);
		return $query->first_row();
	}

	public function condominio_pelo_nome($condominio_nome)
	{
		return $this->obter_imovel_com_foto_destaque()
			->like("f_tipo", 'fechado')
			->where("f_condominio", $condominio_nome)
			->get($this->table)
			->first_row();
	}

	public function condominios($limit = NULL)
	{
		$this->db->select('DISTINCT(f_condominio), f_condominio, f_codigo, f_cidade, f_descricao, f_descricao_abreviada, f_id, (SELECT f_id_foto FROM tb_imoveis_fotos WHERE tb_imoveis_fotos.f_id = tb_imoveis.f_id AND f_destaque = 1 ORDER BY ordem ASC LIMIT 1) as foto');
		$this->db->like('f_tipo','fechado');

		if($limit != NULL){
			$this->db->limit($limit);
		}

		$this->db->order_by("f_condominio", "ASC");
		return $this->db->get($this->table)->result();
	}
}