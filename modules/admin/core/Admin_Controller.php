<?php

require_once(__DIR__ . '/Base_Controller.php');

class Admin_Controller extends Base_Controller
{
    protected $metodo_nivel_minimo = array();

    public function __construct()
    {
        parent::__construct(false);

        $metodo_requisitado = $this->router->fetch_method();

        if(!$this->session->has_userdata('admin') && !$this->input->is_ajax_request() && $metodo_requisitado != 'index')
        {
            redirect('/admin/');
        }
        else if(isset($this->metodo_nivel_minimo[$metodo_requisitado]) && $this->metodo_nivel_minimo[$metodo_requisitado] < $this->session->userdata('admin')->nivel)
        {
            $this->load->view('admin/error/mensagem-personalizada', array('mensagem' => 'Você <strong>não</strong> tem permissão para acessar está área!', 'codigo' => 403));
        }
    }

    protected function usuario_logado_esta_vinculado_ao_cliente($cliente)
    {
        $admin = $this->session->userdata('admin');

        return !($cliente->id_corretor != $admin->id_corretor && $admin->nivel > ADMNivel::Agenciador);
    }
}