<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_DB_mysql_driver $db
 * @property CI_Session $session
 */
class Base_Model extends CI_Model
{
    protected $table;

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();

        $this->db = $this->load->database($this->session->userdata('filial')['chave'], true);
    }

    public function listar($page, $limit)
    {
        $this->db->limit($limit, ($page * $limit));
        return $this->db->get($this->table)->result();
    }

    public function listar_todos()
    {
        return $this->db->get($this->table)->result();
    }

    public function total()
    {
        return $this->db->count_all($this->table);
    }

    public function deletar($id, $campo = 'id')
    {
        $this->db->where($campo, $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    public function obter($id, $campo = 'id')
    {
        return $this->db->where($campo, $id)
            ->get($this->table)
            ->first_row();
    }

    protected function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }
    protected function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

    protected function updateOnDuplicateQuery($table, $values)
    {
        $updatestr = array();

        foreach((array)$values as $key => $val)
            $updatestr[] = $key." = VALUES(" . $key . ")";

        $sql  = $this->db->insert_string($table, $values);
        $sql .= "ON DUPLICATE KEY UPDATE ".implode(', ',$updatestr);

        return $sql;
    }

    public function updateOnDuplicate($values)
    {
        $this->db->query($this->updateOnDuplicateQuery($this->table, $values));
        return $this->db->affected_rows();
    }

    public function inserir($domain)
    {
        $this->db->insert($this->table, $domain);
        return $this->db->insert_id();
    }

    public function atualizar($id, $domain)
    {
        $this->db->where('id', $id)
            ->update($this->table, $domain);

        return $this->db->affected_rows();
    }
}