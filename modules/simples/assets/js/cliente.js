var on_cliente_entrar = {
    successCallback : [],
    failureCallback : [],
    errorCallback : []
};

function cliente_verifica_email(email, successCallback, failureCallback, errorCallback, completeCallback)
{
    //Fazer requisicao, checar o email e setar a sessao
    $.ajax({
        url : $('#base_url').val() + 'cliente/login',
        type : 'POST',
        data : { email : email},
        dataType: "json",
        success : function(data) {

            if(data.status == true)
            {//já esta cadastrado
                $.each( on_cliente_entrar.successCallback, function( key, action ) {
                    action(data.usuario);
                });

                successCallback(data.usuario);
            }
            else
            {
                $.each( on_cliente_entrar.failureCallback, function( key, action ) {
                    action(data);
                });

                failureCallback(data);
            }
        },
        error : function(jqXhr, textStatus, errorThrown)
        {
            $.each( on_cliente_entrar.errorCallback, function( key, action ) {
                action(data);
            });

            errorCallback(jqXhr, textStatus, errorThrown);
        },
        complete: function () {
            completeCallback();
        }
    });
}

function cliente_registrar(cliente, simpleRequest)
{
    ajaxPost(
        cliente,
        $('#base_url').val() + 'cliente/novo',
        simpleRequest
    );
}

function cliente_atualizar_dados(cliente, simpleRequest)
{
    ajaxPost(
        cliente,
        $('#base_url').val() + 'cliente/atualizar',
        simpleRequest
    );
}

function cliente_contato_novo(contato, simpleRequest)
{
    ajaxPost(
        contato,
        $('#base_url').val() + 'contato/novo',
        simpleRequest
    );
}

function cliente_interesse_em_imovel(cod_imovel, obs, simpleRequest)
{
    ajaxPost(
        {cod_imovel : cod_imovel, obs : obs},
        $('#base_url').val() + 'contato/interesse_imovel',
        simpleRequest
    );
}


function cliente_salvar_dados()
{
    if(!$("#form-dados").valid()) return;

    var $btn = $('.btn-salvar').button('loading');

    var dados = $("#form-dados").jsonify();

    cliente_atualizar_dados(
        dados,
        {
            successCallback: function(data){
                alertify.success('Seus dados foram atualizados!');
            },
            failureCallback: function(data){
                alertify.error('Por favor certifique-se de ter editado pelo menos um dos campo.');
            },
            errorCallback: function(){
                alertify.error('Ocorreu um erro ao atualizar seus dados.');
            },
            completeCallback: function(){
                $btn.button('reset');
            }
        });
}

on_cliente_entrar.successCallback.push(function(usuario){
    $(".nav li a[href='#modal-login']").text(usuario.nome);
    $(".nav li a[href='#modal-login']").attr('href', '#modal-dados');

    $(".nav li a:contains('HISTÓRICO')").show();

    $('#form-dados').dejsonify(usuario);

    var creci = '';

    if(usuario.corretor.nivel != 4)
        creci = 'creci: ' + usuario.corretor.creci;

    $('#modal-dados img.corretor-foto').attr('src', $('#filial_fotos_corretores').val() + usuario.corretor.id_corretor + '.jpg');
    $('#modal-dados .corretor-nome').text(usuario.corretor.nome);
    $('#modal-dados .corretor-creci').text(creci);
    $('#modal-dados .corretor-email').html('email: <a href="mailto:' + usuario.corretor.email + '" target="_top">' + usuario.corretor.email + '</a>');
});

$(document).ready(function() {

    $("#form-login").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        }
    });
});

function cliente_login()
{
    if(!$("#form-login").valid()) return;

    var $btn = $('.btn-login').button('loading');

    cliente_verifica_email(
        $("#form-login input[name='email']").val(),
        function(usuario){//successCallback
            alertify.success('Olá ' + usuario.nome);
            //$('#modal-login').modal('hide');
            setTimeout(function(){
                location.reload();
            }, 700);
        },
        function(data){//failureCallback
            if(data.msg != undefined) // BLOQUEADO
            {
                //FECHA MODAL
                $('#modal-login').modal('hide');
                alertify.error(data.msg);
            }
            else
            {
                $("#form-cadastrar input[name='email']").val($("#form-login input[name='email']").val());

                $('#modal-login').modal('hide');
                $('#modal-cadastrar').modal('show');
            }

        },
        function(){//errorCallback
            alertify.error('Ocorreu um erro ao efetuar o login. Por favor tente mais tarde!');
        },
        function(){//completeCallback
            $btn.button('reset');
        });
}

function cliente_logout()
{
    alertify
        .okBtn("Sim")
        .cancelBtn("Não")
        .confirm("Deseja realmente sair?",
            function () {
                window.location.href = $('#base_url').val() + 'cliente/logout';
            });
}


$(document).ready(function() {

    $("#form-cadastrar").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            nome: {
                required: true,
                minlength: 5
            }
        }
    });
});

function cliente_cadastrar()
{
    if(!$("#form-cadastrar").valid()) return;

    var $btn = $('.btn-login').button('loading');

    var cliente = $('#form-cadastrar').jsonify();

    cliente_registrar(
        cliente,
        {
            successCallback: function(data){
                $('#modal-cadastrar').modal('hide');

                //SETA INPUT DE EMAIL NO FORM DE LOGIN PARA EFETUAR O LOGIN APOS REGISTRO DO CLIENTE
                $("#form-login input[name='email']").val(cliente.email);
                cliente_login();
            },
            failureCallback: function(data){
                alertify.error('Ocorreu um erro ao cadastrar. Por favor tente mais tarde!');
            },
            errorCallback: function(){
                alertify.error('Ocorreu um erro ao cadastrar. Por favor tente mais tarde!');
            },
            completeCallback: function(){
                $btn.button('reset');
            }
        });
}

function obter_cidades(form, cidadeSelected)
{
    var select_cidade = $($(form).find('select[name="cidade"]')[0]);
    select_cidade.find('.bs-title-option').text('Carregando...');
    select_cidade.find('option:not(.bs-title-option)').remove();
    select_cidade.prop('disabled', true);
    select_cidade.selectpicker('refresh');

    obter_cidades_pela_uf( { uf: $(form).find('select[name="uf"]').val() }, {
        successCallback: function(data)
        {
            $.each(data.cidades, function(index, cidade){
                select_cidade.append($('<option>', {
                    value: cidade.nome.toUpperCase(),
                    text: cidade.nome
                }));
            });

            select_cidade.find('.bs-title-option').text('Cidade');
            select_cidade.prop('disabled', false);

            if(cidadeSelected != undefined)
                select_cidade.val(cidadeSelected);

            if(cidadeSelected == "")
                select_cidade.prop('disabled', true);

            select_cidade.selectpicker('refresh');
        },
        failureCallback: function(data){
            alertify.error('Ocorreu um erro ao carregar as cidades. Por favor tente mais tarde!');
        },
        errorCallback: function(){
            alertify.error('Ocorreu um erro ao carregar as cidades.');
        }
    })
}