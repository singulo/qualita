function obter_conteudo_incc(onSuccess)
{
    if($.cookie('conteudo-incc') != undefined)
    {
        console.log('INCC do cache');
        onSuccess(JSON.parse($.cookie('conteudo-incc')));
    }
    else{
        $.ajax({
            url: 'http://www.simplesimob.com.br/gerenciamento/conteudo/incc',
            type: 'GET',
            dataType: "json",
            success: function (data) {
                console.log('INCC da consulta');
                $.cookie('conteudo-incc', JSON.stringify(data.incc), { expires: 1, path: '/'});
                onSuccess(data.incc);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log('Ocorreu um erro ao obter o INCC. Tente novamente mais tarde.');
            },
            complete: function(){
                console.log('Ocorreu um erro ao obter o INCC');
            }
        });
    }
}

/*
 * Substitui o texto de todos elementos INCC utilizando o attr data-text como formatador do texto.*
 *
 * Ex elemento:
 * <incc data-text="INCC-M / {data}: {valor}"></incc>
 * <incc data-text="{data}: {valor}%"></incc>
 *
 * Ex saída:
 * <incc data-text="INCC-M / {data}: {valor}">INCC-M / 11/2016: 0,17</incc>
 * <incc data-text="{data}: {valor}%">11/2016: 0,17%</incc>
 */
$(document).ready(function(){
    obter_conteudo_incc(function(incc){
        $.each($('incc'), function(index, inccElem) {
            var text = '{data}: {valor}';

            if($(inccElem).data('text') != undefined)
                text = $(inccElem).data('text');

            $(inccElem).text(text.replace('{data}', incc.data).replace('{valor}', incc.valor));
        });
    });
});
