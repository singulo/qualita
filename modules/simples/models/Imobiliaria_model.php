<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Imobiliaria_Model extends Base_Model
{
    public $id;
    public $nome;
    public $razao_social;
    public $cnpj;
    public $creci;
    public $telefone_1;
    public $telefone_2;
    public $telefone_3;
    public $cidade;
    public $estado;
    public $endereco;
    public $dominio_padrao;
    public $latitude_longitude;
    public $email_padrao;
    public $email_gerente;
    public $email_sender;
    public $email_sender_host;
    public $email_sender_porta;
    public $email_sender_senha;
    public $atualizado_em;
    public $previsao_investimento;
    public $opcoes_saldo;
    public $corretor_padrao_id;
    public $facebook;
    public $instagram;
    public $twitter;
    public $whatsapp;

    protected $table = 'tb_imobiliaria';

    public function novo(Imobiliaria_model $imobiliaria)
    {
        $this->db->insert($this->table, $imobiliaria);
        return $this->db->insert_id();
    }

    public function editar(Imobiliaria_model $imobiliaria)
    {
        $this->db->where('id', $imobiliaria->id);
        $this->db->update($this->table, $imobiliaria);
        return $this->db->affected_rows();
    }
}
