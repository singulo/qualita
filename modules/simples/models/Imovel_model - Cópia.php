<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Empreendimento_model.php');

class Imovel_Model extends Empreendimento_Model
{
    protected $table = 'view_imovel';
    protected $tb_tipo = 'tb_imovel_tipo';
    protected $tb_foto = 'tb_imovel_foto';
    public $tb_foto_foreign_key = 'id_imovel';
    public $tb_video = 'tb_imovel_video';
    public $tb_video_foreign_key = 'id_imovel';
    protected $tb_empreendimento_complemento = 'tb_imovel_complemento';
    protected $tb_complemento_foreign_key = 'id_imovel';
    //protected $tb_complemento = 'tb_complemento';

    public function todos_complementos()
    {
        return $this->db
            ->order_by('tipo')
            ->order_by('complemento')
            ->get($this->tb_complemento)
            ->result();
    }

    public function cidades_imoveis()
    {
        return $this->db->query('SELECT distinct(cidade) FROM ' . $this->table . ' ORDER BY cidade ASC')->result();
    }

    public function tipos()
    {
        return $this->db->get($this->tb_tipo)->result();
    }

    public function pesquisar(array $filtro, $pagina, $limite, $order_by = NULL)
    {
        $this->select_foto_principal();

        $this->monta_filtro($filtro);

        if(! is_null($order_by))
            $this->db->order_by($order_by);

        return $this->db->limit($limite, ($pagina * $limite))->get($this->table)->result();
    }

    public function pesquisar_total_resultados(array $filtro)
    {
        $this->monta_filtro($filtro);

        return $this->db->from($this->table)->count_all_results();
    }

    private function monta_filtro(array $filtro)
    {
        $valoresCond = array();
        if(isset($filtro['preco_min']) && $filtro['preco_min'] > 0)
            $valoresCond['valor >='] = (int)$filtro['preco_min'];
        if(isset($filtro['preco_max']) && $filtro['preco_max'] > 0 && $filtro['preco_max'] != 3000000)
            $valoresCond['valor <='] = (int)$filtro['preco_max'];

        unset($filtro['preco_min']);
        unset($filtro['preco_max']);

        $filtro = array_filter($filtro);

        foreach ($filtro as $key => $val)
            if (is_numeric($key)) // only numbers, a point and an `e` like in 1.1e10
                unset($filtro[$key]);

        if(isset($filtro['complementos']))
        {
            if(is_array($filtro['complementos']))
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento IN (" . implode(', ', $filtro['complementos']) . ")");
            else
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento = " . $filtro['complementos']);

            unset($filtro['complementos']);
        }

        if(count($valoresCond) > 0)
            $this->db->where($valoresCond);

        $pesqConfigs = $this->config->item('pesquisa');

        foreach ($filtro as $key => $value)
        {
            if(is_array($value))
                $this->db->where_in($key, $value);
            else if(isset($pesqConfigs[$key]) && $pesqConfigs[$key] == $value)
                $this->db->where($key . ' >=', $value);
            else
                $this->db->like($key, $value);
        }

        $this->db->select("$this->table.*");
    }

    /**
     * @param $id ignora o mesmo imóvel
     * @param $id_tipo
     * @param $id_empreendimento
     * @param $limit
     * @return mixed
     */
    function similares($id, $id_tipo, $id_condominio, $limit, $foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->db->order_by('RAND()')
            ->where('id !=', $id)
            ->where('id_tipo', $id_tipo)
            ->or_where('id_condominio', $id_condominio)
            ->limit($limit)
            ->get($this->table)
            ->result();
    }

    public function pelo_condominio($id_condominio, $foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->db
            ->where('id_condominio', $id_condominio)
            ->get($this->table)
            ->result();
    }
}