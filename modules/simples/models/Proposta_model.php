<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Proposta_Model extends Base_Model
{
    public $id;
    public $cliente_nome;
    public $cliente_email;
    public $assunto;
    public $cod_imoveis;
    public $id_corretor;
    public $data;

    protected $table = 'tb_propostas';
    protected $tb_lidas = 'tb_propostas_lidas';
    protected $tb_proposta_imovel = 'tb_proposta_imovel';

    public function novo(Proposta_Model $proposta, $imoveis = [])
    {
        $this->db->trans_begin();

        $proposta->id = $this->inserir($proposta);

        $imoveis_propostas = [];
        foreach ($imoveis as $imovel) {
            $imo = new stdClass();
            $imo->id_proposta = $proposta->id;
            $imo->id_imovel = $imovel;
            $imoveis_propostas[] = $imo;
        }

        if(count($imoveis) > 0)
            $this->inserir_varios($imoveis_propostas, $this->tb_proposta_imovel);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return 0;
        }
        else
        {
            $this->db->trans_commit();
            return $proposta->id;
        }
    }

    public function marcar_como_lida($id)
    {
        $proposta = $this->obter($id);

        if( ! is_null($proposta))
        {
            $this->db->trans_begin();

            $proposta->lida_em = date('Y-m-d H:i:s');

            $this->inserir($proposta, $this->tb_lidas);

            $this->deletar($id);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                return false;
            }
            else
            {
                $this->db->trans_commit();
                return true;
            }
        }

        return false;
    }

    public function obter_lida($id)
    {
        return $this->obter($id, 'id', $this->tb_lidas);
    }
}