<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(MODULESPATH . 'simples/helpers/filial_helper.php');
require_once(MODULESPATH . 'simples/helpers/cliente_helper.php');

/**
 * @property Clientes_Model $clientes_model
 * @property Imobiliaria_Model $imobiliaria_model
 * @property CI_Session $session
 */
class Base_Controller extends MX_Controller {

	public function __construct($redirecionar_se_db_nao_setado = true)
	{
		parent::__construct();

		if(! isset($_GET['filial']) && count($this->config->item('filiais')) == 1)
		{
			$filiais = $this->config->item('filiais');
			$_GET['filial'] = reset($filiais)['chave'];
		}

		if( $this->sobreescrever_sessao() )
		{
			armazena_filial_em_sessao();
		}
		else if( ! isset($_SESSION['filial']) )
		{
			redirect( base_url() );
			die;
		}

		if($this->session->has_userdata('cliente_filiais'))
		{
			$chave = $_SESSION['filial']['chave'];

			if( isset($_GET['filial']) && isset($this->session->userdata('cliente_filiais')[$chave]) )
			{
				$cliente = $this->session->userdata('cliente_filiais')[$chave];

				$this->session->unset_userdata('usuario');

				if( ! isset($cliente->ultimo_acesso_registrado) || ! $cliente->ultimo_acesso_registrado )
				{
					$cliente->ultimo_acesso_registrado = registrar_acesso($cliente->id) > 0;
					$this->session->set_userdata('usuario', $cliente);
				}
				else
				{
					$this->session->set_userdata('usuario', $cliente);
				}


			}
		}

		unset($_GET['filial']);
	}

	protected function sobreescrever_sessao()
	{
		if( ! isset($_GET['filial']) && isset($_SESSION['filial']) )
			return false;
		else
			return true;
	}
}
